IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__WhoCr__5F74D762]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [CK__UserPassw__WhoCr__5F74D762]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Usern__5C986AB7]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [CK__UserPassw__Usern__5C986AB7]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Passw__63456846]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [CK__UserPassw__Passw__63456846]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Appli__5BA4467E]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [CK__UserPassw__Appli__5BA4467E]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__WhoCr__10180EBD]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [CK__UserOAuth__WhoCr__10180EBD]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Usern__0D3BA212]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [CK__UserOAuth__Usern__0D3BA212]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__13E89FA1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [CK__UserOAuth__Provi__13E89FA1]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__12F47B68]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [CK__UserOAuth__Provi__12F47B68]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Appli__0C477DD9]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [CK__UserOAuth__Appli__0C477DD9]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__49859643]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] DROP CONSTRAINT [CK__UserLogin__Usern__49859643]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__4891720A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] DROP CONSTRAINT [CK__UserLogin__Appli__4891720A]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__WhoCr__363DB7A5]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [CK__UserLogin__WhoCr__363DB7A5]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__33614AFA]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [CK__UserLogin__Usern__33614AFA]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Token__391A2450]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [CK__UserLogin__Token__391A2450]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__326D26C1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [CK__UserLogin__Appli__326D26C1]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__521ADC44]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] DROP CONSTRAINT [CK__UserLogin__Usern__521ADC44]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__5126B80B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] DROP CONSTRAINT [CK__UserLogin__Appli__5126B80B]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__WhoCr__3E13E397]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [CK__UserLocke__WhoCr__3E13E397]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Usern__3B3776EC]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [CK__UserLocke__Usern__3B3776EC]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Appli__3A4352B3]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [CK__UserLocke__Appli__3A4352B3]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__WhoCr__204E7686]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [CK__UserLocal__WhoCr__204E7686]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Usern__1D7209DB]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [CK__UserLocal__Usern__1D7209DB]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Passw__241F076A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [CK__UserLocal__Passw__241F076A]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Appli__1C7DE5A2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [CK__UserLocal__Appli__1C7DE5A2]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__WhoCr__19D68321]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [CK__UserEmail__WhoCr__19D68321]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Usern__16FA1676]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [CK__UserEmail__Usern__16FA1676]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Appli__1605F23D]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [CK__UserEmail__Appli__1605F23D]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__WhoCr__7287ABD6]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [CK__UserConfi__WhoCr__7287ABD6]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Usern__6FAB3F2B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [CK__UserConfi__Usern__6FAB3F2B]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Token__75641881]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [CK__UserConfi__Token__75641881]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Appli__6EB71AF2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [CK__UserConfi__Appli__6EB71AF2]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__WhoCr__263C5A06]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [CK__UserComme__WhoCr__263C5A06]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Usern__235FED5B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [CK__UserComme__Usern__235FED5B]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Appli__226BC922]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [CK__UserComme__Appli__226BC922]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__WhoCr__31AE0CB2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [CK__UserAppro__WhoCr__31AE0CB2]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Usern__2ED1A007]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [CK__UserAppro__Usern__2ED1A007]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Appli__2DDD7BCE]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [CK__UserAppro__Appli__2DDD7BCE]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__WhoCreated__03E74202]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] DROP CONSTRAINT [CK__User__WhoCreated__03E74202]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Username__01FEF990]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] DROP CONSTRAINT [CK__User__Username__01FEF990]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Applicatio__010AD557]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] DROP CONSTRAINT [CK__User__Applicatio__010AD557]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__WhoCre__0194E53C]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [CK__RoleUser__WhoCre__0194E53C]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Userna__7FAC9CCA]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [CK__RoleUser__Userna__7FAC9CCA]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__RoleNa__7DC45458]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [CK__RoleUser__RoleNa__7DC45458]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Applic__7CD0301F]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [CK__RoleUser__Applic__7CD0301F]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__WhoCreated__7252A1AC]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] DROP CONSTRAINT [CK__Role__WhoCreated__7252A1AC]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__RoleName__706A593A]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] DROP CONSTRAINT [CK__Role__RoleName__706A593A]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__Applicatio__6F763501]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] DROP CONSTRAINT [CK__Role__Applicatio__6F763501]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__WhoCr__1E3123EA]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [CK__RightReso__WhoCr__1E3123EA]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Right__1B54B73F]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [CK__RightReso__Right__1B54B73F]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Resou__1F254823]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [CK__RightReso__Resou__1F254823]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Appli__1A609306]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [CK__RightReso__Appli__1A609306]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__WhoCreate__14A7B9B0]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] DROP CONSTRAINT [CK__Right__WhoCreate__14A7B9B0]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__RightName__12BF713E]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] DROP CONSTRAINT [CK__Right__RightName__12BF713E]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__Applicati__11CB4D05]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] DROP CONSTRAINT [CK__Right__Applicati__11CB4D05]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__WhoCr__02BE139F]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken] DROP CONSTRAINT [CK__OAuthToke__WhoCr__02BE139F]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__Appli__00D5CB2D]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken] DROP CONSTRAINT [CK__OAuthToke__Appli__00D5CB2D]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__WhoCr__5951E9B8]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [CK__GrantedUs__WhoCr__5951E9B8]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Usern__5769A146]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [CK__GrantedUs__Usern__5769A146]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Right__558158D4]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [CK__GrantedUs__Right__558158D4]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Appli__548D349B]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [CK__GrantedUs__Appli__548D349B]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__WhoCr__3DA9CF43]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [CK__GrantedRo__WhoCr__3DA9CF43]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__RoleN__3BC186D1]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [CK__GrantedRo__RoleN__3BC186D1]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Right__39D93E5F]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [CK__GrantedRo__Right__39D93E5F]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Appli__38E51A26]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [CK__GrantedRo__Appli__38E51A26]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__WhoCr__4CEC12D3]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [CK__DeniedUse__WhoCr__4CEC12D3]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Usern__4B03CA61]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [CK__DeniedUse__Usern__4B03CA61]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Right__491B81EF]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [CK__DeniedUse__Right__491B81EF]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Appli__48275DB6]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [CK__DeniedUse__Appli__48275DB6]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__WhoCr__3143F85E]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [CK__DeniedRol__WhoCr__3143F85E]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__RoleN__2F5BAFEC]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [CK__DeniedRol__RoleN__2F5BAFEC]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Right__2D73677A]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [CK__DeniedRol__Right__2D73677A]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Appli__2C7F4341]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [CK__DeniedRol__Appli__2C7F4341]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserPasswordQuestionAnswer]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [FK_SecurityUser_UserPasswordQuestionAnswer]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserOAuthLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [FK_SecurityUser_UserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginSuccess]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] DROP CONSTRAINT [FK_SecurityUser_UserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUserLogin_UserLoginResetToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [FK_SecurityUserLogin_UserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginFailure]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] DROP CONSTRAINT [FK_SecurityUser_UserLoginFailure]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLockedOut]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [FK_SecurityUser_UserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLocalLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [FK_SecurityUser_UserLocalLogin]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserEmail]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [FK_SecurityUser_UserEmail]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserConfirmationToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [FK_SecurityUser_UserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserComment]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [FK_SecurityUser_UserComment]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserApproval]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [FK_SecurityUser_UserApproval]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRole_SecurityRoleUser]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [FK_SecurityRole_SecurityRoleUser]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityRightResource]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [FK_SecurityRight_SecurityRightResource]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [FK_SecurityRight_SecurityGrantedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [FK_SecurityRight_SecurityGrantedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [FK_SecurityRight_SecurityDeniedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [FK_SecurityRight_SecurityDeniedRoleRight]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhoPa__6715F92A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__WhoPa__6715F92A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenP__6621D4F1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__WhenP__6621D4F1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__652DB0B8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__Passw__652DB0B8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__64398C7F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__Passw__64398C7F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__6251440D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__Passw__6251440D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhoDe__615D1FD4]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__WhoDe__615D1FD4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenD__6068FB9B]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__WhenD__6068FB9B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenC__5E80B329]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] DROP CONSTRAINT [DF__UserPassw__WhenC__5E80B329]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhoDe__1200572F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [DF__UserOAuth__WhoDe__1200572F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhenD__110C32F6]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [DF__UserOAuth__WhenD__110C32F6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhenC__0F23EA84]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] DROP CONSTRAINT [DF__UserOAuth__WhenC__0F23EA84]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenS__4B6DDEB5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginSuccess] DROP CONSTRAINT [DF__UserLogin__WhenS__4B6DDEB5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenV__3A0E4889]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [DF__UserLogin__WhenV__3A0E4889]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhoDe__38260017]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [DF__UserLogin__WhoDe__38260017]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenD__3731DBDE]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [DF__UserLogin__WhenD__3731DBDE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenC__3549936C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] DROP CONSTRAINT [DF__UserLogin__WhenC__3549936C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenF__540324B6]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginFailure] DROP CONSTRAINT [DF__UserLogin__WhenF__540324B6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhoDe__3FFC2C09]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [DF__UserLocke__WhoDe__3FFC2C09]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhenD__3F0807D0]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [DF__UserLocke__WhenD__3F0807D0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhenC__3D1FBF5E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] DROP CONSTRAINT [DF__UserLocke__WhenC__3D1FBF5E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhoPa__27EF984E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__WhoPa__27EF984E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenP__26FB7415]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__WhenP__26FB7415]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__26074FDC]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__Passw__26074FDC]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__25132BA3]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__Passw__25132BA3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__232AE331]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__Passw__232AE331]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhoDe__2236BEF8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__WhoDe__2236BEF8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenD__21429ABF]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__WhenD__21429ABF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenC__1F5A524D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] DROP CONSTRAINT [DF__UserLocal__WhenC__1F5A524D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhoDe__1BBECB93]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [DF__UserEmail__WhoDe__1BBECB93]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhenD__1ACAA75A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [DF__UserEmail__WhenD__1ACAA75A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhenC__18E25EE8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] DROP CONSTRAINT [DF__UserEmail__WhenC__18E25EE8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenC__76583CBA]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [DF__UserConfi__WhenC__76583CBA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhoDe__746FF448]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [DF__UserConfi__WhoDe__746FF448]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenD__737BD00F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [DF__UserConfi__WhenD__737BD00F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenC__7193879D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] DROP CONSTRAINT [DF__UserConfi__WhenC__7193879D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhoDe__2824A278]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [DF__UserComme__WhoDe__2824A278]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhenD__27307E3F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [DF__UserComme__WhenD__27307E3F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhenC__254835CD]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] DROP CONSTRAINT [DF__UserComme__WhenC__254835CD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhoDe__33965524]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [DF__UserAppro__WhoDe__33965524]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhenD__32A230EB]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [DF__UserAppro__WhenD__32A230EB]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhenC__30B9E879]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] DROP CONSTRAINT [DF__UserAppro__WhenC__30B9E879]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenLastAc__06C3AEAD]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] DROP CONSTRAINT [DF__User__WhenLastAc__06C3AEAD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhoDeleted__05CF8A74]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] DROP CONSTRAINT [DF__User__WhoDeleted__05CF8A74]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenDelete__04DB663B]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] DROP CONSTRAINT [DF__User__WhenDelete__04DB663B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenCreate__02F31DC9]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] DROP CONSTRAINT [DF__User__WhenCreate__02F31DC9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhoDel__037D2DAE]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [DF__RoleUser__WhoDel__037D2DAE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhenDe__02890975]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [DF__RoleUser__WhenDe__02890975]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhenCr__00A0C103]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] DROP CONSTRAINT [DF__RoleUser__WhenCr__00A0C103]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhoDeleted__743AEA1E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] DROP CONSTRAINT [DF__Role__WhoDeleted__743AEA1E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhenDelete__7346C5E5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] DROP CONSTRAINT [DF__Role__WhenDelete__7346C5E5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhenCreate__715E7D73]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] DROP CONSTRAINT [DF__Role__WhenCreate__715E7D73]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhoDe__210D9095]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [DF__RightReso__WhoDe__210D9095]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhenD__20196C5C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [DF__RightReso__WhenD__20196C5C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhenC__1D3CFFB1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] DROP CONSTRAINT [DF__RightReso__WhenC__1D3CFFB1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhoDelete__16900222]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] DROP CONSTRAINT [DF__Right__WhoDelete__16900222]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhenDelet__159BDDE9]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] DROP CONSTRAINT [DF__Right__WhenDelet__159BDDE9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhenCreat__13B39577]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] DROP CONSTRAINT [DF__Right__WhenCreat__13B39577]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhoDe__04A65C11]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] DROP CONSTRAINT [DF__OAuthToke__WhoDe__04A65C11]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhenD__03B237D8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] DROP CONSTRAINT [DF__OAuthToke__WhenD__03B237D8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhenC__01C9EF66]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] DROP CONSTRAINT [DF__OAuthToke__WhenC__01C9EF66]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhoDe__5B3A322A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [DF__GrantedUs__WhoDe__5B3A322A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhenD__5A460DF1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [DF__GrantedUs__WhenD__5A460DF1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhenC__585DC57F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] DROP CONSTRAINT [DF__GrantedUs__WhenC__585DC57F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhoDe__3F9217B5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [DF__GrantedRo__WhoDe__3F9217B5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhenD__3E9DF37C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [DF__GrantedRo__WhenD__3E9DF37C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhenC__3CB5AB0A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] DROP CONSTRAINT [DF__GrantedRo__WhenC__3CB5AB0A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhoDe__4ED45B45]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [DF__DeniedUse__WhoDe__4ED45B45]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhenD__4DE0370C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [DF__DeniedUse__WhenD__4DE0370C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhenC__4BF7EE9A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] DROP CONSTRAINT [DF__DeniedUse__WhenC__4BF7EE9A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhoDe__332C40D0]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [DF__DeniedRol__WhoDe__332C40D0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhenD__32381C97]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [DF__DeniedRol__WhenD__32381C97]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhenC__304FD425]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] DROP CONSTRAINT [DF__DeniedRol__WhenC__304FD425]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]') AND name = N'IX_Security_UserLoginSuccess')
DROP INDEX [IX_Security_UserLoginSuccess] ON [Security].[UserLoginSuccess] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]') AND name = N'IX_Security_UserLoginResetToken')
DROP INDEX [IX_Security_UserLoginResetToken] ON [Security].[UserLoginResetToken] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailure]') AND name = N'IX_Security_UserLoginFailure')
DROP INDEX [IX_Security_UserLoginFailure] ON [Security].[UserLoginFailure] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[OAuthToken]') AND name = N'IX_Security_OAuthToken')
DROP INDEX [IX_Security_OAuthToken] ON [Security].[OAuthToken] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLoginResetToken]'))
DROP VIEW [Security].[CurrentUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserApproval]'))
DROP VIEW [Security].[CurrentUserApproval]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserApproval]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserApproval]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentDeniedRoleRight]'))
DROP VIEW [Security].[CurrentDeniedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentDeniedRoleRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentDeniedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserComment]'))
DROP VIEW [Security].[CurrentUserComment]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLocalLogin]'))
DROP VIEW [Security].[CurrentUserLocalLogin]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRight]'))
DROP VIEW [Security].[CurrentRight]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserOAuthLogin]'))
DROP VIEW [Security].[CurrentUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserOAuthLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUser]'))
DROP VIEW [Security].[CurrentUser]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserEmail]'))
DROP VIEW [Security].[CurrentUserEmail]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentOAuthToken]'))
DROP VIEW [Security].[CurrentOAuthToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentOAuthToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentOAuthToken]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRoleUser]'))
DROP VIEW [Security].[CurrentRoleUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRoleUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentRoleUser]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserConfirmationToken]'))
DROP VIEW [Security].[CurrentUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserConfirmationToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRole]'))
DROP VIEW [Security].[CurrentRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRole]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentRole]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserPasswordQuestionAnswer]'))
DROP VIEW [Security].[CurrentUserPasswordQuestionAnswer]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentGrantedUserRight]'))
DROP VIEW [Security].[CurrentGrantedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentGrantedUserRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentGrantedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentDeniedUserRight]'))
DROP VIEW [Security].[CurrentDeniedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OnlineMembershipUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[OnlineMembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentDeniedUserRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentDeniedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentMembership]'))
DROP VIEW [Security].[CurrentMembership]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentMembership]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentMembership]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserPasswordQuestionAnswer]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserPasswordQuestionAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLocalLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserLocalLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserEmail]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserComment]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserComment]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUser]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLockedOut]'))
DROP VIEW [Security].[CurrentUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentGrantedRoleRight]'))
DROP VIEW [Security].[CurrentGrantedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentGrantedRoleRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentGrantedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[GetCurrentRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]') AND type in (N'U'))
DROP TABLE [Security].[UserPasswordQuestionAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]') AND type in (N'U'))
DROP TABLE [Security].[UserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLogout]') AND type in (N'U'))
DROP TABLE [Security].[UserLogout]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]') AND type in (N'U'))
DROP TABLE [Security].[UserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]') AND type in (N'U'))
DROP TABLE [Security].[UserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailure]') AND type in (N'U'))
DROP TABLE [Security].[UserLoginFailure]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLockedOut]') AND type in (N'U'))
DROP TABLE [Security].[UserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLocalLogin]') AND type in (N'U'))
DROP TABLE [Security].[UserLocalLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserEmail]') AND type in (N'U'))
DROP TABLE [Security].[UserEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]') AND type in (N'U'))
DROP TABLE [Security].[UserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserComment]') AND type in (N'U'))
DROP TABLE [Security].[UserComment]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserApproval]') AND type in (N'U'))
DROP TABLE [Security].[UserApproval]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[User]') AND type in (N'U'))
DROP TABLE [Security].[User]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleUser]') AND type in (N'U'))
DROP TABLE [Security].[RoleUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[Role]') AND type in (N'U'))
DROP TABLE [Security].[Role]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RightResource]') AND type in (N'U'))
DROP TABLE [Security].[RightResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[Right]') AND type in (N'U'))
DROP TABLE [Security].[Right]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OAuthToken]') AND type in (N'U'))
DROP TABLE [Security].[OAuthToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantedUserRight]') AND type in (N'U'))
DROP TABLE [Security].[GrantedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]') AND type in (N'U'))
DROP TABLE [Security].[GrantedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeniedUserRight]') AND type in (N'U'))
DROP TABLE [Security].[DeniedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]') AND type in (N'U'))
DROP TABLE [Security].[DeniedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenUserCreatedByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[WhenUserCreatedByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenRoleCreatedByRoleName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[WhenRoleCreatedByRoleName]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenRightCreated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[WhenRightCreated]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameFromUserId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UsernameFromUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameByUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UsernameByUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameByUserConfirmationToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UsernameByUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetTokenByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserLoginResetTokenByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailureSinceLastUserLoginSuccess]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserLoginFailureSinceLastUserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLocalLoginWhenCreatedByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserLocalLoginWhenCreatedByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLastActive]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserLastActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdFromUserOAuthLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserIdFromUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdFromUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserIdFromUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdByUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserIdByUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserGrantedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserGrantedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserDeniedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[UserDeniedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[TrimToNull]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[TrimToNull]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleGrantedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[RoleGrantedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleDeniedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[RoleDeniedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OAuthTokenSecretByToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[OAuthTokenSecretByToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLogout]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[LastUserLogout]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLoginSuccess]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[LastUserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLoginFailure]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[LastUserLoginFailure]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[LastUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsValidUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsValidUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserInRole]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsUserInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserConfirmed]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsUserConfirmed]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserApproved]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsUserApproved]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsRolePopulated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsRolePopulated]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsRightPopulated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsRightPopulated]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsResourceUnique]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsResourceUnique]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsEmailUnique]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[IsEmailUnique]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserOathLoginByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[HasUserOathLoginByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserLocalLoginByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[HasUserLocalLoginByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserLocalLoginByUserId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[HasUserLocalLoginByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserHaveRightByResource]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[DoesUserHaveRightByResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserHaveRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[DoesUserHaveRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[DoesUserExist]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesRoleExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[DoesRoleExist]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesRightExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[DoesRightExist]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AllTrim]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Security].[AllTrim]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[VerifyUserLoginResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[VerifyUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUserPasswordAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ValidateUserPasswordAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUserLocalLoginPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ValidateUserLocalLoginPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ValidateUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidatePasswordAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ValidatePasswordAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLoginSuccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[UpdateUserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLoginFailure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[UpdateUserLoginFailure]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLastActive]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[UpdateUserLastActive]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateMembershipUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[UpdateMembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UnlockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[UnlockUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[StoreOAuthRequestToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[StoreOAuthRequestToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserPasswordQuestionAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserPasswordQuestionAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserOAuthLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLoginResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLockedOut]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLocalPasswordWithUserLoginResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserLocalPasswordWithUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLocalLoginPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserLocalLoginPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserConfirmationToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserComment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserComment]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetUserApproval]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetRightResource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetRightResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetPasswordResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetPasswordResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetOAuthToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetOAuthToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetConfirmationToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[SetConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[RoleExists]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ResetPasswordWithToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ResetPasswordWithToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ReplaceOAuthRequestTokenWithAccessToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ReplaceOAuthRequestTokenWithAccessToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RemoveUserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[RemoveUserRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LogoutUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[LogoutUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsConfirmed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[IsConfirmed]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasLocalAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[HasLocalAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantUserRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GrantUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantRoleRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GrantRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetWhenUserCreatedByUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetWhenUserCreatedByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsersInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUsersInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserPasswordQuestionAnswerOption]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserPasswordQuestionAnswerOption]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserNameFromId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserNameFromId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUsernameByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByUserConfirmationToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUsernameByUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUsernameByEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLoginResetTokenByUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserLoginResetTokenByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLoginFailureSinceLastUserLoginSuccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserLoginFailureSinceLastUserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLocalLoginPasswordOption]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserLocalLoginPasswordOption]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromUserOAuthLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserIdFromUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserIdFromUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromPasswordResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserIdFromPasswordResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdByUserLoginResetToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserIdByUserLoginResetToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserByUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserByUsername]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUserByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRolesForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetRolesForUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRightExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetRightExists]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRightByResource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetRightByResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPasswordFailuresSinceLastSuccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetPasswordFailuresSinceLastSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPasswordChangedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetPasswordChangedDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetOnlineMembershipUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetOnlineMembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetOAuthTokenSecretByToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetOAuthTokenSecretByToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetNumberOfUsersOnline]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLoginSuccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetLastUserLoginSuccess]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLoginFailure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetLastUserLoginFailure]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLockedOut]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetLastUserLockedOut]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastPasswordFailureDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetLastPasswordFailureDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsUserInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserGrantedRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsUserGrantedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserDeniedRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsUserDeniedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserConfirmed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsUserConfirmed]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsRoleGrantedRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsRoleGrantedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsRoleDeniedRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetIsRoleDeniedRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetGrantedUserRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetGrantedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetGrantedRoleRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetGrantedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetDeniedUserRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetDeniedUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetDeniedRoleRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetDeniedRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCreateDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetCreateDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetAllUsers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetAccountsForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[GetAccountsForUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindUsersInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[FindUsersInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindRightsByRightName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[FindRightsByRightName]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindRightsByResource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[FindRightsByResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindMembershipUsersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[FindMembershipUsersByName]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindMembershipUsersByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[FindMembershipUsersByEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DenyUserRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DenyUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DenyRoleRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DenyRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteUserOAuthLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteUserOAuthLogin]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthTokenByToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteOAuthTokenByToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteOAuthToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteOAuthAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[DeleteAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[CreateUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[CreateRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[CreateRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateOrUpdateOAuthAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[CreateOrUpdateOAuthAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateMembershipUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[CreateMembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ConfirmUserConfirmationToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ConfirmUserConfirmationToken]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ConfirmAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ConfirmAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ClearUserRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ClearUserRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ClearRoleRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ClearRoleRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ChangePasswordQuestionAndAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ChangePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[ChangePassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AssertRightByResource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[AssertRightByResource]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AssertRight]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[AssertRight]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AddUserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Security].[AddUserRole]
GO
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'Security')
DROP SCHEMA [Security]
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Security')
EXEC sys.sp_executesql N'CREATE SCHEMA [Security]'

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AddUserRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[AddUserRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 2
	end else if ( 
		( @roleName is null ) 
		or ( 0 = [Security].DoesRoleExist( @applicationName, @roleName ) ) 
	) begin 
		set @output = 1
	end else if ( 1 = [Security].IsUserInRole( @applicationName, @roleName, @username ) ) begin 
		set @output = 4
	end else begin 
		declare @whenRoleCreated datetime
		set @whenRoleCreated = [Security].WhenRoleCreatedByRoleName( @applicationName, @roleName )
		insert into 
			[Security].RoleUser ( ApplicationName, RoleName, WhenRoleCreated, Username, WhenCreated, WhoCreated ) 
			values ( @applicationName, @roleName, @whenRoleCreated, @username, GetDate(), @who ) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AssertRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[AssertRight]( @applicationName varchar( 255 ), @username varchar( 255 ), @rightName varchar( 255 ) ) as 
	if ( 1 != Security.DoesUserHaveRight( @applicationName, @username, @rightName ) ) begin 
		RaisError( ''Access is denied.'', 16, 1 )
		return 1
	end
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AssertRightByResource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[AssertRightByResource]( @applicationName varchar( 255 ), @username varchar( 255 ), @resource nvarchar( 256 ) ) as 
	if ( 1 != Security.DoesUserHaveRightByResource( @applicationName, @username, @resource ) ) begin 
		RaisError( ''Access is denied.'', 16, 1 )
		return 1
	end
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ChangePassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ChangePassword]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@oldPassword nvarchar( 512 ), 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@failIfNotApproved bit = 1, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @oldPassword = [Security].TrimToNull( @oldPassword )
	set @password = [Security].TrimToNull( @password )
	set @passwordAlgorithm = [Security].TrimToNull( @passwordAlgorithm )
	set @passwordSalt = [Security].TrimToNull( @passwordSalt )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( ( @oldPassword is null ) or ( @password is null ) ) begin 
		set @output = 2
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ValidateUserLocalLoginPassword @applicationName, @username, @oldPassword, @failIfNotApproved 
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserLocalLoginPassword @applicationName, @username, @password, @passwordFormat, @passwordAlgorithm, @passwordSalt, @failIfNotApproved, @who 
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ChangePasswordQuestionAndAnswer]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@password nvarchar( 512 ), 
	@passwordQuestion nvarchar( 512 ), 
	@passwordAnswer nvarchar( 512 ), @passwordAnswerFormat int, @passwordAnswerAlgorithm nvarchar( 255 ), @passwordAnswerSalt nvarchar( 255 ), 
	@requirePasswordQuestionAndAnswer bit, 
	@failIfNotApproved bit = 1, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @password = [Security].TrimToNull( @password )
	set @passwordQuestion = [Security].TrimToNull( @passwordQuestion )
	set @passwordAnswerAlgorithm = [Security].TrimToNull( @passwordAnswerAlgorithm )
	set @passwordAnswerSalt = [Security].TrimToNull( @passwordAnswerSalt )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @requirePasswordQuestionAndAnswer is null ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @password is null ) begin 
		set @output = 2
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ValidateUserLocalLoginPassword @applicationName, @username, @password, @failIfNotApproved 
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserPasswordQuestionAnswer @applicationName, @username, @passwordQuestion, @passwordAnswer, @passwordAnswerFormat, @passwordAnswerAlgorithm, @passwordAnswerSalt, @requirePasswordQuestionAndAnswer, @failIfNotApproved, @who 
		end
	end

	return @output	
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ClearRoleRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ClearRoleRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @roleName varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 3
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].GrantedRoleRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( RoleName = @roleName ) 
			) 
		update [Security].DeniedRoleRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( RoleName = @roleName ) 
			) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ClearUserRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ClearUserRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @username varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 4
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].GrantedUserRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( Username = @username ) 
			) 
		update [Security].DeniedUserRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( Username = @username ) 
			) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ConfirmAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[ConfirmAccount]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) = null output, @token nvarchar( 256 ), @tokenLifetime int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime < 1 ) ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( 
		( @username is not null ) 
		and ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ConfirmUserConfirmationToken @applicationName, @username output, @token, @tokenLifetime 
		if ( 0 = @output ) begin  
			exec @output = [Security].SetUserApproval @applicationName, @username, 1, @username 
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ConfirmUserConfirmationToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[ConfirmUserConfirmationToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) output, @token nvarchar( 256 ), @tokenLifetime int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @token = [Security].TrimToNull( @token ) 
	set @username = [Security].TrimToNull( @username ) 

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime < 1 ) ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( not exists ( select top 1 1 
		from [Security].GetCurrentUserConfirmationToken() 
		where ( 
			( WhenConfirmed is null ) 
			and ( Token = @token ) 
		) 
	) ) begin 
		set @output = 8
	end else if ( @username is null ) begin 
		set @username = [Security].UsernameByUserConfirmationToken( @applicationName, @token, @tokenLifetime )
		if ( 
			( @username is null ) 
			or ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
			or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		) begin 
			set @output = 8
		end else begin 
			update [Security].UserConfirmationToken 
				set WhenConfirmed = GetDate() 
				where ( 
					( WhenDeleted is null ) 
					and ( WhenConfirmed is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
					and ( Token = @token ) 
					and ( GetDate() <= DateAdd( MI, @tokenLifetime, WhenCreated ) ) 
				) 
			if ( 1 = [Security].IsUserConfirmed( @applicationName, @username ) ) begin 
				set @output = 0
			end else begin 
				set @output = 11
			end
		end
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( @username = [Security].UsernameByUserConfirmationToken( @applicationName, @token, @tokenLifetime ) ) begin 
		update [Security].UserConfirmationToken 
			set WhenConfirmed = GetDate() 
			where ( 
				( WhenDeleted is null ) 
				and ( WhenConfirmed is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( Token = @token ) 
				and ( GetDate() <= DateAdd( MI, @tokenLifetime, WhenCreated ) ) 
			) 
		if ( 1 = [Security].IsUserConfirmed( @applicationName, @username ) ) begin 
			set @output = 0
		end else begin 
			set @output = 11
		end
	end else begin 
		set @output = 8
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateMembershipUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[CreateMembershipUser]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 256 ), @userId int = null output, 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@email nvarchar( 256 ), @requireUniqueEmail bit, 
	@passwordQuestion nvarchar( 512 ), 
	@passwordAnswer nvarchar( 512 ), @passwordAnswerFormat int, @passwordAnswerAlgorithm nvarchar( 255 ), @passwordAnswerSalt nvarchar( 255 ), 
	@requirePasswordQuestionAndAnswer bit, 
	@isApproved bit = 0, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end
	declare @output int
	set @output = 11

	if ( 
		( @applicationName is null ) 
		or ( @requireUniqueEmail is null ) 
		or ( @isApproved is null ) 
	) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @password is null ) begin 
		set @output = 2
	end else if ( ( @passwordAnswerFormat is null ) or ( @passwordAnswerAlgorithm is null ) ) begin 
		set @output = 11
	end else if ( ( @userId is not null ) and ( @userId < 1 ) ) begin 
		set @output = 9
	end else if ( 
		( @userId is not null ) 
		and ( exists ( select top 1 1 
			from [Security].GetCurrentUser() 
			where ( 
				( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( UserId <> @userId ) 
			) 
		) ) 
	) begin 
		set @output = 6
	end else if ( 
		( @userId is not null ) 
		and ( exists ( select top 1 1 
			from [Security].GetCurrentUser() 
			where ( 
				( ApplicationName = @applicationName ) 
				and ( Username <> @username ) 
				and ( UserId = @userId ) 
			) 
		) ) 
	) begin 
		set @output = 10
	end else if ( 1 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) begin 
		set @output = 6
	end else begin 
		if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
			exec @output = [Security].CreateUser @applicationName, @username, @userId output, @who 
		end else begin 
			set @output = 0
		end
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserEmail @applicationName, @username, @email, @requireUniqueEmail, @who 
		end
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserPasswordQuestionAnswer @applicationName, @username, @passwordQuestion, @passwordAnswer, @passwordAnswerFormat, @passwordAnswerAlgorithm, @passwordAnswerSalt, @requirePasswordQuestionAndAnswer, 0, @who 
		end
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserLocalLoginPassword @applicationName, @username, @password, @passwordFormat, @passwordAlgorithm, @passwordSalt, 0, @who 
		end
		if ( ( 0 = @output ) and ( 1 = @isApproved ) ) begin 
			exec @output = [Security].SetUserApproval @applicationName, @username, @isApproved, @who 
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateOrUpdateOAuthAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[CreateOrUpdateOAuthAccount]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else begin 
		exec @output = [Security].SetUserOAuthLogin @applicationName, @provider, @providerUsername, @username, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[CreateRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @resource nvarchar( 1024 ) = null, @requireUniqueResource bit = 1, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @resource = [Security].TrimToNull( @resource )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end
	if ( @requireUniqueResource is null ) begin 
		set @requireUniqueResource = 1
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @rightName is null ) begin 
		set @output = 1
	end else if ( ( 1 = @requireUniqueResource ) and ( @resource is null ) ) begin 
		set @output = 2
	end else if ( 1 = [Security].DoesRightExist( @applicationName, @rightName ) ) begin 
		set @output = 5
	end else if ( ( 1 = @requireUniqueResource ) and ( 0 = [Security].IsResourceUnique( @applicationName, @rightName, @resource ) ) ) begin 
		set @output = 6
	end else begin 
		declare @whenRightCreated datetime
		set @whenRightCreated = GetDate()
		insert into 
			[Security].[Right] ( ApplicationName, RightName, WhenCreated, WhoCreated ) 
			values ( @applicationName, @rightName, @whenRightCreated, @who ) 
		if ( @resource is not null ) begin 
			insert into 
				[Security].[RightResource] ( ApplicationName, RightName, WhenRightCreated, WhenCreated, WhoCreated, [Resource] ) 
				values ( @applicationName, @rightName, @whenRightCreated, @whenRightCreated, @who, @resource ) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[CreateRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 1
	end else if ( 1 = [Security].DoesRoleExist( @applicationName, @roleName ) ) begin 
		set @output = 3
	end else begin 
		insert into 
			[Security].[Role] ( ApplicationName, RoleName, WhenCreated, WhoCreated ) 
			values ( @applicationName, @roleName, GetDate(), @who ) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[CreateUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @userId int = null output, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end
	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1 
	end else if ( ( @userId is not null ) and ( @userId < 1 ) ) begin 
		set @output = 9
	end else if ( 1 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 6
	end else if ( exists ( select top 1 1 
			from [Security].GetCurrentUser() 
			where ( 
				( ApplicationName = @applicationName ) 
				and ( UserId = @userId ) 
			) 
	) ) begin 
		set @output = 10
	end else begin 
		if ( @userId is not null ) begin 
			set identity_insert [Security].[User] on
			insert into 
				[Security].[User] ( ApplicationName, Username, WhenCreated, WhoCreated, UserId, WhenLastActive ) 
				values ( @applicationName, @username, GetDate(), @who, @userId, null ) 
			set identity_insert [Security].[User] off 
		end else begin 
			insert into 
				[Security].[User] ( ApplicationName, Username, WhenCreated, WhoCreated, WhenLastActive ) 
				values ( @applicationName, @username, GetDate(), @who, null ) 
			set @userId = ( select top 1 UserId 
				from [Security].GetCurrentUser() 
				where ( 
					( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
				) 
			)
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteAccount]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @whenUserCreated datetime
	set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
	if ( @whenUserCreated is not null ) begin 
		update [Security].UserLocalLogin 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( WhenUserCreated = @whenUserCreated ) 
			) 
	end

	return ( select Count( * ) 
		from [Security].GetCurrentUserLocalLogin() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteOAuthAccount]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else begin 
		exec @output = [Security].DeleteUserOAuthLogin @applicationName, @provider, @providerUsername, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteOAuthToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else begin 
		exec @output = DeleteOAuthTokenByToken @applicationName, @token, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteOAuthTokenByToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteOAuthTokenByToken]( @applicationName nvarchar( 255 ), @token nvarchar( 128 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	update [Security].OAuthToken 
		set WhenDeleted = null, 
			WhoDeleted = @who 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = @applicationName ) 
			and ( Token = @token ) 
		) 

	return ( select Count( * ) 
		from [Security].GetCurrentOAuthToken() 
		where ( 
			( Token = @token ) 
		) 
	) 
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @failIfPopulated bit = 1, @deleteRelatedData bit = 0, @who varchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end
	if ( @failIfPopulated is null ) begin 
		set @failIfPopulated = 1
	end
	if ( @deleteRelatedData is null ) begin 
		set @deleteRelatedData = 0
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else if ( ( 1 = @failIfPopulated ) and ( 1 = [Security].IsRightPopulated( @applicationName, @rightName ) ) ) begin 
		set @output = 9
	end else begin 
		if ( 1 = @deleteRelatedData ) begin 
			update [Security].DeniedRoleRight 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			update [Security].DeniedUserRight 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			update [Security].GrantedRoleRight 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			update [Security].GrantedUserRight 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			update [Security].DeniedRoleRight 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			update [Security].RightResource 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
		end
		update [Security].[Right] 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
			) 
		if ( 1 = [Security].DoesRightExist( @applicationName, @rightName ) ) begin 
			set @output = 11
		end else begin 
			set @output = 0
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [Security].[DeleteRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @failIfPopulated bit = 1, @deleteRelatedData bit = 0, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end
	if ( @failIfPopulated is null ) begin 
		set @failIfPopulated = 1
	end
	if ( @deleteRelatedData is null ) begin 
		set @deleteRelatedData = 0
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 1
	end else if ( ( 1 = @failIfPopulated ) and ( 1 = [Security].IsRolePopulated( @applicationName, @roleName ) ) ) begin 
		set @output = 5
	end else begin 
		if ( 1 = @deleteRelatedData ) begin 
			update [Security].RoleUser 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( RoleName = @roleName ) 
				) 
		end
		update [Security].[Role] 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RoleName = @roleName ) 
			) 
		if ( 1 = [Security].DoesRoleExist( @applicationName, @roleName ) ) begin 
			set @output = 11
		end else begin 
			set @output = 0
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @deleteAllRelatedData bit = 1, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end
	if ( @deleteAllRelatedData is null ) begin 
		set @deleteAllRelatedData = 1
	end

	update [Security].[User] 
		set WhenDeleted = GetDate(), 
			WhoDeleted = @who 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	if ( 1 = @deleteAllRelatedData ) begin 
		update [Security].UserApproval 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserComment 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserConfirmationToken
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserEmail 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserLockedOut 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserOAuthLogin
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserPasswordQuestionAnswer 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)

		update [Security].UserLocalLogin 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
		update [Security].UserLoginResetToken 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			)
	end

	return ( select Count( * ) 
		from [Security].GetCurrentUser() 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeleteUserOAuthLogin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DeleteUserOAuthLogin]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	update [Security].UserOAuthLogin 
		set WhenDeleted = GetDate(), 
			WhoDeleted = @who 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = @applicationName ) 
			and ( Provider = @provider ) 
			and ( ProviderUsername = @providerUsername ) 
		) 

	return ( select Count( * ) 
		from [Security].GetCurrentUserOAuthLogin() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Provider = @provider ) 
			and ( ProviderUsername = @providerUsername ) 
		) 
	) 
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DenyRoleRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DenyRoleRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @roleName varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 3
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].GrantedRoleRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( RoleName = @roleName ) 
			) 
		if ( not exists ( select top 1 1 
			from [Security].[Right] as r 
			join [Security].DeniedRoleRight as d on ( 
				( r.ApplicationName = d.ApplicationName ) 
				and ( r.RightName = d.RightName ) 
				and ( r.WhenCreated = d.WhenRightCreated ) 
			) 
			where ( 
				( r.WhenDeleted is null ) 
				and ( d.WhenDeleted is null ) 
				and ( d.ApplicationName = @applicationName ) 
				and ( d.RightName = @rightName ) 
				and ( d.RoleName = @roleName ) 
			) 
		) ) begin 
			declare @whenRightCreated datetime
			set @whenRightCreated = [Security].WhenRightCreated( @applicationName, @rightName )
			insert into 
				[Security].DeniedRoleRight ( ApplicationName, RightName, WhenRightCreated, RoleName, WhenCreated, WhoCreated ) 
				values ( @applicationName, @rightName, @whenRightCreated, @roleName, GetDate(), @who ) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DenyUserRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[DenyUserRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @username varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 4
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].GrantedUserRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( Username = @username ) 
			) 
		if ( not exists ( select top 1 1 
			from [Security].[Right] as r 
			join [Security].DeniedUserRight as d on ( 
				( r.ApplicationName = d.ApplicationName ) 
				and ( r.RightName = d.RightName ) 
				and ( r.WhenCreated = d.WhenRightCreated ) 
			) 
			where ( 
				( r.WhenDeleted is null ) 
				and ( d.WhenDeleted is null ) 
				and ( d.ApplicationName = @applicationName ) 
				and ( d.RightName = @rightName ) 
				and ( d.Username = @username ) 
			) 
		) ) begin 
			declare @whenRightCreated datetime
			set @whenRightCreated = [Security].WhenRightCreated( @applicationName, @rightName )
			insert into 
				[Security].DeniedUserRight ( ApplicationName, RightName, WhenRightCreated, Username, WhenCreated, WhoCreated ) 
				values ( @applicationName, @rightName, @whenRightCreated, @username, GetDate(), @who ) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindMembershipUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[FindMembershipUsersByEmail]( @applicationName nvarchar( 255 ), @emailToMatch nvarchar( 256 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @emailToMatch = [Security].TrimToNull( @emailToMatch )

	select * 
		from [Security].GetCurrentMembership() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( Email = @emailToMatch ) 
				or ( Email like ''%'' + @emailToMatch + ''%'' ) 
				or ( @emailToMatch like ''%'' + Email + ''%'' ) 
				or ( 4 <= DIFFERENCE( Email, @emailToMatch ) ) 
			) 
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindMembershipUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[FindMembershipUsersByName]( @applicationName nvarchar( 255 ), @usernameToMatch nvarchar( 256 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @usernameToMatch = [Security].TrimToNull( @usernameToMatch )

	select * 
		from [Security].GetCurrentMembership() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( Username = @usernameToMatch ) 
				or ( Username like ''%'' + @usernameToMatch + ''%'' ) 
				or ( @usernameToMatch like ''%'' + Username + ''%'' ) 
				or ( 3 <= DIFFERENCE( Username, @usernameToMatch ) ) 
			) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindRightsByResource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create procedure [Security].[FindRightsByResource]( @applicationName nvarchar( 255 ), @resourceToMatch nvarchar( 1024 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @resourceToMatch = [Security].TrimToNull( @resourceToMatch )

	select * 
		from [Security].GetCurrentRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( 
					( @resourceToMatch is null ) 
					and ( [Resource] is null ) 
				)
				or ( [Resource] = @resourceToMatch ) 
				or ( [Resource] like ''%'' + @resourceToMatch + ''%'' ) 
				or ( @resourceToMatch like ''%'' + [Resource] + ''%'' ) 
				or ( 3 <= DIFFERENCE( [Resource], @resourceToMatch ) ) 
			) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindRightsByRightName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[FindRightsByRightName]( @applicationName nvarchar( 255 ), @rightNameToMatch nvarchar( 1024 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightNameToMatch = [Security].TrimToNull( @rightNameToMatch )

	select * 
		from [Security].GetCurrentRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( RightName = @rightNameToMatch ) 
				or ( RightName like ''%'' + @rightNameToMatch + ''%'' ) 
				or ( @rightNameToMatch like ''%'' + RightName + ''%'' ) 
				or ( 3 <= DIFFERENCE( RightName, @rightNameToMatch ) ) 
			) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[FindUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[FindUsersInRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @usernameToMatch nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @usernameToMatch = [Security].TrimToNull( @usernameToMatch )

	select ru.Username 
		from [Security].GetCurrentRoleUser() as ru 
		join [Security].GetCurrentRole() as r on ( 
			( ru.ApplicationName = r.ApplicationName ) 
			and ( ru.RoleName = r.RoleName ) 
			and ( ru.WhenRoleCreated = r.WhenCreated ) 
		) 
		where ( 
			( ru.ApplicationName = @applicationName ) 
			and ( ru.RoleName = @roleName ) 
			and ( 
				( ru.Username = @usernameToMatch ) 
				or ( ru.Username like ''%'' + @usernameToMatch + ''%'' ) 
				or ( @usernameToMatch like ''%'' + ru.Username + ''%'' ) 
				or ( 3 <= DIFFERENCE( ru.Username, @usernameToMatch ) ) 
			) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetAccountsForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetAccountsForUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select o.ApplicationName, o.Username, o.Provider, o.ProviderUsername 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserOAuthLogin() as o on ( 
			( u.ApplicationName = o.ApplicationName ) 
			and ( u.Username = o.Username ) 
			and ( u.WhenCreated = o.WhenUserCreated ) 
		) 
		where ( 
			( o.ApplicationName = @applicationName ) 
			and ( o.Username = @username ) 
		) 
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetAllUsers]( @applicationName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select * 
		from [Security].GetCurrentMembership() 
		where ( 
			( ApplicationName = @applicationName ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCreateDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetCreateDate]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].WhenUserCreatedByUsername( @applicationName, @username ) as ''GetCreateDate''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetDeniedRoleRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetDeniedRoleRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) = null, @roleName nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	select * 
		from [Security].GetCurrentDeniedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @rightName is null ) 
				or ( RightName = @rightName ) 
			) 
			and ( 
				( @roleName is null ) 
				or ( RoleName = @roleName ) 
			) 
		)
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetDeniedUserRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetDeniedUserRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) = null, @username nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	select * 
		from [Security].GetCurrentDeniedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @rightName is null ) 
				or ( RightName = @rightName ) 
			) 
			and ( 
				( @username is null ) 
				or ( Username = @username ) 
			) 
		)
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetGrantedRoleRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetGrantedRoleRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) = null, @roleName nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	select * 
		from [Security].GetCurrentGrantedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @rightName is null ) 
				or ( RightName = @rightName ) 
			) 
			and ( 
				( @roleName is null ) 
				or ( RoleName = @roleName ) 
			) 
		)
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetGrantedUserRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetGrantedUserRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) = null, @username nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	select * 
		from [Security].GetCurrentGrantedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @rightName is null ) 
				or ( RightName = @rightName ) 
			) 
			and ( 
				( @username is null ) 
				or ( Username = @username ) 
			) 
		)
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsRoleDeniedRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsRoleDeniedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @roleName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	select top 1 [Security].RoleDeniedRight( @applicationName, @rightName, @roleName ) as ''IsRoleDeniedRight''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsRoleGrantedRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsRoleGrantedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @roleName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	select top 1 [Security].RoleGrantedRight( @applicationName, @rightName, @roleName ) as ''IsRoleGrantedRight''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserConfirmed]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsUserConfirmed]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @username = [Security].TrimToNull( @username ) 

	select top 1 [Security].IsUserConfirmed( @applicationName, @username ) as ''IsConfirmed'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserDeniedRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsUserDeniedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserDeniedRight( @applicationName, @rightName, @username ) as ''IsUserDeniedRight''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserGrantedRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsUserGrantedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserGrantedRight( @applicationName, @rightName, @username ) as ''IsUserGrantedRight''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetIsUserInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetIsUserInRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].IsUserInRole( @applicationName, @roleName, @username ) as ''GetIsUserInRole'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastPasswordFailureDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetLastPasswordFailureDate]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].LastUserLoginFailure( @applicationName, @username ) as ''GetLastPasswordFailureDate''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLockedOut]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetLastUserLockedOut]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].LastUserLockedOut( @applicationName, @username ) as ''LastLockedOutDate'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLoginFailure]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetLastUserLoginFailure]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].LastUserLoginFailure( @applicationName, @username ) as ''LastUserLoginFailure'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetLastUserLoginSuccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetLastUserLoginSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].LastUserLoginSuccess( @applicationName, @username ) as ''LastLoginSuccess'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetNumberOfUsersOnline]( @applicationName nvarchar( 255 ), @userIsOnlineTimeWindow int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select Count( * ) as ''NumberOfUsersOnline'' 
		from [Security].OnlineMembershipUser( @applicationName, @userIsOnlineTimeWindow ) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetOAuthTokenSecretByToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetOAuthTokenSecretByToken]( @applicationName nvarchar( 255 ), @token nvarchar( 128 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	select top 1 [Security].OAuthTokenSecretByToken( @applicationName, @token ) as ''Secret'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetOnlineMembershipUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetOnlineMembershipUser]( @applicationName nvarchar( 255 ), @userIsOnlineTimeWindow int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select * 
		from [Security].OnlineMembershipUser( @applicationName, @userIsOnlineTimeWindow ) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetPassword]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @passwordAnswer nvarchar( 512 ), @failIfNotApproved bit = 1 ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @passwordAnswer = [Security].TrimToNull( @passwordAnswer )

	declare @output int
	set @output = 11
	declare @passwd nvarchar( 512 )
	set @passwd = null

	if ( ( @applicationName is null ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @passwordAnswer is null ) begin 
		set @output = 4
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ValidateUserPasswordAnswer @applicationName, @username, @passwordAnswer, @failIfNotApproved 
		if ( 0 = @output ) begin 
			set @passwd = ( select top 1 l.[Password] 
				from [Security].GetCurrentUser() as u 
				join [Security].GetCurrentUserLocalLogin() as l on ( 
					( u.ApplicationName = l.ApplicationName ) 
					and ( u.Username = l.Username ) 
					and ( u.WhenCreated = l.WhenCreated ) 
				) 
				where ( 
					( l.ApplicationName = @applicationName ) 
					and ( l.Username = @username ) 
				) 
			)
			if ( @passwd is null ) begin 
				set @output = 2
			end
		end
	end

	select top 1 @passwd as ''Password''

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPasswordChangedDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetPasswordChangedDate]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 WhenPasswordUpdated 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetPasswordFailuresSinceLastSuccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetPasswordFailuresSinceLastSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserLoginFailureSinceLastUserLoginSuccess( @applicationName, @username ) as ''GetPasswordFailuresSinceLastSuccess'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )

	select * 
		from [Security].GetCurrentRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @rightName is null ) 
				or ( RightName = @rightName ) 
			)
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRightByResource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetRightByResource]( @applicationName nvarchar( 255 ), @resource nvarchar( 1024 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @resource = [Security].TrimToNull( @resource )

	select * 
		from [Security].GetCurrentRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( 
					( @resource is null ) 
					and ( [Resource] is null ) 
				) 
				or ( [Resource] = @resource ) 
			)
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRightExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetRightExists]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )

	select top 1 [Security].DoesRightExist( @applicationName, @rightName ) as ''RightExists'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetRoles]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) = null ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	select * 
		from [Security].GetCurrentRole() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( 
				( @roleName is null ) 
				or ( RoleName = @roleName ) 
			) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetRolesForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetRolesForUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select r.RoleName 
		from [Security].GetCurrentRoleUser() as ru 
		join [Security].GetCurrentRole() as r on ( 
			( ru.ApplicationName = r.ApplicationName ) 
			and ( ru.RoleName = r.RoleName ) 
			and ( ru.WhenRoleCreated = r.WhenCreated ) 
		) 
		where ( 
			( ru.ApplicationName = @applicationName ) 
			and ( ru.Username = @username ) 
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	select top 1 * 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[GetUserByUserId]( @applicationName nvarchar( 255 ), @userId int, @updateLastActive bit ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	if ( 1 = @updateLastActive ) begin 
		update [Security].[User] 
			set WhenLastActive = GetDate() 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( UserId = @userId ) 
			) 
	end
	select * 
		from [Security].GetCurrentMembership()
		where ( 
			( ApplicationName = @applicationName ) 
			and ( UserId = @userId ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserByUsername]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[GetUserByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @updateLastActive bit ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	if ( 1 = @updateLastActive ) begin 
		update [Security].[User] 
			set WhenLastActive = GetDate() 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			) 
	end
	select top 1 * 
		from [Security].GetCurrentMembership()
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdByUserLoginResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserIdByUserLoginResetToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	select top 1 [Security].UserIdByUserLoginResetToken( @applicationName, @token, @tokenLifetime ) as ''UserId'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromPasswordResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserIdFromPasswordResetToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	select top 1 [Security].UserIdByUserLoginResetToken( @applicationName, @token, @tokenLifetime ) as ''UserId''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromUsername]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserIdFromUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserIdFromUsername( @applicationName, @username ) as ''UserId''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserIdFromUserOAuthLogin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserIdFromUserOAuthLogin]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )

	select top 1 [Security].UserIdFromUserOAuthLogin( @applicationName, @provider, @providerUsername ) as ''UserId'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLocalLoginPasswordOption]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserLocalLoginPasswordOption]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 PasswordFormat, PasswordAlgorithm, PasswordSalt, WhenPasswordUpdated, WhoPasswordUpdated 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLoginFailureSinceLastUserLoginSuccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserLoginFailureSinceLastUserLoginSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserLoginFailureSinceLastUserLoginSuccess( @applicationName, @username ) as ''UserLoginFailureSinceLastUserLoginSuccess'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserLoginResetTokenByUsername]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserLoginResetTokenByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].UserLoginResetTokenByUsername( @applicationName, @username ) as ''Token'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUsernameByEmail]( @applicationName nvarchar( 255 ), @email nvarchar( 512 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @email = [Security].TrimToNull( @email )

	select top 1 u.Username 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserEmail() as e on ( 
			( u.ApplicationName = e.ApplicationName ) 
			and ( u.Username = e.Username ) 
			and ( u.WhenCreated = e.WhenUserCreated ) 
		) 
		where ( 
			( e.ApplicationName = @applicationName ) 
			and ( e.Email = @email ) 
		) 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByUserConfirmationToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUsernameByUserConfirmationToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @token = [Security].TrimToNull( @token ) 

	select top 1 [Security].UsernameByUserConfirmationToken( @applicationName, @token, @tokenLifetime ) as ''Username'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsernameByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUsernameByUserId]( @applicationName nvarchar( 255 ), @userId int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select top 1 [Security].UsernameFromUserId( @applicationName, @userId ) as ''Username'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserNameFromId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserNameFromId]( @applicationName nvarchar( 255 ), @userId int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select top 1 [Security].UsernameFromUserId( @applicationName, @userId ) as ''Username'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUserPasswordQuestionAnswerOption]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUserPasswordQuestionAnswerOption]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 PasswordQuestion, PasswordAnswerFormat, PasswordAnswerAlgorithm, PasswordAnswerSalt, WhenPasswordQuestionAnswerUpdated, WhoPasswordQuestionAnswerUpdated 
		from [Security].GetCurrentUserPasswordQuestionAnswer() as p 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = p.ApplicationName ) 
			and ( u.Username = p.Username ) 
			and ( u.WhenCreated = p.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetUsersInRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	select ru.Username 
		from [Security].GetCurrentRoleUser() as ru 
		join [Security].GetCurrentRole() as r on ( 
			( ru.ApplicationName = r.ApplicationName ) 
			and ( ru.RoleName = r.RoleName ) 
			and ( ru.WhenRoleCreated = r.WhenCreated ) 
		) 
		where ( 
			( ru.ApplicationName = @applicationName ) 
			and ( ru.RoleName = @roleName ) 
		) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetWhenUserCreatedByUsername]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GetWhenUserCreatedByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].WhenUserCreatedByUsername( @applicationName, @username ) as ''WhenCreated''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantRoleRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GrantRoleRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @roleName varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 3
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].DeniedRoleRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( RoleName = @roleName ) 
			) 
		if ( not exists ( select top 1 1 
			from [Security].[Right] as r 
			join [Security].GrantedRoleRight as g on ( 
				( r.ApplicationName = g.ApplicationName ) 
				and ( r.RightName = g.RightName ) 
				and ( r.WhenCreated = g.WhenRightCreated ) 
			) 
			where ( 
				( r.WhenDeleted is null ) 
				and ( g.WhenDeleted is null ) 
				and ( g.ApplicationName = @applicationName ) 
				and ( g.RightName = @rightName ) 
				and ( g.RoleName = @roleName ) 
			) 
		) ) begin 
			declare @whenRightCreated datetime
			set @whenRightCreated = [Security].WhenRightCreated( @applicationName, @rightName )
			insert into 
				[Security].GrantedRoleRight ( ApplicationName, RightName, WhenRightCreated, RoleName, WhenCreated, WhoCreated ) 
				values ( @applicationName, @rightName, @whenRightCreated, @roleName, GetDate(), @who ) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantUserRight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[GrantUserRight]( @applicationName varchar( 255 ), @rightName varchar( 255 ), @username varchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 4
	end else if ( ( @rightName is null ) or ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) ) begin 
		set @output = 1
	end else begin 
		update [Security].DeniedUserRight 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
				and ( Username = @username ) 
			) 
		if ( not exists ( select top 1 1 
			from [Security].[Right] as r 
			join [Security].GrantedUserRight as g on ( 
				( r.ApplicationName = g.ApplicationName ) 
				and ( r.RightName = g.RightName ) 
				and ( r.WhenCreated = g.WhenRightCreated ) 
			) 
			where ( 
				( r.WhenDeleted is null ) 
				and ( g.WhenDeleted is null ) 
				and ( g.ApplicationName = @applicationName ) 
				and ( g.RightName = @rightName ) 
				and ( g.Username = @username ) 
			) 
		) ) begin 
			declare @whenRightCreated datetime
			set @whenRightCreated = [Security].WhenRightCreated( @applicationName, @rightName )
			insert into 
				[Security].GrantedUserRight ( ApplicationName, RightName, WhenRightCreated, Username, WhenCreated, WhoCreated ) 
				values ( @applicationName, @rightName, @whenRightCreated, @username, GetDate(), @who ) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasLocalAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[HasLocalAccount]( @applicationName nvarchar( 255 ), @userId int ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )

	select top 1 [Security].HasUserLocalLoginByUserId( @applicationName, @userId ) as ''HasLocalAccount'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsConfirmed]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[IsConfirmed]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	select top 1 [Security].IsUserConfirmed( @applicationName, @username ) as ''IsConfirmed'' 
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LogoutUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[LogoutUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	insert into 
		[Security].UserLogout ( ApplicationName, Username, WhenUserCreated, WhenLogout ) 
		values ( @applicationName, @username, [Security].WhenUserCreatedByUsername( @applicationName, @username ), GETDATE() ) 

	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RemoveUserRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [Security].[RemoveUserRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @roleName is null ) begin 
		set @output = 1
	end else if ( @username is null ) begin 
		set @output = 2
	end else begin 
		update [Security].RoleUser 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( RoleName = @roleName ) 
				and ( Username = @username ) 
			) 
		set @output = ( select Count( * ) 
			from [Security].GetCurrentRoleUser() as ru 
			join [Security].GetCurrentRole() as r on ( 
				( ru.ApplicationName = r.ApplicationName ) 
				and ( ru.RoleName = r.RoleName ) 
				and ( ru.WhenRoleCreated = r.WhenCreated ) 
			) 
			where ( 
				( ru.ApplicationName = @applicationName ) 
				and ( ru.RoleName = @roleName ) 
				and ( ru.Username = @username ) 
			) 
		)
		if ( 0 <> @output ) begin 
			set @output = 11
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ReplaceOAuthRequestTokenWithAccessToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ReplaceOAuthRequestTokenWithAccessToken]( @applicationName nvarchar( 255 ), @requestToken nvarchar( 128 ), @accessToken nvarchar( 128 ), @accessTokenSecret nvarchar( 128 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @requestToken = [Security].TrimToNull( @requestToken )
	set @accessToken = [Security].TrimToNull( @accessToken )
	set @accessTokenSecret = [Security].TrimToNull( @accessTokenSecret )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	exec @output = [Security].DeleteOAuthTokenByToken @applicationName, @requestToken, @who
	if ( 0 = @output ) begin 
		exec @output = [Security].SetOAuthToken @applicationName, @accessToken, @accessTokenSecret, @who
	end else begin 
		set @output = 11
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ResetPasswordWithToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[ResetPasswordWithToken]( 
	@applicationName nvarchar( 255 ), 
	@token nvarchar( 256 ), @tokenLifetime int, 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@failIfNotApproved bit = 1 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )
	set @password = [Security].TrimToNull( @password )
	set @passwordAlgorithm = [Security].TrimToNull( @passwordAlgorithm )
	set @passwordSalt = [Security].TrimToNull( @passwordSalt )

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime < 1 ) ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( @password is null ) begin 
		set @output = 2 
	end else begin 
		declare @username nvarchar( 255 )
		set @username = [Security].UsernameByUserLoginResetToken( @applicationName, @token, @tokenLifetime )
		if ( 
			( @username is null ) 
			or ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
			or ( 
				( 1 = @failIfNotApproved ) 
				and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
			) 
			or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
		) begin 
			set @output = 8
		end else if ( 0 = [Security].IsValidUserLoginResetToken( @applicationName, @username, @token, @tokenLifetime ) ) begin 
			set @output = 9
		end else begin 
			exec @output = [Security].VerifyUserLoginResetToken @applicationName, @username, @token, @tokenLifetime, @failIfNotApproved, @username 
		end
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserLocalLoginPassword @applicationName, @username, @password, @passwordFormat, @passwordAlgorithm, @passwordSalt, @failIfNotApproved, @username
			if ( 0 = @output ) begin 
				exec @output = [Security].SetUserLockedOut @applicationName, @username, 0, @username
			end
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[RoleExists]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	select top 1 [Security].DoesRoleExist( @applicationName, @roleName ) as ''RoleExists''
	return 0
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetConfirmationToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetConfirmationToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].SetUserConfirmationToken @applicationName, @username, @token, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetOAuthToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetOAuthToken]( @applicationName nvarchar( 255 ), @token nvarchar( 128 ), @requestTokenSecret nvarchar( 128 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )
	set @requestTokenSecret = [Security].TrimToNull( @requestTokenSecret )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	declare @tokenSecret nvarchar( 128 )
	set @tokenSecret = [Security].OAuthTokenSecretByToken( @applicationName, @token )
	if ( @tokenSecret is not null ) begin 
		if ( @tokenSecret = @requestTokenSecret ) begin 
			set @output = 0
		end else begin 
			exec @output = [Security].DeleteOAuthTokenByToken @applicationName, @token, @who
			if ( 0 = @output ) begin 
				insert into 
					[Security].OAuthToken ( ApplicationName, Token, WhenCreated, WhoCreated, [Secret] ) 
					values ( @applicationName, @token, GetDate(), @who, @requestTokenSecret ) 
			end else begin 
				set @output = 11
			end
		end
	end else begin 
		insert into 
			[Security].OAuthToken ( ApplicationName, Token, WhenCreated, WhoCreated, [Secret] ) 
			values ( @applicationName, @token, GetDate(), @who, @requestTokenSecret ) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetPassword]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@failIfNotApproved bit = 1, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @password = [Security].TrimToNull( @password )
	set @passwordAlgorithm = [Security].TrimToNull( @passwordAlgorithm )
	set @passwordSalt = [Security].TrimToNull( @passwordSalt )
	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @failIfNotApproved is null ) begin 
		set @output = 11
	end else if ( @password is null ) begin 
		set @output = 2
	end else if ( @passwordFormat is null ) begin 
		set @output = 11
	end else if ( ( 0 < @passwordFormat ) and ( @passwordAlgorithm is null ) ) begin 
		set @output = 11
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].SetUserLocalLoginPassword @applicationName, @username, @password, @passwordFormat, @passwordAlgorithm, @passwordSalt, @failIfNotApproved, @who
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetPasswordResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetPasswordResetToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].SetUserLoginResetToken @applicationName, @username, @token, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetRightResource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetRightResource]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @resource nvarchar( 1024 ) = null, @requireUniqueResource bit = 1, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @resource = [Security].TrimToNull( @resource )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end
	if ( @requireUniqueResource is null ) begin 
		set @requireUniqueResource = 1
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @rightName is null ) begin 
		set @output = 1
	end else if ( ( 1 = @requireUniqueResource ) and ( @resource is null ) ) begin 
		set @output = 2
	end else if ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) begin 
		set @output = 1
	end else if ( ( 1 = @requireUniqueResource ) and ( 0 = [Security].IsResourceUnique( @applicationName, @rightName, @resource ) ) ) begin 
		set @output = 6
	end else begin 
		declare @oldResource nvarchar( 1024 )
		set @oldResource = ( select top 1 [Resource] 
			from [Security].GetCurrentRight() 
			where ( 
				( ApplicationName = @applicationName ) 
				and ( RightName = @rightName ) 
			) 
		)
		if ( IsNull( @oldResource, '''' ) <> IsNull( @resource, '''' ) ) begin 
			update [Security].RightResource 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who
				where ( 
					( ApplicationName = @applicationName ) 
					and ( RightName = @rightName ) 
				) 
			if ( @resource is not null ) begin 
				declare @whenRightCreated datetime
				set @whenRightCreated = GetDate()
				insert into 
					[Security].[RightResource] ( ApplicationName, RightName, WhenRightCreated, WhenCreated, WhoCreated, [Resource] ) 
					values ( @applicationName, @rightName, @whenRightCreated, @whenRightCreated, @who, @resource ) 
			end
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [Security].[SetUserApproval]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @isApproved bit = 0, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 1
	end else if ( @isApproved is null ) begin 
		set @output = 11
	end else if ( 1 = @isApproved ) begin 
		if ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) begin 
			declare @origin datetime
			set @origin = [Security].WhenUserCreatedByUsername( @applicationName, @username ) 
			insert into 
				[Security].UserApproval ( ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated ) 
				values ( @applicationName, @username, @origin, GetDate(), @who ) 
			update [Security].UserConfirmationToken 
				set WhenConfirmed = GetDate()
				where ( 
					( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
					and ( WhenConfirmed is null ) 
					and ( WhenDeleted is null ) 
				) 
		end
		if ( 1 = [Security].IsUserApproved( @applicationName, @username ) ) begin 
			set @output = 0
		end
	end else if ( 0 = @isApproved ) begin 
		update [Security].UserApproval 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			) 
		if ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) begin 
			set @output = 0
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserComment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserComment]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @comment nvarchar( 4000 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @comment = [Security].TrimToNull( @comment )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 1
	end else begin 
		declare @oldComment nvarchar( 4000 )
		set @oldComment = ( select top 1 Comment 
			from [Security].GetCurrentUserComment() as c 
			join [Security].GetCurrentUser() as u on ( 
				( c.ApplicationName = u.ApplicationName ) 
				and ( c.Username = u.Username ) 
				and ( c.WhenUserCreated = u.WhenCreated ) 
			) 
			where ( 
				( c.ApplicationName = @applicationName ) 
				and ( c.Username = @username ) 
			) 
		) 
		if ( IsNull( @oldComment, '''' ) <> IsNull( @comment, '''' ) ) begin 
			update [Security].UserComment 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
				) 
			if ( @comment is not null ) begin 
				declare @origin datetime
				set @origin = [Security].WhenUserCreatedByUsername( @applicationName, @username ) 
				insert into 
					[Security].UserComment ( ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated, Comment ) 
					values ( @applicationName, @username, @origin, GetDate(), @who, @comment ) 
			end
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserConfirmationToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserConfirmationToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @who nvarchar( 255 ) ) as
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @username = [Security].TrimToNull( @username ) 
	set @token = [Security].TrimToNull( @token ) 
	set @who = [Security].TrimToNull( @who ) 
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @token is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUserConfirmationToken() as t 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = t.ApplicationName ) 
			and ( u.Username = t.Username ) 
			and ( u.WhenCreated = t.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenConfirmed is null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Username = @username ) 
			and ( t.Token = @token ) 
		) 
	) ) begin 
		set @output = 10
	end else begin 
		declare @whenUserCreated datetime
		set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username ) 
		declare @getDate datetime
		set @getDate = GetDate()
		update [Security].UserConfirmationToken 
			set WhenDeleted = @getDate, 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( WhenUserCreated = @whenUserCreated ) 
				and ( WhenConfirmed = null ) 
			) 
		insert into 
			[Security].UserConfirmationToken ( ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated, Token ) 
			values ( @applicationName, @username, @whenUserCreated, @getDate, @who, @token ) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserEmail]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @email nvarchar( 256 ), @requireUniqueEmail bit = 0, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @email = [Security].TrimToNull( @email )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( ( 1 = @requireUniqueEmail ) and ( @email is null ) ) begin 
		set @output = 5
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 1
	end else if ( ( 1 = @requireUniqueEmail ) and ( 0 = [Security].IsEmailUnique( @applicationName, @username, @email ) ) ) begin 
		set @output = 7
	end else begin 
		declare @oldEmail nvarchar( 256 )
		set @oldEmail = ( select top 1 e.Email 
			from [Security].GetCurrentUserEmail() as e 
			join [Security].GetCurrentUser() as u on ( 
				( e.ApplicationName = u.ApplicationName ) 
				and ( e.Username = u.Username ) 
				and ( e.WhenUserCreated = u.WhenCreated ) 
			) 
			where ( 
				( e.ApplicationName = @applicationName ) 
				and ( e.Username = @username ) 
			) 
		) 
		if ( IsNull( @oldEmail, '''' ) <> IsNull( @email, '''' ) ) begin 
			update [Security].UserEmail 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
				) 
			if ( @email is not null ) begin 
				declare @origin datetime
				set @origin = [Security].WhenUserCreatedByUsername( @applicationName, @username ) 
				insert into 
					[Security].UserEmail ( ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated, Email ) 
					values ( @applicationName, @username, @origin, GetDate(), @who, @email ) 
			end
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLocalLoginPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserLocalLoginPassword]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@failIfNotApproved bit = 1, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @password = [Security].TrimToNull( @password )
	set @passwordAlgorithm = [Security].TrimToNull( @passwordAlgorithm )
	set @passwordSalt = [Security].TrimToNull( @passwordSalt )
	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @failIfNotApproved is null ) begin 
		set @output = 11
	end else if ( @password is null ) begin 
		set @output = 2
	end else if ( @passwordFormat is null ) begin 
		set @output = 11
	end else if ( ( 0 < @passwordFormat ) and ( @passwordAlgorithm is null ) ) begin 
		set @output = 11
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		declare @when datetime
		set @when = GetDate()
		declare @whenUserCreated datetime
		set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username ) 
		if ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) begin 
			insert into 
				[security].UserLocalLogin ( 
					ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated, 
					[Password], PasswordFormat, PasswordAlgorithm, PasswordSalt, 
					WhenPasswordUpdated, WhoPasswordUpdated 
				) values ( 
					@applicationName, @username, @whenUserCreated, @when, @who, 
					@password, @passwordFormat, @passwordAlgorithm, @passwordSalt, 
					@when, @who 
				) 
		end else begin 
			update [security].UserLocalLogin 
				set [Password] = @password, 
					PasswordFormat = @passwordFormat, 
					PasswordAlgorithm = @passwordAlgorithm, 
					PasswordSalt = @passwordSalt, 
					WhenPasswordUpdated = @when, 
					WhoPasswordUpdated = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
					and ( WhenUserCreated = @whenUserCreated ) 
				) 
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLocalPasswordWithUserLoginResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserLocalPasswordWithUserLoginResetToken]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@token nvarchar( 256 ), @tokenLifetime int, 
	@password nvarchar( 512 ), @passwordFormat int, @passwordAlgorithm nvarchar( 255 ), @passwordSalt nvarchar( 255 ), 
	@failIfNotApproved bit = 1, 
	@who nvarchar( 255 ) 
) as 

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime < 1 ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @token is null ) begin 
		set @output = 9
	end else if 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].VerifyUserLoginResetToken @applicationName, @username, @token, @tokenLifetime, @failIfNotApproved, @who
		if ( 0 = @output ) begin
			exec @output = [Security].SetUserLocalLoginPassword @applicationName, @username, @password, @passwordFormat, @passwordAlgorithm, @passwordSalt, @failIfNotApproved, @who 
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLockedOut]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserLockedOut]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @isLockedOut bit = 1, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @isLockedOut is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		if ( 1 = @isLockedOut ) begin 
			if ( 0 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
				insert into 
					[Security].UserLockedOut ( ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated ) 
					values ( @applicationName, @username, [Security].WhenUserCreatedByUsername( @applicationName, @username ), GetDate(), @who ) 
			end
			if ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
				set @output = 0
			end else begin 
				set @output = 11
			end
		end else if ( 0 = @isLockedOut ) begin 
			update [Security].UserLockedOut 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Username = @username ) 
				) 
			if ( 0 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
				set @output = 0
			end else begin 
				set @output = 11
			end
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserLoginResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserLoginResetToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @token is null ) begin 
		set @output = 9
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		declare @whenUserLoginCreated datetime
		set @whenUserLoginCreated = [Security].UserLocalLoginWhenCreatedByUsername( @applicationName, @username ) 
		update [Security].UserLoginResetToken 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( WhenLoginCreated = @whenUserLoginCreated ) 
				and ( WhenVerified is null ) 
			) 
		insert into 
			[Security].UserLoginResetToken ( ApplicationName, Username, WhenLoginCreated, WhenCreated, WhoCreated, Token ) 
			values ( @applicationName, @username, @whenUserLoginCreated, GetDate(), @who, @token ) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserOAuthLogin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserOAuthLogin]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( ( @provider is null ) or ( @providerUsername is null ) ) begin 
		set @output = 9
	end else if ( @username is null ) begin 
		set @output = 1
	end else begin 
		if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
			declare @userId int
			exec @output = [Security].CreateUser @applicationName, @username, @userId output, @who 
		end else begin 
			set @output = 0
		end
		if ( 0 = @output ) begin 
			update [Security].UserOAuthLogin 
				set WhenDeleted = GetDate(), 
					WhoDeleted = @who 
				where ( 
					( WhenDeleted is null ) 
					and ( ApplicationName = @applicationName ) 
					and ( Provider = @provider ) 
					and ( ProviderUsername = @providerUsername ) 
				) 
			declare @whenUserCreated datetime
			set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
			insert into 
				[Security].UserOAuthLogin ( ApplicationName, Username, WhenUserCreated, Provider, ProviderUsername, WhenCreated, WhoCreated ) 
				values ( @applicationName, @username, @whenUserCreated, @provider, @providerUsername, GetDate(), @who ) 
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[SetUserPasswordQuestionAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[SetUserPasswordQuestionAnswer]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 255 ), 
	@passwordQuestion nvarchar( 512 ), 
	@passwordAnswer nvarchar( 512 ), @passwordAnswerFormat int, @passwordAnswerAlgorithm nvarchar( 255 ), @passwordAnswerSalt nvarchar( 255 ), 
	@requirePasswordQuestionAndAnswer bit, 
	@failIfNotApproved bit, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @passwordQuestion = [Security].TrimToNull( @passwordQuestion )
	set @passwordAnswer = [Security].TrimToNull( @passwordAnswer )
	set @passwordAnswerAlgorithm = [Security].TrimToNull( @passwordAnswerAlgorithm )
	set @passwordAnswerSalt = [Security].TrimToNull( @passwordAnswerSalt )

	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = USER_NAME()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @passwordAnswerFormat is null ) or ( @requirePasswordQuestionAndAnswer is null ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( ( 0 < @passwordAnswerFormat ) and ( @passwordAnswerAlgorithm is null ) ) begin 
		set @output = 11
	end if ( ( 1 = @requirePasswordQuestionAndAnswer ) and ( @passwordQuestion is null ) ) begin 
		set @output = 3
	end else if ( ( 1 = @requirePasswordQuestionAndAnswer ) and ( @passwordAnswer is null ) ) begin 
		set @output = 4
	end else if 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
	begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		declare @when datetime
		set @when = GetDate()
		declare @whenUserCreated datetime
		set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
		update [Security].UserPasswordQuestionAnswer 
			set WhenDeleted = GetDate(), 
				WhoDeleted = @who 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			) 
		if ( ( @passwordQuestion is not null ) and ( @passwordAnswer is not null ) ) begin 
			insert into 
				[Security].UserPasswordQuestionAnswer ( 
					ApplicationName, Username, WhenUserCreated, WhenCreated, WhoCreated, 
					PasswordQuestion, PasswordAnswer, PasswordAnswerFormat, PasswordAnswerAlgorithm, PasswordAnswerSalt, 
					WhenPasswordQuestionAnswerUpdated, WhoPasswordQuestionAnswerUpdated 
				) values ( 
					@applicationName, @username, @whenUserCreated, @when, @who, 
					@passwordQuestion, @passwordAnswer, @passwordAnswerFormat, @passwordAnswerAlgorithm, @passwordAnswerSalt, 
					@when, @who 
				)
		end
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[StoreOAuthRequestToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[StoreOAuthRequestToken]( @applicationName nvarchar( 255 ), @token nvarchar( 128 ), @requestTokenSecret nvarchar( 128 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )
	set @requestTokenSecret = [Security].TrimToNull( @requestTokenSecret )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else begin 
		exec @output = [Security].SetOAuthToken @applicationName, @token, @requestTokenSecret, @who 
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[UnlockUser]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name() 
	end

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else begin 
		exec @output = [Security].SetUserLockedOut @applicationName, @username, 0, @who
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateMembershipUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[UpdateMembershipUser]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 256 ), 
	@isApproved bit, 
	@email nvarchar( 256 ), @requireUniqueEmail bit, 
	@comment nvarchar( 4000 ), 
	@lastActivity datetime, @lastLogin datetime, 
	@who nvarchar( 255 ) 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @email = [Security].TrimToNull( @email )
	set @comment = [Security].TrimToNull( @comment )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name() 
	end

	declare @output int
	set @output = 11

	if 
		( @applicationName is null ) 
		or ( @isApproved is null ) 
		or ( @requireUniqueEmail is null ) 
	begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		declare @lastActive datetime
		set @lastActive = [Security].UserLastActive( @applicationName, @username )
		if ( @lastActivity is not null ) begin 
			if ( 
				( @lastActive is null ) 
				or ( @lastActive < @lastActivity ) 
			) begin 
				update [Security].[User] 
					set WhenLastActive = @lastActivity 
					where ( 
						( WhenDeleted is null ) 
						and ( ApplicationName = @applicationName ) 
						and ( Username = @username ) 
					) 
			end
		end
		if ( @lastLogin is not null ) begin 
			declare @lastLoginRecord datetime
			set @lastLoginRecord = [Security].LastUserLoginSuccess( @applicationName, @username )
			if ( 
				( @lastLogin is null ) 
				or ( @lastLoginRecord < @lastLogin ) 
			) begin 
				declare @whenUserCreated datetime
				set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
				insert into 
					[Security].UserLoginSuccess ( ApplicationName, Username, WhenUserCreated, WhenSucceeded ) 
					values ( @applicationName, @username, @whenUserCreated, @lastLogin ) 
			end
		end
		exec @output = [Security].SetUserComment @applicationName, @username, @comment, @who 
		if ( 0 = @output ) begin 
			exec @output = [Security].SetUserEmail @applicationName, @username, @email, @requireUniqueEmail, @who
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLastActive]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[UpdateUserLastActive]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		update [Security].[User] 
			set WhenLastActive = GetDate() 
			where ( 
				( WhenDeleted is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
			) 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLoginFailure]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[UpdateUserLoginFailure]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		declare @whenUserCreated datetime
		set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
		if ( @whenUserCreated is null ) begin 
			set @output = 8
		end else begin 
			insert into 
				[Security].UserLoginFailure ( ApplicationName, Username, WhenUserCreated, WhenFailed ) 
				values ( @applicationName, @username, @whenUserCreated, GetDate() ) 
			set @output = 0
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UpdateUserLoginSuccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[UpdateUserLoginSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output int
	set @output = 11

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else begin 
		declare @whenUserCreated datetime
		set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
		if ( @whenUserCreated is null ) begin 
			set @output = 8
		end else begin 
			insert into 
				[Security].UserLoginSuccess ( ApplicationName, Username, WhenUserCreated, WhenSucceeded ) 
				values ( @applicationName, @username, @whenUserCreated, GetDate() ) 
			set @output = 0
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidatePasswordAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ValidatePasswordAnswer]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @passwordAnswer nvarchar( 512 ), @failIfNotApproved bit = 1 ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @passwordAnswer = [Security].TrimToNull( @passwordAnswer )
	if ( @failIfNotApproved is null ) begin 
		set @failIfNotApproved = 1
	end

	declare @output int
	set @output = 8

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ValidateUserPasswordAnswer @applicationName, @username, @passwordAnswer, @failIfNotApproved
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [Security].[ValidateUser]( 
	@applicationName nvarchar( 255 ), @username nvarchar( 256 ), 
	@password nvarchar( 512 ), 
	@updateLastLogin bit, @updateLastActive bit, @failIfNotApproved bit = 1, 
	@maxInvalidPasswordAttempts int, @passwordAttemptWindow int 
) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output int
	set @output = 11

	if ( 
		( @applicationName is null ) 
		or ( @failIfNotApproved is null ) 
		or ( @maxInvalidPasswordAttempts is null ) 
		or ( @maxInvalidPasswordAttempts < 1 ) 
		or ( @passwordAttemptWindow is null ) 
		or ( @passwordAttemptWindow < 1 ) 
	) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @password is null ) begin 
		set @output = 2
	end else if ( 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		exec @output = [Security].ValidateUserLocalLoginPassword @applicationName, @username, @password, @failIfNotApproved 
		if ( 0 = @output ) begin 
			if ( 1 = @updateLastLogin ) begin 
				declare @whenUserCreated datetime
				set @whenUserCreated = [Security].WhenUserCreatedByUsername( @applicationName, @username )
				insert into 
					[Security].UserLoginSuccess ( ApplicationName, Username, WhenUserCreated, WhenSucceeded ) 
					values ( @applicationName, @username, @whenUserCreated, GetDate() ) 
			end
			if ( 1 = @updateLastActive ) begin 
				update [Security].[User] 
					set WhenLastActive = GetDate() 
					where ( 
						( WhenDeleted is null ) 
						and ( ApplicationName = @applicationName ) 
						and ( Username = @username ) 
					) 
			end
		end else begin 
			exec [Security].UpdateUserLoginFailure @applicationName, @username 
			declare @lastSuccess datetime
			set @lastSuccess = [Security].LastUserLoginSuccess( @applicationName, @username )
			declare @failures int
			set @failures = ( select Count( f.WhenFailed ) 
				from [Security].GetCurrentUser() as u 
				join [Security].UserLoginFailure as f on ( 
					( u.ApplicationName = f.ApplicationName ) 
					and ( u.Username = f.Username ) 
					and ( u.WhenCreated = f.WhenUserCreated ) 
				) 
				where ( 
					( f.ApplicationName = @applicationName ) 
					and ( f.Username = @username ) 
					and ( 
						( @lastSuccess is null ) 
						or ( @lastSuccess <= f.WhenFailed ) 
					) 
					and ( GetDate() <= DateAdd( MI, @passwordAttemptWindow, f.WhenFailed ) ) 
				) 
			) 
			if ( ( @failures is not null ) and ( @maxInvalidPasswordAttempts <= @failures ) ) begin 
				exec [Security].SetUserLockedOut @applicationName, @username, 1, @username 
			end
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUserLocalLoginPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ValidateUserLocalLoginPassword]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @password nvarchar( 512 ), @failIfNotApproved bit = 1 ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @password = [Security].TrimToNull( @password ) 

	declare @output int
	set @output = 8

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @failIfNotApproved is null ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @password is null ) begin 
		set @output = 2
	end else if 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
	begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
			and ( l.[Password] COLLATE Latin1_General_CS_AS = @password ) 
		) 
	) ) begin 
		set @output = 0
	end else begin 
		set @output = 8
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[ValidateUserPasswordAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [Security].[ValidateUserPasswordAnswer]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @passwordAnswer nvarchar( 512 ), @failIfNotApproved bit = 1 ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @passwordAnswer = [Security].TrimToNull( @passwordAnswer )

	declare @output int
	set @output = 8

	if ( @applicationName is null ) begin 
		set @output = 11
	end else if ( @failIfNotApproved is null ) begin 
		set @output = 11
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUserPasswordQuestionAnswer() as p 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = p.ApplicationName ) 
			and ( u.Username = p.Username ) 
			and ( u.WhenCreated = p.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
			and ( p.[PasswordAnswer] COLLATE Latin1_General_CS_AS = @passwordAnswer ) 
		) 
	) ) begin 
		set @output = 0
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[VerifyUserLoginResetToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [Security].[VerifyUserLoginResetToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int, @failIfNotApproved bit = 1, @who nvarchar( 255 ) ) as 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )
	set @who = [Security].TrimToNull( @who )
	if ( @who is null ) begin 
		set @who = User_Name()
	end

	declare @output int
	set @output = 11

	if ( ( @applicationName is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime < 1 ) or ( @failIfNotApproved is null ) ) begin 
		set @output = 11
	end else if ( @username is null ) begin 
		set @output = 1
	end else if ( @token is null ) begin 
		set @output = 9
	end else if 
		( 0 = [Security].DoesUserExist( @applicationName, @username ) ) 
		or ( 0 = [Security].HasUserLocalLoginByUsername( @applicationName, @username ) ) 
		or ( 0 = [Security].IsValidUserLoginResetToken( @applicationName, @username, @token, @tokenLifetime ) ) 
	begin 
		set @output = 8
	end else if ( 
		( 1 = @failIfNotApproved ) 
		and ( 0 = [Security].IsUserApproved( @applicationName, @username ) ) 
	) begin 
		set @output = 8
	end else begin 
		update [Security].UserLoginResetToken 
			set WhenVerified = GetDate() 
			where ( 
				( WhenVerified is null ) 
				and ( ApplicationName = @applicationName ) 
				and ( Username = @username ) 
				and ( Token = @token ) 
				and ( GetDate() <= DateAdd( MI, @tokenLifetime, WhenCreated ) ) 
			) 
		if ( 0 = [Security].IsValidUserLoginResetToken( @applicationName, @username, @token, @tokenLifetime ) ) begin 
			set @output = 0
		end else begin 
			set @output = 11
		end
	end

	return @output
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[AllTrim]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[AllTrim]( @value nvarchar( max ) ) returns nvarchar( max ) as begin 
	if ( @value is not null ) begin 
		set @value = RTrim( LTrim( @value ) )
	end
	return @value
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesRightExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[DoesRightExist]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesRoleExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[DoesRoleExist]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentRole() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RoleName = @roleName ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserExist]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[DoesUserExist]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	declare @output bit
	set @output = 0
	if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end
	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserHaveRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE function [Security].[DoesUserHaveRight]( @applicationName varchar( 255 ), @username varchar( 255 ), @rightName varchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @rightName = [Security].TrimToNull( @rightName )

	declare @output bit
	set @output = 0

	if ( ( @applicationName is null ) or ( @rightName is null ) or ( @username is null ) ) begin 
		set @output = 0
	end else if ( 0 = [Security].DoesRightExist( @applicationName, @rightName ) ) begin 
		set @output = 0
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 0
	end else if ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 0 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentDeniedUserRight() as cdur on ( 
			( cdur.ApplicationName = cu.ApplicationName ) 
			and ( cdur.Username = cu.Username ) 
		)
		where ( 
			( cdur.ApplicationName = @applicationName ) 
			and ( cdur.Username = @username ) 
			and ( cdur.RightName = @rightName ) 
		)
	) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 0 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentRoleUser() as cru on ( 
			( cu.ApplicationName = cru.ApplicationName ) 
			and ( cu.Username = cru.Username ) 
		)
		join [Security].GetCurrentDeniedRoleRight() as cdrr on ( 
			( cdrr.ApplicationName = cru.ApplicationName ) 
			and ( cdrr.RoleName = cru.RoleName ) 
		)
		where ( 
			( cdrr.ApplicationName = @applicationName ) 
			and ( cdrr.RightName = @rightName ) 
			and ( cru.Username = @username ) 
		)
	) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentRoleUser() as cru on ( 
			( cu.ApplicationName = cru.ApplicationName ) 
			and ( cu.Username = cru.Username ) 
		)
		join [Security].GetCurrentGrantedRoleRight() as cgrr on ( 
			( cgrr.ApplicationName = cru.ApplicationName ) 
			and ( cgrr.RoleName = cru.RoleName ) 
		)
		where ( 
			( cgrr.ApplicationName = @applicationName ) 
			and ( cgrr.RightName = @rightName ) 
			and ( cru.Username = @username ) 
		)
	) ) begin 
		set @output = 1
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentGrantedUserRight() as cgur on ( 
			( cgur.ApplicationName = cu.ApplicationName ) 
			and ( cgur.Username = cu.Username ) 
		)
		where ( 
			( cgur.ApplicationName = @applicationName ) 
			and ( cgur.RightName = @rightName ) 
			and ( cgur.Username = @username ) 
		)
	) ) begin 
		set @output = 1
	end else begin 
		set @output = 0
	end

	return @output
end

' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DoesUserHaveRightByResource]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE function [Security].[DoesUserHaveRightByResource]( @applicationName varchar( 255 ), @username varchar( 255 ), @resource nvarchar( 256 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @resource = [Security].TrimToNull( @resource ) 

	declare @output bit
	set @output = 0

	if ( ( @applicationName is null ) or ( @resource is null ) or ( @username is null ) ) begin 
		set @output = 0
	end else if ( 0 = [Security].DoesUserExist( @applicationName, @username ) ) begin 
		set @output = 0
	end else if ( 1 = [Security].IsUserLockedOut( @applicationName, @username ) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 0 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentDeniedUserRight() as cdur on ( 
			( cdur.ApplicationName = cu.ApplicationName ) 
			and ( cdur.Username = cu.Username ) 
		)
		where ( 
			( cdur.ApplicationName = @applicationName ) 
			and ( cdur.Username = @username ) 
			and ( cdur.[Resource] = @resource ) 
		)
	) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 0 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentRoleUser() as cru on ( 
			( cu.ApplicationName = cru.ApplicationName ) 
			and ( cu.Username = cru.Username ) 
		)
		join [Security].GetCurrentDeniedRoleRight() as cdrr on ( 
			( cdrr.ApplicationName = cru.ApplicationName ) 
			and ( cdrr.RoleName = cru.RoleName ) 
		)
		where ( 
			( cdrr.ApplicationName = @applicationName ) 
			and ( cdrr.[Resource] = @resource ) 
			and ( cru.Username = @username ) 
		)
	) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentRoleUser() as cru on ( 
			( cu.ApplicationName = cru.ApplicationName ) 
			and ( cu.Username = cru.Username ) 
		)
		join [Security].GetCurrentGrantedRoleRight() as cgrr on ( 
			( cgrr.ApplicationName = cru.ApplicationName ) 
			and ( cgrr.RoleName = cru.RoleName ) 
		)
		where ( 
			( cgrr.ApplicationName = @applicationName ) 
			and ( cgrr.[Resource] = @resource ) 
			and ( cru.Username = @username ) 
		)
	) ) begin 
		set @output = 1
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as cu 
		join [Security].GetCurrentGrantedUserRight() as cgur on ( 
			( cgur.ApplicationName = cu.ApplicationName ) 
			and ( cgur.Username = cu.Username ) 
		)
		where ( 
			( cgur.ApplicationName = @applicationName ) 
			and ( cgur.[Resource] = @resource ) 
			and ( cgur.Username = @username ) 
		)
	) ) begin 
		set @output = 1
	end else begin 
		set @output = 0
	end

	return @output
end

' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserLocalLoginByUserId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[HasUserLocalLoginByUserId]( @applicationName nvarchar( 255 ), @userId int ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.UserId = @userId ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserLocalLoginByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[HasUserLocalLoginByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( u.ApplicationName = @applicationName ) 
			and ( u.Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[HasUserOathLoginByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[HasUserOathLoginByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserOAuthLogin() as o on ( 
			( u.ApplicationName = o.ApplicationName ) 
			and ( u.Username = o.Username ) 
			and ( u.WhenCreated = o.WhenUserCreated ) 
		) 
		where ( 
			( o.ApplicationName = @applicationName ) 
			and ( o.Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsEmailUnique]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[IsEmailUnique]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @email nvarchar( 256 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @email = [Security].TrimToNull( @email )
	declare @output bit
	set @output = 0

	if ( 
		( @applicationName is not null ) 
		and ( @username is not null ) 
		and ( @email is not null ) 
		and ( not exists ( select top 1 1 
			from [Security].GetCurrentUserEmail() as e 
			join [Security].GetCurrentUser() as u on ( 
				( e.ApplicationName = u.ApplicationName ) 
				and ( e.Username = u.Username ) 
				and ( e.WhenUserCreated = u.WhenCreated ) 
			) 
			where ( 
				( e.ApplicationName = @applicationName ) 
				and ( e.Username <> @username ) 
				and ( e.Email = @email ) 
			) 
		) ) 
	) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsResourceUnique]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsResourceUnique]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @resource nvarchar( 1024 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @resource = [Security].TrimToNull( @resource )

	declare @output bit
	set @output = 0

	if ( 
		( @resource is not null ) 
		and ( not exists ( select top 1 1 
			from [Security].GetCurrentRight() 
			where ( 
				( ApplicationName = @applicationName ) 
				and ( RightName <> @rightName ) 
				and ( [Resource] = @resource ) 
			) 
		) ) 
	) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsRightPopulated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsRightPopulated]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentDeniedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
		) 
	) ) begin 
		set @output = 1
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentGrantedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
		) 
	) ) begin 
		set @output = 1
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentDeniedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
		) 
	) ) begin 
		set @output = 1
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentGrantedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsRolePopulated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsRolePopulated]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	declare @output bit
	set @output = 1

	if ( not exists ( select top 1 1 
		from [Security].GetCurrentRoleUser() as ru 
		join [Security].GetCurrentRole() as r on ( 
			( ru.ApplicationName = r.ApplicationName ) 
			and ( ru.RoleName = r.RoleName ) 
			and ( ru.WhenRoleCreated = r.WhenCreated ) 
		) 
		where ( 
			( ru.ApplicationName = @applicationName ) 
			and ( ru.RoleName = @roleName ) 
		) 
	) ) begin 
		set @output = 0
	end

	return @output	
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserApproved]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsUserApproved]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	declare @output bit
	set @output = 0

	if ( 
		( @applicationName is not null ) 
		and ( @username is not null ) 
		and ( exists ( select top 1 1 
			from [Security].GetCurrentUser() as u 
			join [Security].GetCurrentUserApproval() as a on ( 
				( u.ApplicationName = a.ApplicationName ) 
				and ( u.Username = a.Username ) 
				and( u.WhenCreated = a.WhenUserCreated ) 
			) 
			where ( 
				( a.ApplicationName = @applicationName ) 
				and ( a.Username = @username ) 
			) 
		) ) 
	) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserConfirmed]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE function [Security].[IsUserConfirmed]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @username = [Security].TrimToNull( @username ) 

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserConfirmationToken() as t on ( 
			( u.ApplicationName = t.ApplicationName ) 
			and ( u.Username = t.Username ) 
			and ( u.WhenCreated = t.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenConfirmed is not null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserInRole]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsUserInRole]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )
	set @username = [Security].TrimToNull( @username )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentRoleUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RoleName = @roleName ) 
			and ( Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[IsUserLockedOut]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserLockedOut() as l on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[IsValidUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[IsValidUserLoginResetToken]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )
	set @token = [Security].TrimToNull( @token )

	declare @output bit
	set @output = 0

	if ( @username is null ) begin 
		set @username = [Security].UsernameByUserLoginResetToken( @applicationName, @token, @tokenLifetime )
	end

	if ( ( @applicationName is null ) or ( @username is null ) or ( @tokenLifetime is null ) or ( @tokenLifetime <= 0 ) ) begin 
		set @output = 0
	end else if ( exists ( select top 1 1 
		from [Security].GetCurrentUserLoginResetToken() as t 
		join [Security].GetCurrentUserLocalLogin() as l on ( 
			( l.ApplicationName = t.ApplicationName ) 
			and ( l.Username = t.Username ) 
			and ( l.WhenCreated = t.WhenLoginCreated ) 
		) 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenVerified is null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Username = @username ) 
			and ( t.Token = @token ) 
			and ( GetDate() <= DateAdd( MI, @tokenLifetime, t.WhenCreated ) ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output	
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[LastUserLockedOut]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 l.WhenCreated 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserLockedOut() as l on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
		order by l.WhenCreated desc 
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLoginFailure]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[LastUserLoginFailure]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 WhenFailed 
		from [Security].UserLoginFailure as l 
		join [Security].GetCurrentUser() as u on ( 
			( l.ApplicationName = u.ApplicationName ) 
			and ( l.Username = u.Username ) 
			and ( l.WhenUserCreated = u.WhenCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
		order by WhenFailed desc 
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLoginSuccess]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[LastUserLoginSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 l.WhenSucceeded 
		from [Security].UserLoginSuccess as l 
		join [Security].GetCurrentUser() as u on ( 
			( l.ApplicationName = u.ApplicationName ) 
			and ( l.Username = u.Username ) 
			and ( l.WhenUserCreated = u.WhenCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
		order by l.WhenSucceeded desc 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[LastUserLogout]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[LastUserLogout]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 l.WhenLogout 
		from [Security].UserLogout as l 
		join [Security].GetCurrentUser() as u on ( 
			( l.ApplicationName = u.ApplicationName ) 
			and ( l.Username = u.Username ) 
			and ( l.WhenUserCreated = u.WhenCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
		order by l.WhenLogout desc 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OAuthTokenSecretByToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[OAuthTokenSecretByToken]( @applicationName nvarchar( 255 ), @token nvarchar( 128 ) ) returns nvarchar( 128 ) as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	return ( select top 1 [Secret] 
		from [Security].GetCurrentOAuthToken() 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = @applicationName ) 
			and ( Token = @token ) 
		) 
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleDeniedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[RoleDeniedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @roleName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentDeniedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
			and ( RoleName = @roleName ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleGrantedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[RoleGrantedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @roleName nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @roleName = [Security].TrimToNull( @roleName )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentGrantedRoleRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
			and ( RoleName = @roleName ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[TrimToNull]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[TrimToNull]( @value nvarchar( max ) ) returns nvarchar( max ) as begin 
	if ( @value is not null ) begin 
		set @value = [Security].AllTrim( @value )
		if ( '''' = @value ) begin 
			set @value = null
		end
	end
	return @value
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserDeniedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserDeniedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentDeniedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
			and ( Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserGrantedRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserGrantedRight]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ), @username nvarchar( 255 ) ) returns bit as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @rightName = [Security].TrimToNull( @rightName )
	set @username = [Security].TrimToNull( @username )

	declare @output bit
	set @output = 0

	if ( exists ( select top 1 1 
		from [Security].GetCurrentGrantedUserRight() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RightName = @rightName ) 
			and ( Username = @username ) 
		) 
	) ) begin 
		set @output = 1
	end

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdByUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[UserIdByUserLoginResetToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) returns int as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	return ( select top 1 u.UserId 
		from [Security].GetCurrentUserLoginResetToken() as t 
		join [Security].GetCurrentUserLocalLogin() as l on ( 
			( l.ApplicationName = t.ApplicationName ) 
			and ( l.Username = t.Username ) 
			and ( l.WhenCreated = t.WhenLoginCreated ) 
		)
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenVerified is null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Token = @token ) 
			and ( 0 < @tokenLifetime ) 
			and ( GetDate() <= DateAdd( MI, @tokenLifetime, t.WhenCreated ) ) 
		) 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdFromUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserIdFromUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns int as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 UserId 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		)
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserIdFromUserOAuthLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserIdFromUserOAuthLogin]( @applicationName nvarchar( 255 ), @provider nvarchar( 512 ), @providerUsername nvarchar( 1024 ) ) returns int as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @provider = [Security].TrimToNull( @provider )
	set @providerUsername = [Security].TrimToNull( @providerUsername )

	declare @output int

	set @output = ( select top 1 [Security].TrimToNull( u.UserId ) 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserOAuthLogin() as o on ( 
			( u.ApplicationName = o.ApplicationName ) 
			and ( u.Username = o.Username ) 
			and ( u.WhenCreated = o.WhenUserCreated ) 
		) 
		where ( 
			( o.ApplicationName = @applicationName ) 
			and ( o.Provider = @provider ) 
			and ( o.ProviderUsername = @providerUsername ) 
		) 
	)

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLastActive]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserLastActive]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 WhenLastActive 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		) 
		order by WhenCreated desc 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLocalLoginWhenCreatedByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserLocalLoginWhenCreatedByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @output datetime

	set @output = ( select top 1 l.WhenCreated 
		from [Security].GetCurrentUserLocalLogin() as l 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
		) 
	)
	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailureSinceLastUserLoginSuccess]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserLoginFailureSinceLastUserLoginSuccess]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns int as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	declare @lastSuccess datetime
	set @lastSuccess = [Security].LastUserLoginSuccess( @applicationName, @username ) 
	return ( select count ( * ) 
		from [Security].GetCurrentUser() as u 
		join [Security].UserLoginFailure as l on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		)
		where ( 
			( l.ApplicationName = @applicationName ) 
			and ( l.Username = @username ) 
			and ( 
				( @lastSuccess is null ) 
				or ( @lastSuccess < l.WhenFailed ) 
			) 
		) 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetTokenByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UserLoginResetTokenByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns nvarchar( 256 ) as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 t.Token 
		from [Security].GetCurrentUser() as u 
		join [Security].GetCurrentUserLocalLogin() as l on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated )  
		) 
		join [Security].GetCurrentUserLoginResetToken() as t on ( 
			( l.ApplicationName = t.ApplicationName ) 
			and ( l.Username = t.Username ) 
			and ( l.WhenCreated = t.WhenLoginCreated ) 
		) 
		where ( 
			( t.ApplicationName = @applicationName ) 
			and ( t.Username = @username ) 
			and ( t.WhenVerified is null ) 
		) 
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameByUserConfirmationToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[UsernameByUserConfirmationToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) returns nvarchar( 255 ) as begin 
	set @applicationName = [Security].TrimToNull( @applicationName ) 
	set @token = [Security].TrimToNull( @token ) 

	declare @output nvarchar( 255 )
	set @output = null

	set @output = ( select top 1 t.Username 
		from [Security].GetCurrentUserConfirmationToken() as t 
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = t.ApplicationName ) 
			and ( u.Username = t.Username ) 
			and ( u.WhenCreated = t.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenConfirmed is null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Token = @token ) 
			and ( GetDate() <= DateAdd( MI, @tokenLifetime, t.WhenCreated ) ) 
		) 
	)

	return @output
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameByUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[UsernameByUserLoginResetToken]( @applicationName nvarchar( 255 ), @token nvarchar( 256 ), @tokenLifetime int ) returns nvarchar( 255 ) as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @token = [Security].TrimToNull( @token )

	return ( select top 1 [Security].TrimToNull( t.Username ) 
		from [Security].GetCurrentUserLoginResetToken() as t 
		join [Security].GetCurrentUserLocalLogin() as l on ( 
			( l.ApplicationName = t.ApplicationName ) 
			and ( l.Username = t.Username ) 
			and ( l.WhenCreated = t.WhenLoginCreated ) 
		)
		join [Security].GetCurrentUser() as u on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
		where ( 
			( t.WhenVerified is null ) 
			and ( t.ApplicationName = @applicationName ) 
			and ( t.Token = @token ) 
			and ( 0 < @tokenLifetime ) 
			and ( GetDate() <= DateAdd( MI, @tokenLifetime, t.WhenCreated ) ) 
		) 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UsernameFromUserId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[UsernameFromUserId]( @applicationName nvarchar( 255 ), @userId int ) returns nvarchar( 255 ) as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	return ( select top 1 Username 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( UserId = @userId ) 
		)
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenRightCreated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[WhenRightCreated]( @applicationName nvarchar( 255 ), @rightName nvarchar( 255 ) ) returns datetime as begin 
	return ( select top 1 WhenCreated 
		from [Security].[Right] 
		where ( 
			( WhenDeleted is null ) 
			and ( ApplicationName = [Security].TrimToNull( @applicationName ) ) 
			and ( RightName = [Security].TrimToNull( @rightName ) ) 
		) 
		order by WhenCreated desc 
	)
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenRoleCreatedByRoleName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[WhenRoleCreatedByRoleName]( @applicationName nvarchar( 255 ), @roleName nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @roleName = [Security].TrimToNull( @roleName )

	return ( select top 1 WhenCreated 
		from [Security].GetCurrentRole() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( RoleName = @roleName ) 
		) 
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[WhenUserCreatedByUsername]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[WhenUserCreatedByUsername]( @applicationName nvarchar( 255 ), @username nvarchar( 255 ) ) returns datetime as begin 
	set @applicationName = [Security].TrimToNull( @applicationName )
	set @username = [Security].TrimToNull( @username )

	return ( select top 1 WhenCreated 
		from [Security].GetCurrentUser() 
		where ( 
			( ApplicationName = @applicationName ) 
			and ( Username = @username ) 
		)
	) 
end
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[DeniedRoleRight](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenRightCreated] [datetime] NOT NULL,
	[RoleName] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityDeniedRoleRight] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[RoleName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DeniedUserRight]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[DeniedUserRight](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenRightCreated] [datetime] NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityDeniedUserRight] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[GrantedRoleRight](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenRightCreated] [datetime] NOT NULL,
	[RoleName] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityGrantedRoleRight] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[RoleName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GrantedUserRight]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[GrantedUserRight](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenRightCreated] [datetime] NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityGrantedUserRight] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OAuthToken]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[OAuthToken](
	[ApplicationName] [nvarchar](128) NOT NULL,
	[Token] [nvarchar](128) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Secret] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_SecurityOAuthToken] PRIMARY KEY NONCLUSTERED 
(
	[Token] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[Right]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[Right](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[RightId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_SecurityRight] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RightResource]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[RightResource](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RightName] [nvarchar](255) NOT NULL,
	[WhenRightCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[Resource] [nvarchar](1024) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityRightResource] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RightName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[Role](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RoleName] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_SecurityRole] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RoleName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[RoleUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[RoleUser](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[RoleName] [nvarchar](255) NOT NULL,
	[WhenRoleCreated] [datetime] NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityRoleUser] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[RoleName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[User]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[User](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[WhenLastActive] [datetime] NULL,
 CONSTRAINT [PK_SecurityUser] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserApproval]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserApproval](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityUserApproval] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserComment](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Comment] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_SecurityUserComment] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserConfirmationToken](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Token] [nvarchar](256) NOT NULL,
	[WhenConfirmed] [datetime] NULL,
 CONSTRAINT [PK_SecurityUserConfirmationToken] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserEmail]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserEmail](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Email] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_SecurityUserEmail] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLocalLogin]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLocalLogin](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Password] [nvarchar](512) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordAlgorithm] [nvarchar](255) NOT NULL,
	[PasswordSalt] [nvarchar](255) NULL,
	[WhenPasswordUpdated] [datetime] NOT NULL,
	[WhoPasswordUpdated] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityUserLocalLogin] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLockedOut]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLockedOut](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityUserLockedOut] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailure]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLoginFailure](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenFailed] [datetime] NOT NULL,
 CONSTRAINT [PK_SecurityUserLoginFailure] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenUserCreated] ASC,
	[WhenFailed] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLoginResetToken](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenLoginCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Token] [nvarchar](256) NOT NULL,
	[WhenVerified] [datetime] NULL,
 CONSTRAINT [PK_SecurityUserLoginResetToken] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenLoginCreated] ASC,
	[WhenCreated] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLoginSuccess](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenSucceeded] [datetime] NOT NULL,
 CONSTRAINT [PK_SecurityUserLoginSuccess] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenUserCreated] ASC,
	[WhenSucceeded] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserLogout]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserLogout](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenLogout] [datetime] NOT NULL,
 CONSTRAINT [PK_UserLogout] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenUserCreated] ASC,
	[WhenLogout] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserOAuthLogin](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[Provider] [nvarchar](512) NOT NULL,
	[ProviderUsername] [nvarchar](1024) NOT NULL,
 CONSTRAINT [PK_SecurityUserOAuthLogin] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[UserPasswordQuestionAnswer](
	[ApplicationName] [nvarchar](255) NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[WhenUserCreated] [datetime] NOT NULL,
	[WhenCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](255) NOT NULL,
	[WhenDeleted] [datetime] NULL,
	[WhoDeleted] [nvarchar](255) NULL,
	[PasswordQuestion] [nvarchar](512) NOT NULL,
	[PasswordAnswer] [nvarchar](512) NOT NULL,
	[PasswordAnswerFormat] [int] NOT NULL,
	[PasswordAnswerAlgorithm] [nvarchar](255) NULL,
	[PasswordAnswerSalt] [nvarchar](255) NULL,
	[WhenPasswordQuestionAnswerUpdated] [datetime] NOT NULL,
	[WhoPasswordQuestionAnswerUpdated] [nvarchar](255) NULL,
 CONSTRAINT [PK_SecurityUserPasswordQuestionAnswer] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentRight]() returns table as return ( 
	select r.*, rr.[Resource] 
		from [Security].[Right] as r 
		left outer join [Security].RightResource as rr on ( 
			( r.ApplicationName = rr.ApplicationName ) 
			and ( r.RightName = rr.RightName ) 
			and ( r.WhenCreated = rr.WhenRightCreated ) 
		) 
		where ( 
			( rr.WhenDeleted is null ) 
			and ( r.WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentGrantedRoleRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentGrantedRoleRight]() returns table as return ( 
	select r.*, g.RoleName 
		from [Security].GetCurrentRight() as r 
		join [Security].GrantedRoleRight as g on ( 
			( r.ApplicationName = g.ApplicationName ) 
			and ( r.RightName = g.RightName ) 
			and ( r.WhenCreated = g.WhenRightCreated ) 
		) 
		where ( 
			( g.WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentGrantedRoleRight]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentGrantedRoleRight] as 
	select * 
		from [Security].GetCurrentGrantedRoleRight() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLockedOut]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserLockedOut]() returns table as return ( 
	select * 
		from [Security].UserLockedOut 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLockedOut]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserLockedOut] as 
	select * 
		from [Security].GetCurrentUserLockedOut() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUser]() returns table as return ( 
	select * 
		from [Security].[User] 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserComment]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserComment]() returns table as return ( 
	select * 
		from [Security].UserComment 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserEmail]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserEmail]() returns table as return ( 
	select * 
		from [Security].UserEmail 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLocalLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserLocalLogin]() returns table as return ( 
	select * 
		from [Security].UserLocalLogin 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserPasswordQuestionAnswer]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserPasswordQuestionAnswer]() returns table as return ( 
	select * 
		from [Security].UserPasswordQuestionAnswer 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentMembership]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[GetCurrentMembership]() returns table as return ( 
	select u.ApplicationName, u.Username, u.UserId, e.Email, q.PasswordQuestion, c.Comment, 
			[Security].IsUserApproved( u.ApplicationName, u.Username ) as ''IsApproved'', [Security].IsUserLockedOut( u.ApplicationName, u.Username ) as ''IsLockedOut'', 
			u.WhenCreated, [Security].LastUserLoginSuccess( u.ApplicationName, u.Username ) as ''LastLoginDate'', u.WhenLastActive as ''LastActivityDate'', 
			l.WhenPasswordUpdated as ''LastPasswordChangedDate'', [Security].LastUserLockedOut( u.ApplicationName, u.Username ) as ''LastLockoutDate'', 
			[Security].LastUserLogout( u.ApplicationName, u.Username ) as ''LastLogoutDate'' 
		from [Security].GetCurrentUser() as u 
		left outer join [Security].GetCurrentUserEmail() as e on ( 
			( u.ApplicationName = e.ApplicationName ) 
			and ( u.Username = e.Username ) 
			and ( u.WhenCreated = e.WhenUserCreated ) 
		) 
		left outer join [Security].GetCurrentUserPasswordQuestionAnswer() as q on ( 
			( u.ApplicationName = q.ApplicationName ) 
			and ( u.Username = q.Username ) 
			and ( u.WhenCreated = q.WhenUserCreated ) 
		) 
		left outer join [Security].GetCurrentUserComment() as c on ( 
			( u.ApplicationName = c.ApplicationName ) 
			and ( u.Username = c.Username ) 
			and ( u.WhenCreated = c.WhenUserCreated ) 
		) 
		left outer join [Security].GetCurrentUserLocalLogin() as l on ( 
			( u.ApplicationName = l.ApplicationName ) 
			and ( u.Username = l.Username ) 
			and ( u.WhenCreated = l.WhenUserCreated ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentMembership]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentMembership] as 
	select * 
		from [Security].GetCurrentMembership() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentDeniedUserRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentDeniedUserRight]() returns table as return ( 
	select r.*, d.UserName 
		from [Security].GetCurrentRight() as r 
		join [Security].DeniedUserRight as d on ( 
			( r.ApplicationName = d.ApplicationName ) 
			and ( r.RightName = d.RightName ) 
			and ( r.WhenCreated = d.WhenRightCreated ) 
		) 
		where ( 
			( d.WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[OnlineMembershipUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [Security].[OnlineMembershipUser]( @applicationName nvarchar( 255 ), @userIsOnlineTimeWindow int ) returns table as return ( 
	select * 
		from [Security].GetCurrentMembership() 
		where ( 
			( ApplicationName = [Security].TrimToNull( @applicationName ) ) 
			and ( 0 < @userIsOnlineTimeWindow ) 
			and ( 
				( LastActivityDate is not null ) 
				or ( LastLoginDate is not null ) 
			) 
			and ( 
				( LastLogoutDate is null ) 
				or ( LastLogoutDate <= LastActivityDate ) 
				or ( LastLogoutDate <= LastLoginDate ) 
			) 
			and ( 
				( GetDate() <= DATEADD( MI, @userIsOnlineTimeWindow, LastActivityDate ) ) 
				or ( GetDate() <= DATEADD( MI, @userIsOnlineTimeWindow, LastLoginDate ) ) 
			) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentDeniedUserRight]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentDeniedUserRight] as 
	select * 
		from [Security].GetCurrentDeniedUserRight() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentGrantedUserRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentGrantedUserRight]() returns table as return ( 
	select r.*, g.UserName 
		from [Security].GetCurrentRight() as r 
		join [Security].GrantedUserRight as g on ( 
			( r.ApplicationName = g.ApplicationName ) 
			and ( r.RightName = g.RightName ) 
			and ( r.WhenCreated = g.WhenRightCreated ) 
		) 
		where ( 
			( g.WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentGrantedUserRight]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentGrantedUserRight] as 
	select * 
		from [Security].GetCurrentGrantedUserRight() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserPasswordQuestionAnswer]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserPasswordQuestionAnswer] as 
	select * 
		from [Security].GetCurrentUserPasswordQuestionAnswer() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRole]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentRole]() returns table as return ( 
	select * 
		from [Security].[Role] 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRole]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentRole] as 
	select * 
		from [Security].GetCurrentRole()
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserConfirmationToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserConfirmationToken]() returns table as return ( 
	select * 
		from [Security].UserConfirmationToken 
		where ( 
			( WhenDeleted is null ) 
		) 
) 
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserConfirmationToken]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserConfirmationToken] as 
	select * 
		from [Security].GetCurrentUserConfirmationToken() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentRoleUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentRoleUser]() returns table as return ( 
	select * 
		from [Security].RoleUser 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRoleUser]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentRoleUser] as 
	select * 
		from [Security].GetCurrentRoleUser() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentOAuthToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentOAuthToken]() returns table as return ( 
	select * 
		from [Security].OAuthToken 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentOAuthToken]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentOAuthToken] as 
	select * 
		from [Security].GetCurrentOAuthToken() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserEmail]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserEmail] as 
	select * 
		from [Security].GetCurrentUserEmail() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUser]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUser] as 
	select * 
		from [Security].GetCurrentUser() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserOAuthLogin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserOAuthLogin]() returns table as return ( 
	select * 
		from [Security].UserOAuthLogin 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserOAuthLogin]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserOAuthLogin] as 
	select * 
		from [Security].GetCurrentUserOAuthLogin() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentRight]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentRight] as 
	select * 
		from [Security].GetCurrentRight() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLocalLogin]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserLocalLogin] as 
	select * 
		from [Security].GetCurrentUserLocalLogin() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserComment]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserComment] as 
	select * 
		from [Security].GetCurrentUserComment() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentDeniedRoleRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentDeniedRoleRight]() returns table as return ( 
	select r.*, d.RoleName 
		from [Security].GetCurrentRight() as r 
		join [Security].DeniedRoleRight as d on ( 
			( r.ApplicationName = d.ApplicationName ) 
			and ( r.RightName = d.RightName ) 
			and ( r.WhenCreated = d.WhenRightCreated ) 
		) 
		where ( 
			( d.WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentDeniedRoleRight]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentDeniedRoleRight] as 
	select * 
		from [Security].GetCurrentDeniedRoleRight() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserApproval]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserApproval]() returns table as return ( 
	select * 
		from [Security].UserApproval 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserApproval]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserApproval] as 
	select * 
		from [Security].GetCurrentUserApproval() 
' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetCurrentUserLoginResetToken]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [Security].[GetCurrentUserLoginResetToken]() returns table as return ( 
	select * 
		from [Security].UserLoginResetToken 
		where ( 
			( WhenDeleted is null ) 
		) 
)
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Security].[CurrentUserLoginResetToken]'))
EXEC dbo.sp_executesql @statement = N'
create view [Security].[CurrentUserLoginResetToken] as 
	select * 
		from [Security].GetCurrentUserLoginResetToken() 
' 
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[OAuthToken]') AND name = N'IX_Security_OAuthToken')
CREATE CLUSTERED INDEX [IX_Security_OAuthToken] ON [Security].[OAuthToken]
(
	[ApplicationName] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginFailure]') AND name = N'IX_Security_UserLoginFailure')
CREATE CLUSTERED INDEX [IX_Security_UserLoginFailure] ON [Security].[UserLoginFailure]
(
	[WhenFailed] DESC,
	[ApplicationName] ASC,
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]') AND name = N'IX_Security_UserLoginResetToken')
CREATE CLUSTERED INDEX [IX_Security_UserLoginResetToken] ON [Security].[UserLoginResetToken]
(
	[ApplicationName] ASC,
	[Username] ASC,
	[WhenCreated] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]') AND name = N'IX_Security_UserLoginSuccess')
CREATE CLUSTERED INDEX [IX_Security_UserLoginSuccess] ON [Security].[UserLoginSuccess]
(
	[WhenSucceeded] DESC,
	[ApplicationName] ASC,
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhenC__304FD425]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] ADD  CONSTRAINT [DF__DeniedRol__WhenC__304FD425]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhenD__32381C97]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] ADD  CONSTRAINT [DF__DeniedRol__WhenD__32381C97]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedRol__WhoDe__332C40D0]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedRoleRight] ADD  CONSTRAINT [DF__DeniedRol__WhoDe__332C40D0]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhenC__4BF7EE9A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] ADD  CONSTRAINT [DF__DeniedUse__WhenC__4BF7EE9A]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhenD__4DE0370C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] ADD  CONSTRAINT [DF__DeniedUse__WhenD__4DE0370C]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__DeniedUse__WhoDe__4ED45B45]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[DeniedUserRight] ADD  CONSTRAINT [DF__DeniedUse__WhoDe__4ED45B45]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhenC__3CB5AB0A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] ADD  CONSTRAINT [DF__GrantedRo__WhenC__3CB5AB0A]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhenD__3E9DF37C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] ADD  CONSTRAINT [DF__GrantedRo__WhenD__3E9DF37C]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedRo__WhoDe__3F9217B5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedRoleRight] ADD  CONSTRAINT [DF__GrantedRo__WhoDe__3F9217B5]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhenC__585DC57F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] ADD  CONSTRAINT [DF__GrantedUs__WhenC__585DC57F]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhenD__5A460DF1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] ADD  CONSTRAINT [DF__GrantedUs__WhenD__5A460DF1]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__GrantedUs__WhoDe__5B3A322A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[GrantedUserRight] ADD  CONSTRAINT [DF__GrantedUs__WhoDe__5B3A322A]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhenC__01C9EF66]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] ADD  CONSTRAINT [DF__OAuthToke__WhenC__01C9EF66]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhenD__03B237D8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] ADD  CONSTRAINT [DF__OAuthToke__WhenD__03B237D8]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__OAuthToke__WhoDe__04A65C11]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[OAuthToken] ADD  CONSTRAINT [DF__OAuthToke__WhoDe__04A65C11]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhenCreat__13B39577]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] ADD  CONSTRAINT [DF__Right__WhenCreat__13B39577]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhenDelet__159BDDE9]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] ADD  CONSTRAINT [DF__Right__WhenDelet__159BDDE9]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Right__WhoDelete__16900222]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Right] ADD  CONSTRAINT [DF__Right__WhoDelete__16900222]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhenC__1D3CFFB1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] ADD  CONSTRAINT [DF__RightReso__WhenC__1D3CFFB1]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhenD__20196C5C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] ADD  CONSTRAINT [DF__RightReso__WhenD__20196C5C]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RightReso__WhoDe__210D9095]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RightResource] ADD  CONSTRAINT [DF__RightReso__WhoDe__210D9095]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhenCreate__715E7D73]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] ADD  CONSTRAINT [DF__Role__WhenCreate__715E7D73]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhenDelete__7346C5E5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] ADD  CONSTRAINT [DF__Role__WhenDelete__7346C5E5]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__Role__WhoDeleted__743AEA1E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Role] ADD  CONSTRAINT [DF__Role__WhoDeleted__743AEA1E]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhenCr__00A0C103]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] ADD  CONSTRAINT [DF__RoleUser__WhenCr__00A0C103]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhenDe__02890975]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] ADD  CONSTRAINT [DF__RoleUser__WhenDe__02890975]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__RoleUser__WhoDel__037D2DAE]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[RoleUser] ADD  CONSTRAINT [DF__RoleUser__WhoDel__037D2DAE]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenCreate__02F31DC9]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] ADD  CONSTRAINT [DF__User__WhenCreate__02F31DC9]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenDelete__04DB663B]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] ADD  CONSTRAINT [DF__User__WhenDelete__04DB663B]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhoDeleted__05CF8A74]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] ADD  CONSTRAINT [DF__User__WhoDeleted__05CF8A74]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__User__WhenLastAc__06C3AEAD]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[User] ADD  CONSTRAINT [DF__User__WhenLastAc__06C3AEAD]  DEFAULT (NULL) FOR [WhenLastActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhenC__30B9E879]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] ADD  CONSTRAINT [DF__UserAppro__WhenC__30B9E879]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhenD__32A230EB]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] ADD  CONSTRAINT [DF__UserAppro__WhenD__32A230EB]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserAppro__WhoDe__33965524]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserApproval] ADD  CONSTRAINT [DF__UserAppro__WhoDe__33965524]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhenC__254835CD]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] ADD  CONSTRAINT [DF__UserComme__WhenC__254835CD]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhenD__27307E3F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] ADD  CONSTRAINT [DF__UserComme__WhenD__27307E3F]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserComme__WhoDe__2824A278]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserComment] ADD  CONSTRAINT [DF__UserComme__WhoDe__2824A278]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenC__7193879D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] ADD  CONSTRAINT [DF__UserConfi__WhenC__7193879D]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenD__737BD00F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] ADD  CONSTRAINT [DF__UserConfi__WhenD__737BD00F]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhoDe__746FF448]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] ADD  CONSTRAINT [DF__UserConfi__WhoDe__746FF448]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserConfi__WhenC__76583CBA]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserConfirmationToken] ADD  CONSTRAINT [DF__UserConfi__WhenC__76583CBA]  DEFAULT (NULL) FOR [WhenConfirmed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhenC__18E25EE8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] ADD  CONSTRAINT [DF__UserEmail__WhenC__18E25EE8]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhenD__1ACAA75A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] ADD  CONSTRAINT [DF__UserEmail__WhenD__1ACAA75A]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserEmail__WhoDe__1BBECB93]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserEmail] ADD  CONSTRAINT [DF__UserEmail__WhoDe__1BBECB93]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenC__1F5A524D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__WhenC__1F5A524D]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenD__21429ABF]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__WhenD__21429ABF]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhoDe__2236BEF8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__WhoDe__2236BEF8]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__232AE331]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__Passw__232AE331]  DEFAULT ((0)) FOR [PasswordFormat]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__25132BA3]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__Passw__25132BA3]  DEFAULT ('Clear') FOR [PasswordAlgorithm]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__Passw__26074FDC]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__Passw__26074FDC]  DEFAULT (NULL) FOR [PasswordSalt]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhenP__26FB7415]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__WhenP__26FB7415]  DEFAULT (getdate()) FOR [WhenPasswordUpdated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocal__WhoPa__27EF984E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLocalLogin] ADD  CONSTRAINT [DF__UserLocal__WhoPa__27EF984E]  DEFAULT (NULL) FOR [WhoPasswordUpdated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhenC__3D1FBF5E]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] ADD  CONSTRAINT [DF__UserLocke__WhenC__3D1FBF5E]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhenD__3F0807D0]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] ADD  CONSTRAINT [DF__UserLocke__WhenD__3F0807D0]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLocke__WhoDe__3FFC2C09]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLockedOut] ADD  CONSTRAINT [DF__UserLocke__WhoDe__3FFC2C09]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenF__540324B6]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginFailure] ADD  CONSTRAINT [DF__UserLogin__WhenF__540324B6]  DEFAULT (getdate()) FOR [WhenFailed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenC__3549936C]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] ADD  CONSTRAINT [DF__UserLogin__WhenC__3549936C]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenD__3731DBDE]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] ADD  CONSTRAINT [DF__UserLogin__WhenD__3731DBDE]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhoDe__38260017]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] ADD  CONSTRAINT [DF__UserLogin__WhoDe__38260017]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenV__3A0E4889]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginResetToken] ADD  CONSTRAINT [DF__UserLogin__WhenV__3A0E4889]  DEFAULT (NULL) FOR [WhenVerified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserLogin__WhenS__4B6DDEB5]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserLoginSuccess] ADD  CONSTRAINT [DF__UserLogin__WhenS__4B6DDEB5]  DEFAULT (getdate()) FOR [WhenSucceeded]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhenC__0F23EA84]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] ADD  CONSTRAINT [DF__UserOAuth__WhenC__0F23EA84]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhenD__110C32F6]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] ADD  CONSTRAINT [DF__UserOAuth__WhenD__110C32F6]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserOAuth__WhoDe__1200572F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserOAuthLogin] ADD  CONSTRAINT [DF__UserOAuth__WhoDe__1200572F]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenC__5E80B329]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__WhenC__5E80B329]  DEFAULT (getdate()) FOR [WhenCreated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenD__6068FB9B]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__WhenD__6068FB9B]  DEFAULT (NULL) FOR [WhenDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhoDe__615D1FD4]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__WhoDe__615D1FD4]  DEFAULT (NULL) FOR [WhoDeleted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__6251440D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__Passw__6251440D]  DEFAULT ((0)) FOR [PasswordAnswerFormat]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__64398C7F]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__Passw__64398C7F]  DEFAULT (NULL) FOR [PasswordAnswerAlgorithm]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__Passw__652DB0B8]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__Passw__652DB0B8]  DEFAULT (NULL) FOR [PasswordAnswerSalt]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhenP__6621D4F1]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__WhenP__6621D4F1]  DEFAULT (getdate()) FOR [WhenPasswordQuestionAnswerUpdated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Security].[DF__UserPassw__WhoPa__6715F92A]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[UserPasswordQuestionAnswer] ADD  CONSTRAINT [DF__UserPassw__WhoPa__6715F92A]  DEFAULT (NULL) FOR [WhoPasswordQuestionAnswerUpdated]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRight_SecurityDeniedRoleRight] FOREIGN KEY([ApplicationName], [RightName], [WhenRightCreated])
REFERENCES [Security].[Right] ([ApplicationName], [RightName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] CHECK CONSTRAINT [FK_SecurityRight_SecurityDeniedRoleRight]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRight_SecurityDeniedUserRight] FOREIGN KEY([ApplicationName], [RightName], [WhenRightCreated])
REFERENCES [Security].[Right] ([ApplicationName], [RightName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityDeniedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] CHECK CONSTRAINT [FK_SecurityRight_SecurityDeniedUserRight]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRight_SecurityGrantedRoleRight] FOREIGN KEY([ApplicationName], [RightName], [WhenRightCreated])
REFERENCES [Security].[Right] ([ApplicationName], [RightName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedRoleRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] CHECK CONSTRAINT [FK_SecurityRight_SecurityGrantedRoleRight]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRight_SecurityGrantedUserRight] FOREIGN KEY([ApplicationName], [RightName], [WhenRightCreated])
REFERENCES [Security].[Right] ([ApplicationName], [RightName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityGrantedUserRight]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] CHECK CONSTRAINT [FK_SecurityRight_SecurityGrantedUserRight]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityRightResource]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRight_SecurityRightResource] FOREIGN KEY([ApplicationName], [RightName], [WhenRightCreated])
REFERENCES [Security].[Right] ([ApplicationName], [RightName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRight_SecurityRightResource]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] CHECK CONSTRAINT [FK_SecurityRight_SecurityRightResource]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRole_SecurityRoleUser]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRole_SecurityRoleUser] FOREIGN KEY([ApplicationName], [RoleName], [WhenRoleCreated])
REFERENCES [Security].[Role] ([ApplicationName], [RoleName], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityRole_SecurityRoleUser]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] CHECK CONSTRAINT [FK_SecurityRole_SecurityRoleUser]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserApproval]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserApproval] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserApproval]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] CHECK CONSTRAINT [FK_SecurityUser_UserApproval]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserComment]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserComment] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserComment]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] CHECK CONSTRAINT [FK_SecurityUser_UserComment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserConfirmationToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserConfirmationToken] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserConfirmationToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] CHECK CONSTRAINT [FK_SecurityUser_UserConfirmationToken]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserEmail]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserEmail] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserEmail]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] CHECK CONSTRAINT [FK_SecurityUser_UserEmail]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLocalLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserLocalLogin] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLocalLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] CHECK CONSTRAINT [FK_SecurityUser_UserLocalLogin]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLockedOut]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserLockedOut] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLockedOut]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] CHECK CONSTRAINT [FK_SecurityUser_UserLockedOut]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginFailure]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserLoginFailure] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginFailure]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] CHECK CONSTRAINT [FK_SecurityUser_UserLoginFailure]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUserLogin_UserLoginResetToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUserLogin_UserLoginResetToken] FOREIGN KEY([ApplicationName], [Username], [WhenLoginCreated])
REFERENCES [Security].[UserLocalLogin] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUserLogin_UserLoginResetToken]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] CHECK CONSTRAINT [FK_SecurityUserLogin_UserLoginResetToken]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginSuccess]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserLoginSuccess] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserLoginSuccess]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] CHECK CONSTRAINT [FK_SecurityUser_UserLoginSuccess]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserOAuthLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserOAuthLogin] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserOAuthLogin]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [FK_SecurityUser_UserOAuthLogin]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserPasswordQuestionAnswer]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUser_UserPasswordQuestionAnswer] FOREIGN KEY([ApplicationName], [Username], [WhenUserCreated])
REFERENCES [Security].[User] ([ApplicationName], [Username], [WhenCreated])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Security].[FK_SecurityUser_UserPasswordQuestionAnswer]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] CHECK CONSTRAINT [FK_SecurityUser_UserPasswordQuestionAnswer]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Appli__2C7F4341]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedRol__Appli__2C7F4341] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Appli__2C7F4341]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] CHECK CONSTRAINT [CK__DeniedRol__Appli__2C7F4341]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Right__2D73677A]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedRol__Right__2D73677A] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__Right__2D73677A]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] CHECK CONSTRAINT [CK__DeniedRol__Right__2D73677A]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__RoleN__2F5BAFEC]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedRol__RoleN__2F5BAFEC] CHECK  ((''<>ltrim(rtrim([RoleName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__RoleN__2F5BAFEC]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] CHECK CONSTRAINT [CK__DeniedRol__RoleN__2F5BAFEC]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__WhoCr__3143F85E]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedRol__WhoCr__3143F85E] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedRol__WhoCr__3143F85E]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedRoleRight]'))
ALTER TABLE [Security].[DeniedRoleRight] CHECK CONSTRAINT [CK__DeniedRol__WhoCr__3143F85E]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Appli__48275DB6]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedUse__Appli__48275DB6] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Appli__48275DB6]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] CHECK CONSTRAINT [CK__DeniedUse__Appli__48275DB6]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Right__491B81EF]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedUse__Right__491B81EF] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Right__491B81EF]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] CHECK CONSTRAINT [CK__DeniedUse__Right__491B81EF]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Usern__4B03CA61]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedUse__Usern__4B03CA61] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__Usern__4B03CA61]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] CHECK CONSTRAINT [CK__DeniedUse__Usern__4B03CA61]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__WhoCr__4CEC12D3]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__DeniedUse__WhoCr__4CEC12D3] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__DeniedUse__WhoCr__4CEC12D3]') AND parent_object_id = OBJECT_ID(N'[Security].[DeniedUserRight]'))
ALTER TABLE [Security].[DeniedUserRight] CHECK CONSTRAINT [CK__DeniedUse__WhoCr__4CEC12D3]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Appli__38E51A26]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedRo__Appli__38E51A26] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Appli__38E51A26]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] CHECK CONSTRAINT [CK__GrantedRo__Appli__38E51A26]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Right__39D93E5F]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedRo__Right__39D93E5F] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__Right__39D93E5F]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] CHECK CONSTRAINT [CK__GrantedRo__Right__39D93E5F]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__RoleN__3BC186D1]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedRo__RoleN__3BC186D1] CHECK  ((''<>ltrim(rtrim([RoleName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__RoleN__3BC186D1]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] CHECK CONSTRAINT [CK__GrantedRo__RoleN__3BC186D1]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__WhoCr__3DA9CF43]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedRo__WhoCr__3DA9CF43] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedRo__WhoCr__3DA9CF43]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedRoleRight]'))
ALTER TABLE [Security].[GrantedRoleRight] CHECK CONSTRAINT [CK__GrantedRo__WhoCr__3DA9CF43]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Appli__548D349B]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedUs__Appli__548D349B] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Appli__548D349B]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] CHECK CONSTRAINT [CK__GrantedUs__Appli__548D349B]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Right__558158D4]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedUs__Right__558158D4] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Right__558158D4]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] CHECK CONSTRAINT [CK__GrantedUs__Right__558158D4]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Usern__5769A146]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedUs__Usern__5769A146] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__Usern__5769A146]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] CHECK CONSTRAINT [CK__GrantedUs__Usern__5769A146]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__WhoCr__5951E9B8]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight]  WITH CHECK ADD  CONSTRAINT [CK__GrantedUs__WhoCr__5951E9B8] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__GrantedUs__WhoCr__5951E9B8]') AND parent_object_id = OBJECT_ID(N'[Security].[GrantedUserRight]'))
ALTER TABLE [Security].[GrantedUserRight] CHECK CONSTRAINT [CK__GrantedUs__WhoCr__5951E9B8]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__Appli__00D5CB2D]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken]  WITH CHECK ADD  CONSTRAINT [CK__OAuthToke__Appli__00D5CB2D] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__Appli__00D5CB2D]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken] CHECK CONSTRAINT [CK__OAuthToke__Appli__00D5CB2D]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__WhoCr__02BE139F]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken]  WITH CHECK ADD  CONSTRAINT [CK__OAuthToke__WhoCr__02BE139F] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__OAuthToke__WhoCr__02BE139F]') AND parent_object_id = OBJECT_ID(N'[Security].[OAuthToken]'))
ALTER TABLE [Security].[OAuthToken] CHECK CONSTRAINT [CK__OAuthToke__WhoCr__02BE139F]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__Applicati__11CB4D05]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right]  WITH CHECK ADD  CONSTRAINT [CK__Right__Applicati__11CB4D05] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__Applicati__11CB4D05]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] CHECK CONSTRAINT [CK__Right__Applicati__11CB4D05]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__RightName__12BF713E]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right]  WITH CHECK ADD  CONSTRAINT [CK__Right__RightName__12BF713E] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__RightName__12BF713E]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] CHECK CONSTRAINT [CK__Right__RightName__12BF713E]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__WhoCreate__14A7B9B0]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right]  WITH CHECK ADD  CONSTRAINT [CK__Right__WhoCreate__14A7B9B0] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Right__WhoCreate__14A7B9B0]') AND parent_object_id = OBJECT_ID(N'[Security].[Right]'))
ALTER TABLE [Security].[Right] CHECK CONSTRAINT [CK__Right__WhoCreate__14A7B9B0]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Appli__1A609306]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource]  WITH CHECK ADD  CONSTRAINT [CK__RightReso__Appli__1A609306] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Appli__1A609306]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] CHECK CONSTRAINT [CK__RightReso__Appli__1A609306]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Resou__1F254823]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource]  WITH CHECK ADD  CONSTRAINT [CK__RightReso__Resou__1F254823] CHECK  ((''<>ltrim(rtrim([Resource]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Resou__1F254823]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] CHECK CONSTRAINT [CK__RightReso__Resou__1F254823]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Right__1B54B73F]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource]  WITH CHECK ADD  CONSTRAINT [CK__RightReso__Right__1B54B73F] CHECK  ((''<>ltrim(rtrim([RightName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__Right__1B54B73F]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] CHECK CONSTRAINT [CK__RightReso__Right__1B54B73F]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__WhoCr__1E3123EA]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource]  WITH CHECK ADD  CONSTRAINT [CK__RightReso__WhoCr__1E3123EA] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RightReso__WhoCr__1E3123EA]') AND parent_object_id = OBJECT_ID(N'[Security].[RightResource]'))
ALTER TABLE [Security].[RightResource] CHECK CONSTRAINT [CK__RightReso__WhoCr__1E3123EA]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__Applicatio__6F763501]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role]  WITH CHECK ADD  CONSTRAINT [CK__Role__Applicatio__6F763501] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__Applicatio__6F763501]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] CHECK CONSTRAINT [CK__Role__Applicatio__6F763501]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__RoleName__706A593A]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role]  WITH CHECK ADD  CONSTRAINT [CK__Role__RoleName__706A593A] CHECK  ((''<>ltrim(rtrim([RoleName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__RoleName__706A593A]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] CHECK CONSTRAINT [CK__Role__RoleName__706A593A]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__WhoCreated__7252A1AC]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role]  WITH CHECK ADD  CONSTRAINT [CK__Role__WhoCreated__7252A1AC] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__Role__WhoCreated__7252A1AC]') AND parent_object_id = OBJECT_ID(N'[Security].[Role]'))
ALTER TABLE [Security].[Role] CHECK CONSTRAINT [CK__Role__WhoCreated__7252A1AC]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Applic__7CD0301F]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser]  WITH CHECK ADD  CONSTRAINT [CK__RoleUser__Applic__7CD0301F] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Applic__7CD0301F]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] CHECK CONSTRAINT [CK__RoleUser__Applic__7CD0301F]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__RoleNa__7DC45458]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser]  WITH CHECK ADD  CONSTRAINT [CK__RoleUser__RoleNa__7DC45458] CHECK  ((''<>ltrim(rtrim([RoleName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__RoleNa__7DC45458]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] CHECK CONSTRAINT [CK__RoleUser__RoleNa__7DC45458]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Userna__7FAC9CCA]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser]  WITH CHECK ADD  CONSTRAINT [CK__RoleUser__Userna__7FAC9CCA] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__Userna__7FAC9CCA]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] CHECK CONSTRAINT [CK__RoleUser__Userna__7FAC9CCA]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__WhoCre__0194E53C]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser]  WITH CHECK ADD  CONSTRAINT [CK__RoleUser__WhoCre__0194E53C] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__RoleUser__WhoCre__0194E53C]') AND parent_object_id = OBJECT_ID(N'[Security].[RoleUser]'))
ALTER TABLE [Security].[RoleUser] CHECK CONSTRAINT [CK__RoleUser__WhoCre__0194E53C]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Applicatio__010AD557]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User]  WITH CHECK ADD  CONSTRAINT [CK__User__Applicatio__010AD557] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Applicatio__010AD557]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] CHECK CONSTRAINT [CK__User__Applicatio__010AD557]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Username__01FEF990]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User]  WITH CHECK ADD  CONSTRAINT [CK__User__Username__01FEF990] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__Username__01FEF990]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] CHECK CONSTRAINT [CK__User__Username__01FEF990]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__WhoCreated__03E74202]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User]  WITH CHECK ADD  CONSTRAINT [CK__User__WhoCreated__03E74202] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__User__WhoCreated__03E74202]') AND parent_object_id = OBJECT_ID(N'[Security].[User]'))
ALTER TABLE [Security].[User] CHECK CONSTRAINT [CK__User__WhoCreated__03E74202]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Appli__2DDD7BCE]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval]  WITH CHECK ADD  CONSTRAINT [CK__UserAppro__Appli__2DDD7BCE] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Appli__2DDD7BCE]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] CHECK CONSTRAINT [CK__UserAppro__Appli__2DDD7BCE]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Usern__2ED1A007]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval]  WITH CHECK ADD  CONSTRAINT [CK__UserAppro__Usern__2ED1A007] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__Usern__2ED1A007]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] CHECK CONSTRAINT [CK__UserAppro__Usern__2ED1A007]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__WhoCr__31AE0CB2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval]  WITH CHECK ADD  CONSTRAINT [CK__UserAppro__WhoCr__31AE0CB2] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserAppro__WhoCr__31AE0CB2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserApproval]'))
ALTER TABLE [Security].[UserApproval] CHECK CONSTRAINT [CK__UserAppro__WhoCr__31AE0CB2]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Appli__226BC922]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment]  WITH CHECK ADD  CONSTRAINT [CK__UserComme__Appli__226BC922] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Appli__226BC922]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] CHECK CONSTRAINT [CK__UserComme__Appli__226BC922]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Usern__235FED5B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment]  WITH CHECK ADD  CONSTRAINT [CK__UserComme__Usern__235FED5B] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__Usern__235FED5B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] CHECK CONSTRAINT [CK__UserComme__Usern__235FED5B]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__WhoCr__263C5A06]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment]  WITH CHECK ADD  CONSTRAINT [CK__UserComme__WhoCr__263C5A06] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserComme__WhoCr__263C5A06]') AND parent_object_id = OBJECT_ID(N'[Security].[UserComment]'))
ALTER TABLE [Security].[UserComment] CHECK CONSTRAINT [CK__UserComme__WhoCr__263C5A06]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Appli__6EB71AF2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken]  WITH CHECK ADD  CONSTRAINT [CK__UserConfi__Appli__6EB71AF2] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Appli__6EB71AF2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] CHECK CONSTRAINT [CK__UserConfi__Appli__6EB71AF2]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Token__75641881]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken]  WITH CHECK ADD  CONSTRAINT [CK__UserConfi__Token__75641881] CHECK  ((''<>ltrim(rtrim([Token]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Token__75641881]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] CHECK CONSTRAINT [CK__UserConfi__Token__75641881]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Usern__6FAB3F2B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken]  WITH CHECK ADD  CONSTRAINT [CK__UserConfi__Usern__6FAB3F2B] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__Usern__6FAB3F2B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] CHECK CONSTRAINT [CK__UserConfi__Usern__6FAB3F2B]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__WhoCr__7287ABD6]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken]  WITH CHECK ADD  CONSTRAINT [CK__UserConfi__WhoCr__7287ABD6] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserConfi__WhoCr__7287ABD6]') AND parent_object_id = OBJECT_ID(N'[Security].[UserConfirmationToken]'))
ALTER TABLE [Security].[UserConfirmationToken] CHECK CONSTRAINT [CK__UserConfi__WhoCr__7287ABD6]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Appli__1605F23D]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail]  WITH CHECK ADD  CONSTRAINT [CK__UserEmail__Appli__1605F23D] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Appli__1605F23D]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] CHECK CONSTRAINT [CK__UserEmail__Appli__1605F23D]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Usern__16FA1676]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail]  WITH CHECK ADD  CONSTRAINT [CK__UserEmail__Usern__16FA1676] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__Usern__16FA1676]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] CHECK CONSTRAINT [CK__UserEmail__Usern__16FA1676]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__WhoCr__19D68321]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail]  WITH CHECK ADD  CONSTRAINT [CK__UserEmail__WhoCr__19D68321] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserEmail__WhoCr__19D68321]') AND parent_object_id = OBJECT_ID(N'[Security].[UserEmail]'))
ALTER TABLE [Security].[UserEmail] CHECK CONSTRAINT [CK__UserEmail__WhoCr__19D68321]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Appli__1C7DE5A2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserLocal__Appli__1C7DE5A2] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Appli__1C7DE5A2]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] CHECK CONSTRAINT [CK__UserLocal__Appli__1C7DE5A2]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Passw__241F076A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserLocal__Passw__241F076A] CHECK  (((0)<=[PasswordFormat] AND [PasswordFormat]<=(2)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Passw__241F076A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] CHECK CONSTRAINT [CK__UserLocal__Passw__241F076A]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Usern__1D7209DB]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserLocal__Usern__1D7209DB] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__Usern__1D7209DB]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] CHECK CONSTRAINT [CK__UserLocal__Usern__1D7209DB]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__WhoCr__204E7686]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserLocal__WhoCr__204E7686] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocal__WhoCr__204E7686]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLocalLogin]'))
ALTER TABLE [Security].[UserLocalLogin] CHECK CONSTRAINT [CK__UserLocal__WhoCr__204E7686]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Appli__3A4352B3]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut]  WITH CHECK ADD  CONSTRAINT [CK__UserLocke__Appli__3A4352B3] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Appli__3A4352B3]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] CHECK CONSTRAINT [CK__UserLocke__Appli__3A4352B3]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Usern__3B3776EC]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut]  WITH CHECK ADD  CONSTRAINT [CK__UserLocke__Usern__3B3776EC] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__Usern__3B3776EC]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] CHECK CONSTRAINT [CK__UserLocke__Usern__3B3776EC]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__WhoCr__3E13E397]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut]  WITH CHECK ADD  CONSTRAINT [CK__UserLocke__WhoCr__3E13E397] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLocke__WhoCr__3E13E397]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLockedOut]'))
ALTER TABLE [Security].[UserLockedOut] CHECK CONSTRAINT [CK__UserLocke__WhoCr__3E13E397]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__5126B80B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Appli__5126B80B] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__5126B80B]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] CHECK CONSTRAINT [CK__UserLogin__Appli__5126B80B]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__521ADC44]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Usern__521ADC44] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__521ADC44]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginFailure]'))
ALTER TABLE [Security].[UserLoginFailure] CHECK CONSTRAINT [CK__UserLogin__Usern__521ADC44]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__326D26C1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Appli__326D26C1] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__326D26C1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] CHECK CONSTRAINT [CK__UserLogin__Appli__326D26C1]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Token__391A2450]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Token__391A2450] CHECK  ((''<>ltrim(rtrim([Token]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Token__391A2450]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] CHECK CONSTRAINT [CK__UserLogin__Token__391A2450]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__33614AFA]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Usern__33614AFA] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__33614AFA]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] CHECK CONSTRAINT [CK__UserLogin__Usern__33614AFA]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__WhoCr__363DB7A5]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__WhoCr__363DB7A5] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__WhoCr__363DB7A5]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginResetToken]'))
ALTER TABLE [Security].[UserLoginResetToken] CHECK CONSTRAINT [CK__UserLogin__WhoCr__363DB7A5]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__4891720A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Appli__4891720A] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Appli__4891720A]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] CHECK CONSTRAINT [CK__UserLogin__Appli__4891720A]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__49859643]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess]  WITH CHECK ADD  CONSTRAINT [CK__UserLogin__Usern__49859643] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserLogin__Usern__49859643]') AND parent_object_id = OBJECT_ID(N'[Security].[UserLoginSuccess]'))
ALTER TABLE [Security].[UserLoginSuccess] CHECK CONSTRAINT [CK__UserLogin__Usern__49859643]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Appli__0C477DD9]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserOAuth__Appli__0C477DD9] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Appli__0C477DD9]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [CK__UserOAuth__Appli__0C477DD9]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__12F47B68]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserOAuth__Provi__12F47B68] CHECK  ((''<>ltrim(rtrim([Provider]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__12F47B68]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [CK__UserOAuth__Provi__12F47B68]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__13E89FA1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserOAuth__Provi__13E89FA1] CHECK  ((''<>ltrim(rtrim([ProviderUsername]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Provi__13E89FA1]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [CK__UserOAuth__Provi__13E89FA1]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Usern__0D3BA212]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserOAuth__Usern__0D3BA212] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__Usern__0D3BA212]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [CK__UserOAuth__Usern__0D3BA212]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__WhoCr__10180EBD]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin]  WITH CHECK ADD  CONSTRAINT [CK__UserOAuth__WhoCr__10180EBD] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserOAuth__WhoCr__10180EBD]') AND parent_object_id = OBJECT_ID(N'[Security].[UserOAuthLogin]'))
ALTER TABLE [Security].[UserOAuthLogin] CHECK CONSTRAINT [CK__UserOAuth__WhoCr__10180EBD]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Appli__5BA4467E]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [CK__UserPassw__Appli__5BA4467E] CHECK  ((''<>ltrim(rtrim([ApplicationName]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Appli__5BA4467E]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] CHECK CONSTRAINT [CK__UserPassw__Appli__5BA4467E]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Passw__63456846]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [CK__UserPassw__Passw__63456846] CHECK  (((0)<=[PasswordAnswerFormat] AND [PasswordAnswerFormat]<=(2)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Passw__63456846]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] CHECK CONSTRAINT [CK__UserPassw__Passw__63456846]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Usern__5C986AB7]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [CK__UserPassw__Usern__5C986AB7] CHECK  ((''<>ltrim(rtrim([Username]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__Usern__5C986AB7]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] CHECK CONSTRAINT [CK__UserPassw__Usern__5C986AB7]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__WhoCr__5F74D762]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [CK__UserPassw__WhoCr__5F74D762] CHECK  ((''<>ltrim(rtrim([WhoCreated]))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Security].[CK__UserPassw__WhoCr__5F74D762]') AND parent_object_id = OBJECT_ID(N'[Security].[UserPasswordQuestionAnswer]'))
ALTER TABLE [Security].[UserPasswordQuestionAnswer] CHECK CONSTRAINT [CK__UserPassw__WhoCr__5F74D762]
GO