﻿using System.Linq;
namespace Oits.Web.Security { 

	[System.AttributeUsage( ( System.AttributeTargets.Field | System.AttributeTargets.Property ), AllowMultiple = false, Inherited = true )]
	public sealed class PasswordStrengthRegularExpressionAttribute : PasswordValidatorAttributeBase { 

		#region fields
		private const System.String theDefaultErrorMessage = "'{0}' does neet meet the Password Strength Regular Expression.";

		private const System.String thePatternErrorMessage = "The field {0} must match the regular expression '{1}'.";
		#endregion fields


		#region .ctor
		public PasswordStrengthRegularExpressionAttribute() : this( theDefaultErrorMessage ) { 
		}
		public PasswordStrengthRegularExpressionAttribute( System.String errorMessage ) : this( null, errorMessage.TrimToNull() ) { 
		}
		public PasswordStrengthRegularExpressionAttribute( System.String providerName, System.String errorMessage ) : base( providerName.TrimToNull(), errorMessage.TrimToNull() ?? theDefaultErrorMessage ) { 
		}
		#endregion .ctor


		#region methods
		public sealed override System.String FormatErrorMessage( System.String name ) { 
			return base.FormatErrorMessage( this.ErrorMessageString, name );
		}
		protected sealed override System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) {
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var regex = this.Provider.PasswordStrengthRegularExpression.TrimToNull();
			if ( System.String.IsNullOrEmpty( regex ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( System.Text.RegularExpressions.Regex.IsMatch( value, regex ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return this.CreateValidationFailure( validationContext );
			}
		}

		public sealed override System.Collections.Generic.IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules( System.Web.Mvc.ModelMetadata metadata, System.Web.Mvc.ControllerContext context ) { 
			System.Collections.Generic.ICollection<System.Web.Mvc.ModelClientValidationRule> output = new System.Collections.Generic.List<System.Web.Mvc.ModelClientValidationRule>( 1 );

			var pattern = this.Provider.PasswordStrengthRegularExpression.TrimToNull();
			if ( !System.String.IsNullOrEmpty( pattern ) ) { 
				var regexRule = this.CreateRule( "password", this.ErrorMessageString, metadata, pattern );
				regexRule.ValidationParameters.Add( "regex", pattern );
				output.Add( regexRule );
			}

			return output;
		}
		#endregion methods

	}

}