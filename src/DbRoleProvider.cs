﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public class DbRoleProvider : DbRoleProviderBase { 

		#region internal classes
		protected class ParametersBase : Oits.Configuration.DbMap.ReturnValueBase {
			public ParametersBase() : base() {
			}

			public System.String ApplicationName {
				get;
				set;
			}
			public System.String RoleName {
				get;
				set;
			}
			public System.String RequestingUser {
				get {
					if ( System.Web.HttpContext.Current.User.Identity.IsAuthenticated ) {
						return System.Web.HttpContext.Current.User.Identity.Name;
					} else {
						return System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				set {
					;
				}
			}
			public System.String Username {
				get;
				set;
			}
			public System.Boolean ThrowOnPopulatedRole {
				get;
				set;
			}
			public System.Boolean DeleteRelatedData { 
				get;
				set;
			}
		}
		#endregion internal classes


		#region .ctor
		public DbRoleProvider() : base() { 
		}
		#endregion .ctor


		#region methods
		public sealed override void Initialize( string name, System.Collections.Specialized.NameValueCollection config ) { 
			if ( null == config ) { 
				throw new System.ArgumentNullException( "config" );
			}
			if ( System.String.IsNullOrEmpty( name ) ) { 
				name = "DbRoleProvider";
			}
			if ( System.String.IsNullOrEmpty( config[ "description" ].TrimToNull() ) ) { 
				config.Remove( "description" );
				config.Add( "description", "Oits.Web.Security.DbRoleProvider implementation." );
			}

			base.Initialize( name, config );
		}

		protected override void AddUsersToRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> users, System.Collections.Generic.IEnumerable<System.String> roles ) { 
			if ( ( null == roles ) || !roles.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roles" );
			} else if ( ( null == users ) || !users.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "users" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var cmd = this.DbMapGroup.Commands[ "AddUserToRole" ];
			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			foreach ( var role in roles ) { 
				p.RoleName = role.CheckParameter( true, true, true, "roleName" );
				if ( !this.RoleExists( connection, transaction, p.RoleName ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The specified roleName is invalid or does not exist for the current application." );
				}
				foreach ( var user in users ) { 
					p.ReturnValue = 0;
					p.Username = user.CheckParameter( true, true, true, "username" );
					cmd.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnParameter( p.ReturnValue );
				}
			}
		}
		protected override void CreateRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName,
				RoleName = roleName 
			};
			this.DbMapGroup.Commands[ "CreateRole" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnParameter( p.ReturnValue );
		}
		protected override System.Boolean DeleteRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.Boolean throwOnPopulatedRole ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			return this.DeleteRole( connection, transaction, roleName, throwOnPopulatedRole, true );
		}
		protected override System.Boolean DeleteRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.Boolean throwOnPopulatedRole, System.Boolean deleteRelatedData ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RoleName = roleName, 
				ThrowOnPopulatedRole = throwOnPopulatedRole, 
				DeleteRelatedData = deleteRelatedData 
			};
			this.DbMapGroup.Commands[ "DeleteRole" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override System.String[] FindUsersInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String usernameToMatch ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName,
				RoleName = roleName,
				Username = usernameToMatch.TrimToNull()  
			};
			return new System.Collections.Generic.List<System.String>( this.DbMapGroup.Commands[ "FindUsersInRole" ].ExecuteReaderSingleResult<System.String>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "UserRole" ], 
				( reader, config ) => { 
					System.Int32 o = reader.GetOrdinal( config.Columns[ "Username" ].ColumnName );
					return reader.IsDBNull( o ) ? (System.String)null : reader.GetString( o ).TrimToNull();
				} 
			).Where<System.String>( 
				x => !System.String.IsNullOrEmpty( x ) 
			) ).ToArray();
		}
		protected override System.String[] GetAllRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) { 
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			return new System.Collections.Generic.List<System.String>( this.DbMapGroup.Commands[ "GetAllRoles" ].ExecuteReaderSingleResult<System.String>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "UserRole" ], 
				( reader, config ) => { 
					System.Int32 o = reader.GetOrdinal( config.Columns[ "RoleName" ].ColumnName );
					return reader.IsDBNull( o ) ? (System.String)null : reader.GetString( o ).TrimToNull();
				} 
			).Where<System.String>( 
				x => !System.String.IsNullOrEmpty( x.TrimToNull() ) 
			) ).ToArray();
		}
		protected override System.String[] GetRolesForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return new System.Collections.Generic.List<System.String>( this.DbMapGroup.Commands[ "GetRolesForUser" ].ExecuteReaderSingleResult<System.String>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "UserRole" ], 
				( reader, config ) => { 
					System.Int32 o = reader.GetOrdinal( config.Columns[ "RoleName" ].ColumnName );
					return reader.IsDBNull( o ) ? (System.String)null : reader.GetString( o ).TrimToNull();
				} 
			).Where<System.String>( 
				x => !System.String.IsNullOrEmpty( x.TrimToNull() ) 
			) ).ToArray();
		}
		protected override System.String[] GetUsersInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RoleName = roleName 
			};
			return new System.Collections.Generic.List<System.String>( this.DbMapGroup.Commands[ "GetUsersInRole" ].ExecuteReaderSingleResult<System.String>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "UserRole" ], 
				( reader, config ) => { 
					System.Int32 o = reader.GetOrdinal( config.Columns[ "Username" ].ColumnName );
					return reader.IsDBNull( o ) ? (System.String)null : reader.GetString( o ).TrimToNull();
				} 
			).Where<System.String>( 
				x => !System.String.IsNullOrEmpty( x ) 
			) ).ToArray();
		}
		protected override System.Boolean IsUserInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				RoleName = roleName 
			};
			return this.DbMapGroup.Commands[ "GetIsUserInRole" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override void RemoveUsersFromRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> users, System.Collections.Generic.IEnumerable<System.String> roles ) { 
			if ( ( null == roles ) || !roles.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roles" );
			} else if ( ( null == users ) || !users.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "users" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var cmd = this.DbMapGroup.Commands[ "RemoveUserRole" ];
			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				ReturnValue = 0 
			};
			foreach ( var role in roles ) { 
				p.RoleName = role.CheckParameter( true, true, true, "roleName" );
				if ( !this.RoleExists( connection, transaction, p.RoleName ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The specified roleName is invalid or does not exist for the current application." );
				}
				foreach ( var user in users ) { 
					p.ReturnValue = 0;
					p.Username = user.CheckParameter( true, true, true, "username" );
					cmd.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnParameter( p.ReturnValue );
				}
			}
		}
		protected override System.Boolean RoleExists( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RoleName = roleName 
			};
			return this.DbMapGroup.Commands[ "RoleExists" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		#endregion methods

	}

}