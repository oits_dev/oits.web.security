﻿using System.Linq;
using System.Web;
using System.Web.Security;

namespace Oits.Web.Security { 

	public static class Rights { 

		#region fields
		private static System.Object theLock;
		private static System.Boolean theIsInitialized;
		private static System.Boolean theIsEnabled;
		private static RightProviderBase theDefaultProvider;
		private static System.Boolean theIsDefaultProviderInitialized;
		private static RightProviderCollection theProviders;
		private static System.Boolean theAreProvidersInitialized;
		#endregion fields


		#region .ctor
		static Rights() { 
			theLock = new System.Object();
			theIsInitialized = false;
			theIsDefaultProviderInitialized = false;
			theAreProvidersInitialized = false;
		}
		#endregion .ctor


		#region properties
		public static RightProviderBase Provider { 
			get { 
				EnsureEnabled();
				if ( null == theDefaultProvider ) { 
					throw new System.InvalidCastException( "The default RightProvider is not specified." );
				}
				return theDefaultProvider;
			}
		}
		public static RightProviderCollection Providers { 
			get { 
				EnsureEnabled();
				return theProviders;
			}
		}
		public static System.Boolean Enabled { 
			get { 
				EnsureEnabled();
				return theIsEnabled;
			}
		}
		public static System.String ApplicationName { 
			get { 
				EnsureEnabled();
				return Provider.ApplicationName;
			}
			set { 
				EnsureEnabled();
				Provider.ApplicationName = value;
			}
		}
		#endregion properties


		#region methods
		#region helpers
		private static void EnsureEnabled() { 
			Initialize();
			if ( !theIsEnabled ) { 
				throw new System.Configuration.Provider.ProviderException( "Rights feature is not enabled." );
			}
		}
		private static void Initialize() { 
			if ( theIsInitialized ) { 
				return;
			}
			lock ( theLock ) { 
				RightManagerSection config = (RightManagerSection)System.Configuration.ConfigurationManager.GetSection( RightManagerSection.DefaultSectionName );
				if ( null != config ) { 
					theIsEnabled = config.Enabled;
					InitializeSettings( config );
					InitializeDefaultProvider( config );
					theIsInitialized = true;
				}
			}
		}
		private static void InitializeSettings( RightManagerSection config ) { 
			if ( theIsInitialized ) { 
				return;
			}
			theProviders = new RightProviderCollection();
			System.Web.Configuration.ProvidersHelper.InstantiateProviders( config.Providers, theProviders, typeof( RightProviderBase ) );
			theAreProvidersInitialized = true;
		}
		private static void InitializeDefaultProvider( RightManagerSection config ) { 
			if ( theIsDefaultProviderInitialized ) { 
				return;
			}
			if ( theAreProvidersInitialized ) { 
				theProviders.SetReadOnly();
				if ( System.String.IsNullOrEmpty( config.DefaultProvider ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The default RightProvider is not specified." );
				}
				theDefaultProvider = theProviders[ config.DefaultProvider ];
				theIsDefaultProviderInitialized = true;
			}
		}
		#endregion helpers

		#region RightProvider
		public static void ClearUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			EnsureEnabled();
			Provider.ClearUsersRights( usernames, rightNames );
		}
		public static void ClearUsersRight( System.Collections.Generic.IEnumerable<System.String> usernames, System.String rightName ) { 
			EnsureEnabled();
			Provider.ClearUsersRights( usernames, new System.String[] { rightName } );
		}
		public static void ClearUserRights( System.String username, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.ClearUsersRights( new System.String[] { username }, rightNames );
		}
		public static void ClearUserRight( System.String username, System.String rightName ) { 
			EnsureEnabled();
			Provider.ClearUsersRights( new System.String[] { username }, new System.String[] { rightName } );
		}

		public static void ClearRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.ClearRolesRights( roleNames, rightNames );
		}
		public static void ClearRolesRight( System.Collections.Generic.IEnumerable<System.String> roleNames, System.String rightName ) {
			EnsureEnabled();
			Provider.ClearRolesRights( roleNames, new System.String[] { rightName } );
		}
		public static void ClearRoleRights( System.String roleName, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.ClearRolesRights( new System.String[] { roleName }, rightNames );
		}
		public static void ClearRoleRight( System.String roleName, System.String rightName ) {
			EnsureEnabled();
			Provider.ClearRolesRights( new System.String[] { roleName }, new System.String[] { rightName } );
		}

		public static void CreateRight( System.String rightName ) { 
			EnsureEnabled();
			Provider.CreateRight( rightName );
		}
		public static void CreateRight( System.String rightName, System.String resource ) { 
			EnsureEnabled();
			Provider.CreateRight( rightName, resource );
		}

		public static System.Boolean DeleteRight( System.String rightName ) { 
			EnsureEnabled();
			return Provider.DeleteRight( rightName );
		}
		public static System.Boolean DeleteRight( System.String rightName, System.Boolean throwIfPopulated ) { 
			EnsureEnabled();
			return Provider.DeleteRight( rightName, throwIfPopulated );
		}
		public static System.Boolean DeleteRight( System.String rightName, System.Boolean throwIfPopulated, System.Boolean deleteRelatedData ) { 
			EnsureEnabled();
			return Provider.DeleteRight( rightName, throwIfPopulated, deleteRelatedData );
		}

		public static void DenyUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			EnsureEnabled();
			Provider.DenyUsersRights( usernames, rightNames );
		}
		public static void DenyUsersRight( System.Collections.Generic.IEnumerable<System.String> usernames, System.String rightName ) { 
			EnsureEnabled();
			Provider.DenyUsersRights( usernames, new System.String[] { rightName } );
		}
		public static void DenyUserRights( System.String username, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.DenyUsersRights( new System.String[] { username }, rightNames );
		}
		public static void DenyUserRight( System.String username, System.String rightName ) { 
			EnsureEnabled();
			Provider.DenyUsersRights( new System.String[] { username }, new System.String[] { rightName } );
		}

		public static void DenyRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			EnsureEnabled();
			Provider.DenyRolesRights( roleNames, rightNames );
		}
		public static void DenyRolesRight( System.Collections.Generic.IEnumerable<System.String> roleNames, System.String rightName ) { 
			EnsureEnabled();
			Provider.DenyRolesRights( roleNames, new System.String[] { rightName } );
		}
		public static void DenyRoleRights( System.String roleName, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.DenyRolesRights( new System.String[] { roleName }, rightNames );
		}
		public static void DenyRoleRight( System.String roleName, System.String rightName ) { 
			EnsureEnabled();
			Provider.DenyRolesRights( new System.String[] { roleName }, new System.String[] { rightName } );
		}

		public static System.Collections.Generic.IList<Right> FindRightsByResource( System.String resourceToMatch ) { 
			EnsureEnabled();
			return Provider.FindRightsByResource( resourceToMatch );
		}
		public static System.Collections.Generic.IList<Right> FindRightsByRightName( System.String rightNameToMatch ) { 
			EnsureEnabled();
			return Provider.FindRightsByRightName( rightNameToMatch );
		}

		public static System.Collections.Generic.IList<Right> GetAllRights() { 
			EnsureEnabled();
			return Provider.GetAllRights();
		}

		public static System.Collections.Generic.IList<Right> GetGrantedRightsForUser( System.String username ) { 
			EnsureEnabled();
			return Provider.GetGrantedRightsForUser( username );
		}
		public static System.Collections.Generic.IList<Right> GetDeniedRightsForUser( System.String username ) {
			EnsureEnabled();
			return Provider.GetDeniedRightsForUser( username );
		}
		public static System.Collections.Generic.IList<Right> GetGrantedRightsForRole( System.String roleName ) {
			EnsureEnabled();
			return Provider.GetGrantedRightsForRole( roleName );
		}
		public static System.Collections.Generic.IList<Right> GetDeniedRightsForRole( System.String roleName ) { 
			EnsureEnabled();
			return Provider.GetDeniedRightsForRole( roleName );
		}

		public static Right GetRight( System.String rightName ) { 
			EnsureEnabled();
			return Provider.GetRight( rightName );
		}
		public static Right GetRightByResource( System.String resource ) { 
			EnsureEnabled();
			return Provider.GetRightByResource( resource );
		}

		public static void GrantUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			EnsureEnabled();
			Provider.GrantUsersRights( usernames, rightNames );
		}
		public static void GrantUsersRight( System.Collections.Generic.IEnumerable<System.String> usernames, System.String rightName ) { 
			EnsureEnabled();
			Provider.GrantUsersRights( usernames, new System.String[] { rightName } );
		}
		public static void GrantUserRights( System.String username, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.GrantUsersRights( new System.String[] { username }, rightNames );
		}
		public static void GrantUserRight( System.String username, System.String rightName ) { 
			EnsureEnabled();
			Provider.GrantUsersRights( new System.String[] { username }, new System.String[] { rightName } );
		}

		public static void GrantRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			EnsureEnabled();
			Provider.GrantRolesRights( roleNames, rightNames );
		}
		public static void GrantRolesRight( System.Collections.Generic.IEnumerable<System.String> roleNames, System.String rightName ) {
			EnsureEnabled();
			Provider.GrantRolesRights( roleNames, new System.String[] { rightName } );
		}
		public static void GrantRoleRights( System.String roleName, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			EnsureEnabled();
			Provider.GrantRolesRights( new System.String[] { roleName }, rightNames );
		}
		public static void GrantRoleRight( System.String roleName, System.String rightName ) {
			EnsureEnabled();
			Provider.GrantRolesRights( new System.String[] { roleName }, new System.String[] { rightName } );
		}

		public static System.Boolean IsUserGrantedRight( System.String username, System.String rightName ) { 
			EnsureEnabled();
			return Provider.IsUserGrantedRight( username, rightName );
		}
		public static System.Boolean IsUserDeniedRight( System.String username, System.String rightName ) { 
			EnsureEnabled();
			return Provider.IsUserDeniedRight( username, rightName );
		}
		public static System.Boolean IsRoleGrantedRight( System.String roleName, System.String rightName ) { 
			EnsureEnabled();
			return Provider.IsRoleGrantedRight( roleName, rightName );
		}
		public static System.Boolean IsRoleDeniedRight( System.String roleName, System.String rightName ) { 
			EnsureEnabled();
			return Provider.IsRoleDeniedRight( roleName, rightName );
		}
		public static System.Boolean RightExists( System.String rightName ) { 
			EnsureEnabled();
			return Provider.RightExists( rightName );
		}
		public static void SetRightResource( System.String rightName, System.String resource ) { 
			EnsureEnabled();
			Provider.SetRightResource( rightName, resource );
		}
		#endregion RightProvider
		#endregion methods

	}

}