﻿using System.Linq;

namespace Oits.Web.Security { 

	public class Right : System.IEquatable<Right> { 

		#region internal classes
		private sealed class HashCode { 
			public HashCode() : base() { 
			}

			public override System.Int32 GetHashCode() { 
				return System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			}
		}
		#endregion intnernal classes


		#region fields
		private readonly static System.Int32 theHashCode;

		private readonly System.String myRightName;
		private readonly System.String myResource;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Right() { 
			theHashCode = new HashCode().GetHashCode();
		}

		public Right( System.String rightName, System.String resource ) : base() { 
			myRightName = rightName.CheckParameter( true, true, true, "rightName" );
			myResource = resource.TrimToNull();

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += myRightName.GetHashCode();
				if ( !System.String.IsNullOrEmpty( myResource ) ) { 
					myHashCode += myResource.GetHashCode();
				}
			}
		}
		#endregion .ctor


		#region properties
		public System.String RightName { 
			get { 
				return myRightName;
			}
		}
		public System.String Resource { 
			get { 
				return myResource;
			}
		}
		#endregion properties


		#region methods
		public override System.Int32 GetHashCode() { 
			return myHashCode;
		}

		public override System.Boolean Equals( System.Object obj ) { 
			if ( obj is Right ) { 
				return this.Equals( obj as Right );
			} else { 
				return false;
			}
		}
		public System.Boolean Equals( Right other ) { 
			if ( null == other ) { 
				return false;
			} else if ( System.Object.ReferenceEquals( this, other ) ) { 
				return true;
			} else if ( this.GetHashCode() != other.GetHashCode() ) { 
				return false;
			} else { 
				return ( 
					this.RightName.Equals( other.RightName, System.StringComparison.InvariantCultureIgnoreCase ) 
					&& System.String.Equals( this.Resource, other.Resource, System.StringComparison.OrdinalIgnoreCase ) 
				);
			}
		}
		#endregion methods


		#region static methods
		public static System.Boolean operator ==( Right a, Right b ) { 
			if ( ( null == (System.Object)a ) && ( null == (System.Object)b ) ) { 
				return true;
			} else if ( ( null == (System.Object)a ) || ( null == (System.Object)b ) ) {
				return false;
			} else { 
				return a.Equals( b );
			}
		}
		public static System.Boolean operator !=( Right a, Right b ) { 
			return !( a == b );
		}
		#endregion static methods

	}

}