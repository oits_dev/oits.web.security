﻿using System.Linq;
namespace Oits.Web.Security { 

	[System.AttributeUsage( ( System.AttributeTargets.Field | System.AttributeTargets.Property ), AllowMultiple = false, Inherited = true )]
	public sealed class PasswordMinRequiredNonalphanumericCharactersAttribute : PasswordValidatorAttributeBase { 

		#region fields
		private const System.String theDefaultErrorMessage = "'{0}' must containt at least '{1:g}' non-alphanumeric characters.";

		private const System.String theMinRequiredNonalphanumericErrorMessage = "The field {0} must be a string with a minimum of {1} non-alphanumeric characters.";
		#endregion fields


		#region .ctor
		public PasswordMinRequiredNonalphanumericCharactersAttribute() : this( null ) { 
		}
		public PasswordMinRequiredNonalphanumericCharactersAttribute( System.String errorMessage ) : this( null, errorMessage.TrimToNull() ) { 
		}
		public PasswordMinRequiredNonalphanumericCharactersAttribute( System.String providerName, System.String errorMessage ) : base( providerName.TrimToNull(), errorMessage.TrimToNull() ?? theDefaultErrorMessage ) { 
		}
		#endregion .ctor


		#region methods
		public sealed override System.String FormatErrorMessage( System.String name ) { 
			return base.FormatErrorMessage( this.ErrorMessageString, name, this.Provider.MinRequiredNonAlphanumericCharacters );
		}
		protected sealed override System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var minNaCount = this.Provider.MinRequiredNonAlphanumericCharacters;
			if ( 0 == minNaCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}
			var naCount = value.Count( 
				x => !System.Char.IsLetterOrDigit( x ) 
			);
			if ( minNaCount <= naCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return this.CreateValidationFailure( validationContext );
			}
		}

		public sealed override System.Collections.Generic.IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules( System.Web.Mvc.ModelMetadata metadata, System.Web.Mvc.ControllerContext context ) { 
			System.Collections.Generic.ICollection<System.Web.Mvc.ModelClientValidationRule> output = new System.Collections.Generic.List<System.Web.Mvc.ModelClientValidationRule>( 1 );

			var nonalphamin = this.Provider.MinRequiredNonAlphanumericCharacters;
			if ( 0 < nonalphamin ) { 
				var nonalphaminRule = this.CreateRule( "password", this.ErrorMessageString, metadata, nonalphamin );
				nonalphaminRule.ValidationParameters.Add( "nonalphamin", nonalphamin );
				output.Add( nonalphaminRule );
			}

			return output;
		}
		#endregion methods

	}

}