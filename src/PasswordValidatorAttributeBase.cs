﻿using System.Linq;
namespace Oits.Web.Security { 

	[System.AttributeUsage( ( System.AttributeTargets.Field | System.AttributeTargets.Property ), AllowMultiple = false, Inherited = true )]
	public abstract class PasswordValidatorAttributeBase : System.ComponentModel.DataAnnotations.ValidationAttribute, System.Web.Mvc.IClientValidatable { 

		#region fields
		protected const System.String DefaultDisplayName = "Password";
		private const System.String theDefaultErrorMessage = "'{0}' is invalid.";

		private readonly System.String myProviderName;
		#endregion fields


		#region .ctor
		protected PasswordValidatorAttributeBase() : this( theDefaultErrorMessage ) { 
		}
		protected PasswordValidatorAttributeBase( System.String errorMessage ) : this( null, errorMessage.TrimToNull() ) { 
		}
		protected PasswordValidatorAttributeBase( System.String providerName, System.String errorMessage ) : base( errorMessage.TrimToNull() ?? theDefaultErrorMessage ) { 
			myProviderName = providerName.TrimToNull();
		}
		#endregion .ctor


		#region properties
		protected System.String ProviderName { 
			get { 
				return myProviderName;
			}
		}
		protected System.Web.Security.MembershipProvider Provider { 
			get { 
				var name = this.ProviderName;
				if ( System.String.IsNullOrEmpty( name ) ) { 
					return System.Web.Security.Membership.Provider;
				} else { 
					return System.Web.Security.Membership.Providers[ name ];
				}
			}
		}
		#endregion properties


		#region methods
		public override System.String FormatErrorMessage( System.String name ) { 
			return System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.ErrorMessageString, name );
		}
		public virtual System.String FormatErrorMessage( System.String name, params System.Object[] args ) { 
			return System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.ErrorMessageString, name, args );
		}
		public virtual System.String FormatErrorMessage( System.String errorMessage, System.String name ) { 
			return System.String.Format( System.Globalization.CultureInfo.CurrentCulture, errorMessage, name );
		}
		public virtual System.String FormatErrorMessage( System.String errorMessage, System.String name, params System.Object[] args ) { 
			return System.String.Format( System.Globalization.CultureInfo.CurrentCulture, errorMessage, name, args );
		}

		public sealed override System.Boolean IsValid( System.Object value ) { 
			return this.IsValid( value as System.String );
		}
		protected virtual System.Boolean IsValid( System.String value ) { 
			return this.IsValid( value, null ).Equals( System.ComponentModel.DataAnnotations.ValidationResult.Success );
		}
		protected sealed override System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.Object value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			return this.IsValid( ( value as System.String ), validationContext );
		}
		protected abstract System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext );
		protected virtual System.ComponentModel.DataAnnotations.ValidationResult CreateValidationFailure( System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null != validationContext ) { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( validationContext.DisplayName ), new System.String[] { validationContext.MemberName } );
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( DefaultDisplayName ) );
			}
		}

		public abstract System.Collections.Generic.IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules( System.Web.Mvc.ModelMetadata metadata, System.Web.Mvc.ControllerContext context );

		protected virtual System.String GetDisplayName( System.Web.Mvc.ModelMetadata metadata ) { 
			if ( null == metadata ) { 
				return DefaultDisplayName;
			} else { 
				return metadata.GetDisplayName().TrimToNull() ?? DefaultDisplayName;
			}
		}

		protected virtual System.Web.Mvc.ModelClientValidationRule CreateRule( System.String validationType, System.String errorMessage, System.Web.Mvc.ModelMetadata metadata ) { 
			return this.CreateRule( validationType, errorMessage, metadata, null );
		}
		protected virtual System.Web.Mvc.ModelClientValidationRule CreateRule( System.String validationType, System.String errorMessage, System.Web.Mvc.ModelMetadata metadata, params System.Object[] args ) { 
			if ( ( null == args ) && ( 0 == args.Length ) ) { 
				return new System.Web.Mvc.ModelClientValidationRule { 
					ValidationType = validationType.TrimToNull(), 
					ErrorMessage = this.FormatErrorMessage( errorMessage, this.GetDisplayName( metadata ) ) 
				};
			} else { 
				return new System.Web.Mvc.ModelClientValidationRule { 
					ValidationType = validationType.TrimToNull(), 
					ErrorMessage = this.FormatErrorMessage( errorMessage, this.GetDisplayName( metadata ), args ) 
				};
			}
		}
		#endregion methods

	}

}