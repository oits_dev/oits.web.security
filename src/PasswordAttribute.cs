﻿using System.Linq;
namespace Oits.Web.Security { 

	[System.AttributeUsage( ( System.AttributeTargets.Field | System.AttributeTargets.Property ), AllowMultiple = false, Inherited = true )]
	public sealed class PasswordAttribute : PasswordValidatorAttributeBase { 

		#region fields
		private const System.String theDefaultMinLengthErrorMessage = "'{0}' must be at least '{1:g}' characters long.";
		private const System.String theDefaultMinRequiredNonalphanumericCharactersErrorMessage = "'{0}' must containt at least '{1:g}' non-alphanumeric characters.";
		private const System.String theDefaultStrengthRegularExpressionErrorMessage = "'{0}' does neet meet the Password Strength Regular Expression.";

		private readonly System.String myMinLengthErrorMessage;
		private readonly System.String myMinRequiredNonalphanumericCharactersErrorMessage;
		private readonly System.String myStrengthRegularExpressionErrorMessage;
		#endregion fields


		#region .ctor
		public PasswordAttribute( System.String providerName ) : this( providerName.TrimToNull(), null, null, null ) { 
		}
		public PasswordAttribute( System.String providerName, System.String minLengthErrorMessage, System.String minRequiredNonalphanumericCharactersErrorMessage, System.String strengthRegularExpressionErrorMessage ) : this( providerName.TrimToNull(), null, minLengthErrorMessage.TrimToNull(), minRequiredNonalphanumericCharactersErrorMessage.TrimToNull(), strengthRegularExpressionErrorMessage.TrimToNull() ) { 
		}
		public PasswordAttribute( System.String providerName, System.String defaultErrorMessage, System.String minLengthErrorMessage, System.String minRequiredNonalphanumericCharactersErrorMessage, System.String strengthRegularExpressionErrorMessage ) : base( providerName.TrimToNull(), defaultErrorMessage.TrimToNull() ) { 
			myMinLengthErrorMessage = minLengthErrorMessage.TrimToNull() ?? theDefaultMinLengthErrorMessage;
			myMinRequiredNonalphanumericCharactersErrorMessage = minRequiredNonalphanumericCharactersErrorMessage.TrimToNull() ?? theDefaultMinRequiredNonalphanumericCharactersErrorMessage;
			myStrengthRegularExpressionErrorMessage = strengthRegularExpressionErrorMessage.TrimToNull() ?? theDefaultStrengthRegularExpressionErrorMessage;
		}
		#endregion .ctor


		#region properties
		public System.String MinLengthErrorMessage { 
			get { 
				return myMinLengthErrorMessage;
			}
		}
		public System.String MinRequiredNonalphanumericCharactersErrorMessage { 
			get { 
				return myMinRequiredNonalphanumericCharactersErrorMessage;
			}
		}
		public System.String StrengthRegularExpressionErrorMessage { 
			get { 
				return myStrengthRegularExpressionErrorMessage;
			}
		}
		#endregion properties


		#region methods
		private System.ComponentModel.DataAnnotations.ValidationResult IsValidMinLength( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var minCount = this.Provider.MinRequiredPasswordLength;
			if ( 0 == minCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( minCount <= value.Length ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null != validationContext ) { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( validationContext.DisplayName ), new System.String[] { validationContext.MemberName } );
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( DefaultDisplayName ) );
			}
		}
		private System.ComponentModel.DataAnnotations.ValidationResult IsValidMinRequiredNonalphanumericCharacters( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var minNaCount = this.Provider.MinRequiredNonAlphanumericCharacters;
			if ( 0 == minNaCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}
			var naCount = value.Count( 
				x => !System.Char.IsLetterOrDigit( x ) 
			);
			if ( minNaCount <= naCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null != validationContext ) { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( validationContext.DisplayName ), new System.String[] { validationContext.MemberName } );
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( DefaultDisplayName ) );
			}
		}
		private System.ComponentModel.DataAnnotations.ValidationResult IsValidStrengthRegularExpression( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var regex = this.Provider.PasswordStrengthRegularExpression.TrimToNull();
			if ( System.String.IsNullOrEmpty( regex ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( System.Text.RegularExpressions.Regex.IsMatch( value, regex ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null != validationContext ) { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( validationContext.DisplayName ), new System.String[] { validationContext.MemberName } );
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( this.FormatErrorMessage( DefaultDisplayName ) );
			}
		}

		protected sealed override System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			var success = System.ComponentModel.DataAnnotations.ValidationResult.Success;
			if ( null == value ) { 
				return success;
			}

			var probe = this.IsValidMinLength( value, validationContext );
			if ( !success.Equals( probe ) ) { 
				return probe;
			}

			probe = this.IsValidMinRequiredNonalphanumericCharacters( value, validationContext );
			if ( !success.Equals( probe ) ) { 
				return probe;
			}

			probe = this.IsValidStrengthRegularExpression( value, validationContext );
			if ( !success.Equals( probe ) ) { 
				return probe;
			}

			return success;
		}

		public sealed override System.Collections.Generic.IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules( System.Web.Mvc.ModelMetadata metadata, System.Web.Mvc.ControllerContext context ) { 
			System.Collections.Generic.ICollection<System.Web.Mvc.ModelClientValidationRule> output = new System.Collections.Generic.List<System.Web.Mvc.ModelClientValidationRule>( 3 );

			var p = this.Provider;
			var min = p.MinRequiredPasswordLength;
			if ( 0 < min ) { 
				var minRule = this.CreateRule( "password", this.MinLengthErrorMessage, metadata, min );
				minRule.ValidationParameters.Add( "min", min );
				output.Add( minRule );
			}

			var nonalphamin = p.MinRequiredNonAlphanumericCharacters;
			if ( 0 < nonalphamin ) { 
				var nonalphaminRule = this.CreateRule( "password", this.MinRequiredNonalphanumericCharactersErrorMessage, metadata, nonalphamin );
				nonalphaminRule.ValidationParameters.Add( "nonalphamin", nonalphamin );
				output.Add( nonalphaminRule );
			}

			var pattern = p.PasswordStrengthRegularExpression.TrimToNull();
			if ( !System.String.IsNullOrEmpty( pattern ) ) { 
				var regexRule = this.CreateRule( "password", this.StrengthRegularExpressionErrorMessage, metadata, pattern );
				regexRule.ValidationParameters.Add( "regex", pattern );
				output.Add( regexRule );
			}

			return output;
		}
		#endregion methods

	}

}