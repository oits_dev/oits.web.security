﻿using System.Linq;
using System.Web;
using System.Web.Security;

namespace Oits.Web.Security { 

	public class RightProviderCollection : System.Configuration.Provider.ProviderCollection { 

		#region .ctor
		public RightProviderCollection() : base() { 
		}
		#endregion .ctor


		#region properties
		new public RightProviderBase this[ System.String name ] { 
			get { 
				return (RightProviderBase)base[ name ];
			}
		}
		#endregion properties


		#region methods
		public override void Add( System.Configuration.Provider.ProviderBase provider ) { 
			if ( null == provider ) { 
				throw new System.ArgumentNullException( "provider" );
			} else if ( !( provider is RightProviderBase ) ) { 
				throw new System.ArgumentException( System.String.Format( "Provider must implement {0}", typeof( RightProviderBase ) ), "provider" );
			}
			base.Add( provider );
		}
		public void CopyTo( RightProviderBase[] array, System.Int32 index ) { 
			base.CopyTo( array, index );
		}
		#endregion methods

	}

}