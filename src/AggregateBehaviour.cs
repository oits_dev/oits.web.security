﻿namespace Oits.Web.Security { 

	public enum AggregateBehaviour : int { 
		Any = 0, 
		All = System.Int32.MaxValue 
	}

}