﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public class DbMembershipProvider : DbMembershipProviderBase { 

		#region internal classes
		protected class BasicUserParameters : ParameterValuesBase { 
			public BasicUserParameters() : base() { 
			}

			public System.String Username { 
				get;
				set;
			}
		}
		protected sealed class GetUserLocalLoginPasswordOptionParameters : BasicUserParameters { 
			public GetUserLocalLoginPasswordOptionParameters() : base() { 
			}
		}
		protected sealed class GetUserPasswordQuestionAnswerOptionParameters : BasicUserParameters { 
			public GetUserPasswordQuestionAnswerOptionParameters() : base() { 
			}
		}
		protected sealed class ValidateUserParameters : BasicUserParameters { 
			public ValidateUserParameters() : base() { 
			}

			public System.String Password { 
				get;
				set;
			}
			public System.Boolean UpdateLastLogin { 
				get;
				set;
			}
			public System.Boolean UpdateLastActive { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
			public System.Int32 MaxInvalidPasswordAttempts { 
				get;
				set;
			}
			public System.Int32 PasswordAttemptWindow { 
				get;
				set;
			}
		}
		protected sealed class UpdateMembershipUserParameters : BasicUserParameters { 
			public UpdateMembershipUserParameters() : base() { 
			}

			public System.Boolean IsApproved { 
				get;
				set;
			}
			public System.String Email { 
				get;
				set;
			}
			public System.Boolean RequireUniqueEmail { 
				get;
				set;
			}
			public System.String Comment { 
				get;
				set;
			}
			public System.DateTime LastActivity { 
				get;
				set;
			}
			public System.DateTime LastLogin { 
				get;
				set;
			}
		}
		protected sealed class UnlockUserParameters : BasicUserParameters { 
			public UnlockUserParameters() : base() { 
			}
		}
		protected sealed class ValidatePasswordAnswerParameters : BasicUserParameters { 
			public ValidatePasswordAnswerParameters() : base() { 
			}

			public System.String PasswordAnswer { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}
		protected sealed class SetPasswordParameters : BasicUserParameters { 
			public SetPasswordParameters() : base() { 
			}

			public System.String Password { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat Format { 
				get;
				set;
			}
			public System.String Algorithm { 
				get;
				set;
			}
			public System.String Salt { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}
		protected sealed class GetUserNameByEmailParameters : ParameterValuesBase { 
			public GetUserNameByEmailParameters() : base() { 
			}

			public System.String Email { 
				get;
				set;
			}
		}
		protected sealed class GetUserByUsernameParameters : BasicUserParameters { 
			public GetUserByUsernameParameters() : base() { 
			}

			public System.Boolean UpdateLastActive { 
				get;
				set;
			}
		}
		protected sealed class GetUserByUserIdParameters : ParameterValuesBase { 
			public GetUserByUserIdParameters() : base() { 
			}

			public System.Object ProviderUserKey { 
				get;
				set;
			}
			public System.Boolean UpdateLastActive { 
				get;
				set;
			}
		}
		protected sealed class GetPasswordPasswordParameters : BasicUserParameters { 
			public GetPasswordPasswordParameters() : base() { 
			}

			public System.String PasswordAnswer { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}
		protected sealed class GetNumberOfUsersOnlineParameters : ParameterValuesBase { 
			public GetNumberOfUsersOnlineParameters() : base() { 
			}

			public System.Int32 UserIsOnlineTimeWindow { 
				get;
				set;
			}
		}
		protected sealed class GetAllUsersParameters : ParameterValuesBase { 
			public GetAllUsersParameters() : base() { 
			}
		}
		protected sealed class FindMembershipUsersByNameParameters : ParameterValuesBase { 
			public FindMembershipUsersByNameParameters() : base() { 
			}

			public System.String UsernameToMatch { 
				get;
				set;
			}
		}
		protected sealed class FindMembershipUsersByEmailParameters : ParameterValuesBase { 
			public FindMembershipUsersByEmailParameters() : base() { 
			}

			public System.String EmailToMatch { 
				get;
				set;
			}
		}
		protected sealed class DeleteUserParameters : BasicUserParameters { 
			public DeleteUserParameters() : base() { 
			}

			public System.Boolean DeleteAllRelatedData { 
				get;
				set;
			}
		}
		protected sealed class CreateMembershipUserParameters : BasicUserParameters { 
			public CreateMembershipUserParameters() : base() { 
			}

			public System.Object ProviderUserKey { 
				get;
				set;
			}
			public System.String Password { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat PasswordFormat { 
				get;
				set;
			}
			public System.String PasswordAlgorithm { 
				get;
				set;
			}
			public System.String PasswordSalt { 
				get;
				set;
			}
			public System.String Email { 
				get;
				set;
			}
			public System.Boolean RequireUniqueEmail { 
				get;
				set;
			}
			public System.String PasswordQuestion { 
				get;
				set;
			}
			public System.String PasswordAnswer { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat PasswordAnswerFormat { 
				get;
				set;
			}
			public System.String PasswordAnswerAlgorithm { 
				get;
				set;
			}
			public System.String PasswordAnswerSalt { 
				get;
				set;
			}
			public System.Boolean RequirePasswordQuestionAndAnswer { 
				get;
				set;
			}
			public System.Boolean IsApproved { 
				get;
				set;
			}
		}
		protected sealed class ChangePasswordQuestionAndAnswerParameters : BasicUserParameters { 
			public ChangePasswordQuestionAndAnswerParameters() : base() { 
			}

			public System.String Password { 
				get;
				set;
			}
			public System.String PasswordQuestion { 
				get;
				set;
			}
			public System.String PasswordAnswer { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat PasswordAnswerFormat { 
				get;
				set;
			}
			public System.String PasswordAnswerAlgorithm { 
				get;
				set;
			}
			public System.String PasswordAnswerSalt { 
				get;
				set;
			}
			public System.Boolean RequirePasswordQuestionAndAnswer { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}
		protected class ChangePasswordParameters : BasicUserParameters { 
			public ChangePasswordParameters() : base() { 
			}

			public System.String OldPassword { 
				get;
				set;
			}
			public System.String Password { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat PasswordFormat { 
				get;
				set;
			}
			public System.String PasswordAlgorithm { 
				get;
				set;
			}
			public System.String PasswordSalt { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}

		protected class ResetPasswordWithTokenParameters : ParameterValuesBase { 
			public ResetPasswordWithTokenParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
			public System.Int32 TokenLifetime { 
				get;
				set;
			}
			public System.String Password { 
				get;
				set;
			}
			public System.Web.Security.MembershipPasswordFormat PasswordFormat { 
				get;
				set;
			}
			public System.String PasswordAlgorithm { 
				get;
				set;
			}
			public System.String PasswordSalt { 
				get;
				set;
			}
			public System.Boolean FailIfNotApproved { 
				get;
				set;
			}
		}
		protected sealed class IsConfirmedParameters : BasicUserParameters { 
			public IsConfirmedParameters() : base() { 
			}
		}
		protected sealed class GetUserIdFromPasswordResetTokenParameters : ParameterValuesBase { 
			public GetUserIdFromPasswordResetTokenParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
			public System.Int32 TokenLifetime { 
				get;
				set;
			}
		}
		protected sealed class GetPasswordFailuresSinceLastSuccessParameters : BasicUserParameters { 
			public GetPasswordFailuresSinceLastSuccessParameters() : base() { 
			}
		}
		protected sealed class GetPasswordChangedDateParameters : BasicUserParameters { 
			public GetPasswordChangedDateParameters() : base() { 
			}
		}
		protected sealed class GetLastPasswordFailureDateParameters : BasicUserParameters { 
			public GetLastPasswordFailureDateParameters() : base() { 
			}
		}
		protected sealed class GetCreateDateParameters : BasicUserParameters { 
			public GetCreateDateParameters() : base() { 
			}
		}
		protected sealed class SetTokenParameters : BasicUserParameters { 
			public SetTokenParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
		}
		protected sealed class ConfirmAccountParameters : BasicUserParameters { 
			public ConfirmAccountParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
			public System.Int32 TokenLifetime { 
				get;
				set;
			}
		}
		protected sealed class GetAccountsForUserParameters : BasicUserParameters { 
			public GetAccountsForUserParameters() : base() { 
			}
		}
		protected sealed class HasLocalAccountParameters : ParameterValuesBase { 
			public HasLocalAccountParameters() : base() { 
			}

			public System.Int32 UserId { 
				get;
				set;
			}
		}
		protected sealed class GetUsernameByUserIdParameters : ParameterValuesBase { 
			public GetUsernameByUserIdParameters() : base() { 
			}

			public System.Int32 UserId { 
				get;
				set;
			}
		}
		protected sealed class CreateOrUpdateOAuthAccountParameters : BasicUserParameters { 
			public CreateOrUpdateOAuthAccountParameters() : base() { 
			}

			public System.String Provider { 
				get;
				set;
			}
			public System.String ProviderUsername { 
				get;
				set;
			}
		}
		protected sealed class DeleteUserOAuthLoginParameters : ParameterValuesBase { 
			public DeleteUserOAuthLoginParameters() : base() { 
			}

			public System.String Provider { 
				get;
				set;
			}
			public System.String ProviderUsername { 
				get;
				set;
			}
		}
		protected sealed class DeleteOAuthTokenParameters : ParameterValuesBase { 
			public DeleteOAuthTokenParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
		}
		protected sealed class GetOAuthTokenSecretParameters : ParameterValuesBase { 
			public GetOAuthTokenSecretParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
		}
		protected sealed class UserIdFromUserOAuthLoginParameters : ParameterValuesBase { 
			public UserIdFromUserOAuthLoginParameters() : base() { 
			}

			public System.String Provider { 
				get;
				set;
			}
			public System.String ProviderUsername { 
				get;
				set;
			}
		}
		protected sealed class ReplaceOAuthRequestTokenWithAccessTokenParameters : ParameterValuesBase { 
			public ReplaceOAuthRequestTokenWithAccessTokenParameters() : base() { 
			}

			public System.String RequestToken { 
				get;
				set;
			}
			public System.String AccessToken { 
				get;
				set;
			}
			public System.String AccessTokenSecret { 
				get;
				set;
			}
		}
		protected sealed class StoreOAuthRequestTokenParameters : ParameterValuesBase { 
			public StoreOAuthRequestTokenParameters() : base() { 
			}

			public System.String Token { 
				get;
				set;
			}
			public System.String RequestTokenSecret { 
				get;
				set;
			}
		}
		#endregion internal classes


		#region .ctor
		public DbMembershipProvider() : base() { 
		}
		#endregion .ctor


		#region methods
		public sealed override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) { 
			if ( null == config ) { 
				throw new System.ArgumentNullException( "config" );
			}
			if ( System.String.IsNullOrEmpty( name ) ) { 
				name = "DbMembershipProvider";
			}
			if ( System.String.IsNullOrEmpty( config[ "description" ].TrimToNull() ) ) { 
				config.Remove( "description" );
				config.Add( "description", "Oits.Web.Security.DbMembershipProvider implementation." );
			}

			base.Initialize( name, config );
		}

		#region utilities
		protected virtual UserPasswordOption GetUserPasswordFormatOption( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var group = this.DbMapGroup;
			var commandConfig = group.Commands[ "GetUserLocalLoginPasswordOption" ];
			var p = new GetUserLocalLoginPasswordOptionParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return commandConfig.ExecuteReaderSingleRow<UserPasswordOption>( connection, transaction, p, 
				group.Results[ "GetUserLocalLoginPasswordOption" ] 
			) ?? new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};
		}
		protected virtual UserPasswordOption GetUserPasswordFormatOption( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object providerUserKey ) { 
			if ( null == providerUserKey ) { 
				throw new System.ArgumentNullException( "providerUserKey" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var user = this.GetUser( connection, transaction, (System.Object)providerUserKey, false );
			if ( null == user ) { 
				return new UserPasswordOption { 
					Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
				};
			} else { 
				return this.GetUserPasswordFormatOption( connection, transaction, user.UserName );
			}
		}
		protected virtual UserPasswordOption GetUserPasswordQuestionAnswerFormatOption( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var group = this.DbMapGroup;
			var commandConfig = group.Commands[ "GetUserPasswordQuestionAnswerOption" ];
			var p = new GetUserPasswordQuestionAnswerOptionParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return commandConfig.ExecuteReaderSingleRow<UserPasswordOption>( connection, transaction, p, 
				group.Results[ "GetUserPasswordQuestionAnswerOption" ] 
			) ?? new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};
		}
		#endregion utilities

		#region ExtendedMembershipProvider extentions
		public override System.String GetUserNameFromId( System.Int32 userId ) { 
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetUserNameFromId( cnxn, null, userId );
			}
		}

		protected override System.Boolean ConfirmAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String accountConfirmationToken ) { 
			accountConfirmationToken = accountConfirmationToken.TrimToNull();
			if ( System.String.IsNullOrEmpty( accountConfirmationToken ) ) { 
				throw new System.ArgumentNullException( "accountConfirmationToken" );
			}
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ConfirmAccountParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username.TrimToNull(), 
				Token = accountConfirmationToken, 
				TokenLifetime = this.TokenLifetime 
			};
			this.DbMapGroup.Commands[ "ConfirmAccount" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override void SetConfirmationToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String token ) { 
			this.CheckReturnValue( (System.Int32)this.SetToken( connection, transaction, username, token, "SetConfirmationToken" ) );
		}
		protected override void CreateOrUpdateOAuthAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId, System.String userName ) { 
			userName = userName.CheckParameter( true, true, true, "userName" );
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new CreateOrUpdateOAuthAccountParameters { 
				ApplicationName = this.ApplicationName, 
				Username = userName, 
				Provider = provider, 
				ProviderUsername = providerUserId 
			};
			this.DbMapGroup.Commands[ "CreateOrUpdateOAuthAccount" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		protected override void DeleteOAuthAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId ) { 
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new DeleteUserOAuthLoginParameters { 
				ApplicationName = this.ApplicationName, 
				Provider = provider, 
				ProviderUsername = providerUserId 
			};
			this.DbMapGroup.Commands[ "DeleteOAuthAccount" ].ExecuteNonQuery( connection, transaction, p );
			if ( 0 != p.ReturnValue ) { 
				this.CheckReturnValue( (System.Int32)System.Web.Security.MembershipCreateStatus.ProviderError );
			}
		}
		protected override void DeleteOAuthToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token ) { 
			token = token.TrimToNull().CheckParameter( false, true, true, "token" );

			var p = new DeleteOAuthTokenParameters { 
				ApplicationName = this.ApplicationName, 
				Token = token 
			};
			this.DbMapGroup.Commands[ "DeleteOAuthTokenByToken" ].ExecuteNonQuery( connection, transaction, p );
			if ( 0 != p.ReturnValue ) { 
				this.CheckReturnValue( (System.Int32)System.Web.Security.MembershipCreateStatus.ProviderError );
			}
		}
		protected override System.Boolean SetPasswordResetToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String token ) { 
			return ( System.Web.Security.MembershipCreateStatus.Success == this.SetToken( connection, transaction, username, token, "SetPasswordResetToken" ) );
		}
		protected virtual System.Web.Security.MembershipCreateStatus SetToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String token, System.String commandName ) { 
			commandName = commandName.TrimToNull();
			if ( System.String.IsNullOrEmpty( commandName ) ) { 
				throw new System.ArgumentNullException( "commandName" );
			}

			token = token.TrimToNull();
			if ( System.String.IsNullOrEmpty( token ) ) { 
				throw new System.ArgumentNullException( "token" );
			}

			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var command = this.DbMapGroup.Commands[ commandName ];
			if ( null == command ) { 
				throw new System.ArgumentOutOfRangeException( "commandName" );
			}
			var p = new SetTokenParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				Token = token 
			};
			command.ExecuteNonQuery( connection, transaction, p );
			return (System.Web.Security.MembershipCreateStatus)p.ReturnValue;
		}
		protected override System.Collections.Generic.ICollection<WebMatrix.WebData.OAuthAccountData> GetAccountsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetAccountsForUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetAccountsForUser" ].ExecuteReaderSingleResult<WebMatrix.WebData.OAuthAccountData>( connection, transaction, p, 
				group.Results[ "GetAccountsForUser" ], 
				this.ReadOathAccountData 
			);
		}
		protected override WebMatrix.WebData.OAuthAccountData ReadOathAccountData( System.Data.Common.DbDataReader reader, Oits.Configuration.DbMap.DbResultElement resultConfig ) { 
			if ( null == resultConfig ) { 
				throw new System.ArgumentNullException( "resultConfig" );
			} else if ( null == reader ) { 
				throw new System.ArgumentNullException( "reader" );
			} else if ( reader.IsClosed || !reader.HasRows ) { 
				return null;
			}

			WebMatrix.WebData.OAuthAccountData output = null;
			try { 
				System.Int32 index;

				index = reader.GetOrdinal( resultConfig.Columns[ "Provider" ].ColumnName );
				System.String provider = reader.GetString( index ).TrimToNull();

				index = reader.GetOrdinal( resultConfig.Columns[ "ProviderUsername" ].ColumnName );
				System.String providerUsername = reader.GetString( index ).TrimToNull();

				output = new WebMatrix.WebData.OAuthAccountData( provider, providerUsername );
			} catch ( System.Exception e ) { 
				throw new System.Configuration.Provider.ProviderException( e.Message, e );
			}
			return output;
		}
		protected override System.DateTime GetCreateDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetCreateDateParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return this.DbMapGroup.Commands[ "GetCreateDate" ].ExecuteScalar<System.DateTime>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? MinSqlDateTime : System.Convert.ToDateTime( x ) 
			);
		}
		protected override System.DateTime GetLastPasswordFailureDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetLastPasswordFailureDateParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return this.DbMapGroup.Commands[ "GetLastPasswordFailureDate" ].ExecuteScalar<System.DateTime>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? MinSqlDateTime : System.Convert.ToDateTime( x ) 
			);
		}
		protected override System.String GetOAuthTokenSecret( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token ) { 
			token = token.TrimToNull().CheckParameter( false, true, true, "token" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetOAuthTokenSecretParameters { 
				ApplicationName = this.ApplicationName, 
				Token = token 
			};
			return this.DbMapGroup.Commands[ "GetOAuthTokenSecretByToken" ].ExecuteScalar<System.String>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? (System.String)null : System.Convert.ToString( x ).TrimToNull() 
			);
		}
		protected override System.DateTime GetPasswordChangedDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetPasswordChangedDateParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return this.DbMapGroup.Commands[ "GetPasswordChangedDate" ].ExecuteScalar<System.DateTime>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? MinSqlDateTime : System.Convert.ToDateTime( x ) 
			);
		}
		protected override System.Int32 GetPasswordFailuresSinceLastSuccess( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetPasswordFailuresSinceLastSuccessParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return this.DbMapGroup.Commands[ "GetPasswordFailuresSinceLastSuccess" ].ExecuteScalar<System.Int32>( connection, transaction, p, 
				x => ( System.DBNull.Value.Equals( x ) ? 0 : System.Convert.ToInt32( x ) ) 
			);
		}
		protected override System.Int32 GetUserIdFromPasswordResetToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token ) { 
			token = token.TrimToNull();
			if ( null == token ) { 
				throw new System.ArgumentNullException( "token" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetUserIdFromPasswordResetTokenParameters { 
				ApplicationName = this.ApplicationName, 
				Token = token, 
				TokenLifetime = this.TokenLifetime 
			};
			var output = this.DbMapGroup.Commands[ "GetUserIdFromPasswordResetToken" ].ExecuteScalar<System.Int32>( connection, transaction, p, 
				x => ( System.DBNull.Value.Equals( x ) ? -1 : System.Convert.ToInt32( x ) ) 
			);
			return output;
		}
		protected override System.Int32 GetUserIdFromOAuth( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId ) { 
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );

			var p = new UserIdFromUserOAuthLoginParameters { 
				ApplicationName = this.ApplicationName, 
				Provider = provider, 
				ProviderUsername = providerUserId 
			};
			return this.DbMapGroup.Commands[ "GetUserIdFromUserOAuthLogin" ].ExecuteScalar<System.Int32>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? -1 : System.Convert.ToInt32( x ) 
			);
		}
		protected override System.String GetUserNameFromId( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Int32 userId ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetUsernameByUserIdParameters { 
				ApplicationName = this.ApplicationName, 
				UserId = userId 
			};
			return this.DbMapGroup.Commands[ "GetUserNameFromId" ].ExecuteScalar<System.String>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? (System.String)null : System.Convert.ToString( x ).TrimToNull() 
			);
		}
		protected override System.Boolean HasLocalAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Int32 userId ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new HasLocalAccountParameters { 
				ApplicationName = this.ApplicationName, 
				UserId = userId 
			};
			return this.DbMapGroup.Commands[ "HasLocalAccount" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override System.Boolean IsConfirmed( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new IsConfirmedParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			return this.DbMapGroup.Commands[ "IsConfirmed" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override void ReplaceOAuthRequestTokenWithAccessToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String requestToken, System.String accessToken, System.String accessTokenSecret ) { 
			accessTokenSecret = accessTokenSecret.TrimToNull().CheckParameter( false, true, true, "accessTokenSecret" );
			accessToken = accessToken.TrimToNull().CheckParameter( false, true, true, "accessToken" );
			requestToken = requestToken.TrimToNull().CheckParameter( false, true, true, "requestToken" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ReplaceOAuthRequestTokenWithAccessTokenParameters { 
				ApplicationName = this.ApplicationName, 
				RequestToken = requestToken, 
				AccessToken = accessToken, 
				AccessTokenSecret = accessTokenSecret 
			};
			this.DbMapGroup.Commands[ "ReplaceOAuthRequestTokenWithAccessToken" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		protected override System.Boolean ResetPasswordWithToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token, System.String newPassword ) { 
			newPassword = newPassword.TrimToNull().CheckPasswdParameter( this, "newPassword" );
			token = token.TrimToNull();
			if ( System.String.IsNullOrEmpty( token ) ) { 
				throw new System.ArgumentNullException( "token" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var option = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};
			var p = new ResetPasswordWithTokenParameters { 
				ApplicationName = this.ApplicationName, 
				Token = token, 
				TokenLifetime = this.TokenLifetime, 
				PasswordAlgorithm = option.Algorithm, 
				PasswordFormat = option.Format, 
				PasswordSalt = option.Salt, 
				Password = this.GetEncodedPassword( newPassword, option ), 
				FailIfNotApproved = this.RequireConfirmation 
			};
			this.DbMapGroup.Commands[ "ResetPasswordWithToken" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override void StoreOAuthRequestToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String requestToken, System.String requestTokenSecret ) { 
			requestTokenSecret = requestTokenSecret.TrimToNull().CheckParameter( false, true, true, "requestTokenSecret" );
			requestToken = requestTokenSecret.TrimToNull().CheckParameter( false, true, true, "requestToken" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new StoreOAuthRequestTokenParameters { 
				ApplicationName = this.ApplicationName, 
				Token = requestToken, 
				RequestTokenSecret = requestTokenSecret 
			};
			this.DbMapGroup.Commands[ "StoreOAuthRequestToken" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		#endregion ExtendedMembershipProvider extentions

		#region MembershipProvider extentions
		protected override System.Boolean ChangePassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String oldPassword, System.String newPassword ) { 
			newPassword = newPassword.CheckPasswdParameter( this, "newPassword" );
			oldPassword = oldPassword.TrimToNull();
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var oldOption = this.GetUserPasswordFormatOption( connection, transaction, username );
			var newOption = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};
			var p = new ChangePasswordParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				Password = this.GetEncodedPassword( newPassword, newOption ), 
				PasswordFormat = newOption.Format, 
				PasswordAlgorithm = newOption.Algorithm, 
				FailIfNotApproved = this.RequireConfirmation, 
				PasswordSalt = newOption.Salt 
			};
			if ( !System.String.IsNullOrEmpty( oldPassword ) ) { 
				p.OldPassword = this.GetEncodedPassword( oldPassword, oldOption );
			}
			this.DbMapGroup.Commands[ "ChangePassword" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override System.Boolean ChangePasswordQuestionAndAnswer( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.String newPasswordQuestion, System.String newPasswordAnswer ) { 
			if ( this.RequiresQuestionAndAnswer ) { 
				newPasswordQuestion = newPasswordQuestion.CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer ? 1 : 0, System.Int16.MaxValue, "newPasswordQuestion" );
				newPasswordAnswer = newPasswordAnswer.TrimToNull().CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer ? 1 : 0, System.Int16.MaxValue, "newPasswordAnswer" );
			}
			if ( !System.String.IsNullOrEmpty( newPasswordAnswer ) ) { 
				newPasswordAnswer = newPasswordAnswer.ToLower();
			}

			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var passwordOption = this.GetUserPasswordFormatOption( connection, transaction, username );
			var answerOption = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, 
				Format = this.PasswordFormat, 
				Salt = this.GenerateSalt() 
			};
			var p = new ChangePasswordQuestionAndAnswerParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				Password = this.GetEncodedString( password, passwordOption.Format, passwordOption.Algorithm, passwordOption.Salt ), 
				PasswordQuestion = newPasswordQuestion, 
				PasswordAnswerAlgorithm = answerOption.Algorithm, 
				PasswordAnswerFormat = answerOption.Format, 
				PasswordAnswerSalt = answerOption.Salt, 
				RequirePasswordQuestionAndAnswer = this.RequiresQuestionAndAnswer, 
				FailIfNotApproved = this.RequireConfirmation 
			};
			if ( !System.String.IsNullOrEmpty( newPasswordAnswer ) ) { 
				try { 
					p.PasswordAnswer = this.GetEncodedString( newPasswordAnswer, answerOption.Format, answerOption.Algorithm, answerOption.Salt );
				} catch ( System.Exception ) { 
					return false;
				}
			}
			this.DbMapGroup.Commands[ "ChangePasswordQuestionAndAnswer" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override System.Web.Security.MembershipUser CreateUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.String email, System.String passwordQuestion, System.String passwordAnswer, System.Boolean isApproved, System.Object providerUserKey, out System.Web.Security.MembershipCreateStatus status ) { 
			#region checks
			try { 
				username = username.CheckParameter( true, true, true, "username" );
			} catch ( System.Exception ) { 
				status = System.Web.Security.MembershipCreateStatus.InvalidUserName;
				return null;
			}
			try { 
				password = password.CheckPasswdParameter( this, "password" );
			} catch ( System.Exception ) { 
				status = System.Web.Security.MembershipCreateStatus.InvalidPassword;
				return null;
			}
			if ( this.RequiresQuestionAndAnswer ) { 
				try { 
					passwordQuestion = passwordQuestion.CheckParameter( false, true, true, "passwordQuestion" );
				} catch ( System.Exception ) { 
					status = System.Web.Security.MembershipCreateStatus.InvalidQuestion;
					return null;
				}
				try { 
					passwordAnswer = passwordAnswer.CheckParameter( false, true, true, "passwordAnswer" );
				} catch ( System.Exception ) { 
					status = System.Web.Security.MembershipCreateStatus.InvalidAnswer;
					return null;
				}
			} else { 
				passwordQuestion = passwordQuestion.TrimToNull();
				passwordAnswer = passwordAnswer.TrimToNull() ?? System.String.Empty;
			}
			try { 
				email = new System.Net.Mail.MailAddress( email ).Address;
			} catch ( System.Exception ) { 
				status = System.Web.Security.MembershipCreateStatus.InvalidEmail;
				return null;
			}
			if ( null != providerUserKey ) { 
				status = System.Web.Security.MembershipCreateStatus.InvalidProviderUserKey;
				return null;
			}
			#endregion checks
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			passwordAnswer = passwordAnswer.TrimToNull();
			if ( !System.String.IsNullOrEmpty( passwordAnswer ) ) { 
				passwordAnswer = passwordAnswer.ToLower();
			}

			var passwordOption = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};
			var answerOption = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt()
			};
			var p = new CreateMembershipUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				ProviderUserKey = providerUserKey, 
				PasswordFormat = passwordOption.Format, 
				PasswordAlgorithm = passwordOption.Algorithm, 
				PasswordSalt = passwordOption.Salt, 
				Email = email, 
				PasswordQuestion = passwordQuestion.TrimToNull(), 
				PasswordAnswerFormat = answerOption.Format, 
				PasswordAnswerAlgorithm = answerOption.Algorithm, 
				PasswordAnswerSalt = answerOption.Salt, 
				IsApproved = isApproved, 
				RequireUniqueEmail = this.RequiresUniqueEmail,
				RequirePasswordQuestionAndAnswer = this.RequiresQuestionAndAnswer 
			};
			try { 
				p.Password = this.GetEncodedPassword( password, passwordOption );
			} catch ( System.Exception ) { 
				status = System.Web.Security.MembershipCreateStatus.InvalidPassword;
				return null;
			}
			if ( !System.String.IsNullOrEmpty( passwordAnswer ) ) { 
				try { 
					p.PasswordAnswer = this.GetEncodedString( passwordAnswer, answerOption.Format, answerOption.Algorithm, answerOption.Salt );
				} catch ( System.Exception ) { 
					status = System.Web.Security.MembershipCreateStatus.InvalidAnswer;
					return null;
				}
			}
			this.DbMapGroup.Commands[ "CreateMembershipUser" ].ExecuteNonQuery( connection, transaction, p );
			status = (System.Web.Security.MembershipCreateStatus)p.ReturnValue;
			if ( System.Web.Security.MembershipCreateStatus.Success == status ) { 
				return this.GetUser( connection, transaction, username, false );
			} else { 
				return null;
			}
		}
		protected override System.Boolean DeleteUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.Boolean deleteAllRelatedData ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentException( "connection" );
			}

			var p = new DeleteUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				DeleteAllRelatedData = deleteAllRelatedData 
			};
			this.DbMapGroup.Commands[ "DeleteUser" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override System.Collections.Generic.IList<System.Web.Security.MembershipUser> FindUsersByEmail( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String emailToMatch ) { 
			emailToMatch = emailToMatch.TrimToNull();
			if ( null == emailToMatch ) { 
				throw new System.ArgumentNullException( "emailToMatch" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var group = this.DbMapGroup;
			var commandConfig = group.Commands[ "FindMembershipUsersByEmail" ];
			var p = new FindMembershipUsersByEmailParameters { 
				ApplicationName = this.ApplicationName, 
				EmailToMatch = emailToMatch 
			};
			var list = commandConfig.ExecuteReaderSingleResult<System.Web.Security.MembershipUser>( connection, transaction, p, 
				group.Results[ "GetUser" ], 
				this.ReadMembershipUser 
			);
			return new System.Collections.Generic.List<System.Web.Security.MembershipUser>( list ?? new System.Web.Security.MembershipUser[ 0 ] ).AsReadOnly();
		}
		protected override System.Collections.Generic.IList<System.Web.Security.MembershipUser> FindUsersByName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String usernameToMatch ) { 
			usernameToMatch = usernameToMatch.TrimToNull();
			if ( System.String.IsNullOrEmpty( usernameToMatch ) ) { 
				throw new System.ArgumentNullException( "usernameToMatch" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var group = this.DbMapGroup;
			var commandConfig = group.Commands[ "FindMembershipUsersByName" ];
			var p = new FindMembershipUsersByNameParameters { 
				ApplicationName = this.ApplicationName, 
				UsernameToMatch = usernameToMatch 
			};
			var list = commandConfig.ExecuteReaderSingleResult<System.Web.Security.MembershipUser>( connection, transaction, p, 
				group.Results[ "GetUser" ], 
				this.ReadMembershipUser 
			);
			return new System.Collections.Generic.List<System.Web.Security.MembershipUser>( list ?? new System.Web.Security.MembershipUser[ 0 ] ).AsReadOnly();
		}
		protected override System.Collections.Generic.IList<System.Web.Security.MembershipUser> GetAllUsers( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetAllUsersParameters { 
				ApplicationName = this.ApplicationName 
			};
			var list = this.DbMapGroup.Commands[ "GetAllUsers" ].ExecuteReaderSingleResult<System.Web.Security.MembershipUser>( connection, transaction, p, 
				this.DbMapGroup.Results[ "GetUser" ], 
				this.ReadMembershipUser 
			);
			return new System.Collections.Generic.List<System.Web.Security.MembershipUser>( list ?? new System.Web.Security.MembershipUser[ 0 ] ).AsReadOnly();
		}
		protected override System.Int32 GetNumberOfUsersOnline( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetNumberOfUsersOnlineParameters { 
				ApplicationName = this.ApplicationName, 
				UserIsOnlineTimeWindow = System.Web.Security.Membership.UserIsOnlineTimeWindow 
			};
			return this.DbMapGroup.Commands[ "GetNumberOfUsersOnline" ].ExecuteScalar<System.Int32>( connection, transaction, p, 
				x => ( System.DBNull.Value.Equals( x ) ? 0 : System.Convert.ToInt32( x ) ) 
			);
		}
		protected override System.String GetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer ) { 
			answer = answer.TrimToNull();
			if ( !System.String.IsNullOrEmpty( answer ) ) { 
				answer = answer.ToLower();
			}
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			} else if ( !this.EnablePasswordRetrieval ) { 
				throw new System.NotSupportedException();
			}

			var option = this.GetUserPasswordQuestionAnswerFormatOption( connection, transaction, username );
			if ( System.Web.Security.MembershipPasswordFormat.Hashed == option.Format ) { 
				throw new System.NotSupportedException();
			}

			var p = new GetPasswordPasswordParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				FailIfNotApproved = this.RequireConfirmation, 
				PasswordAnswer = this.GetEncodedString( answer, option.Format, option.Algorithm, option.Salt ) 
			};
			System.String output = null;
			switch ( option.Format ) { 
				case System.Web.Security.MembershipPasswordFormat.Clear : 
				case System.Web.Security.MembershipPasswordFormat.Encrypted : 
					output = this.DbMapGroup.Commands[ "GetPassword" ].ExecuteScalar<System.String>( connection, transaction, p, 
						x => ( System.DBNull.Value.Equals( x ) ? (System.String)null : x.ToString().TrimToNull() ) 
					);
					break;
				case System.Web.Security.MembershipPasswordFormat.Hashed : 
					throw new System.NotSupportedException();
				default : 
					throw new System.InvalidOperationException();
			}
			this.CheckReturnValue( p.ReturnValue );
			if ( 
				!System.String.IsNullOrEmpty( output ) 
				&& ( System.Web.Security.MembershipPasswordFormat.Encrypted == option.Format ) 
			) { 
				var passwordOption = this.GetUserPasswordFormatOption( connection, transaction, username );
				output = this.GetDecryptedString( output, passwordOption.Format, passwordOption.Algorithm, passwordOption.Salt ).TrimToNull();
			}

			return output;
		}
		protected override System.Web.Security.MembershipUser GetUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object providerUserKey, System.Boolean updateLastActive ) { 
			if ( null == providerUserKey ) { 
				throw new System.ArgumentNullException( "providerUserKey" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetUserByUserIdParameters { 
				ApplicationName = this.ApplicationName, 
				ProviderUserKey = providerUserKey, 
				UpdateLastActive = updateLastActive 
			};
			var output = this.DbMapGroup.Commands[ "GetUserByUserId" ].ExecuteReaderSingleRow<System.Web.Security.MembershipUser>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "GetUser" ], 
				this.ReadMembershipUser 
			);
			this.CheckReturnValue( p.ReturnValue );
			return output;
		}
		protected override System.Web.Security.MembershipUser GetUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.Boolean updateLastActive ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetUserByUsernameParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				UpdateLastActive = updateLastActive 
			};
			var output = this.DbMapGroup.Commands[ "GetUserByUsername" ].ExecuteReaderSingleRow<System.Web.Security.MembershipUser>( 
				connection, transaction, p, 
				this.DbMapGroup.Results[ "GetUser" ], 
				this.ReadMembershipUser 
			);
			this.CheckReturnValue( p.ReturnValue );
			return output;
		}
		protected override System.Web.Security.MembershipUser ReadMembershipUser( System.Data.Common.DbDataReader reader, Oits.Configuration.DbMap.DbResultElement resultConfig ) { 
			if ( null == resultConfig ) { 
				throw new System.ArgumentNullException( "resultConfig" );
			} else if ( null == reader ) { 
				throw new System.ArgumentNullException( "reader" );
			} else if ( reader.IsClosed || !reader.HasRows ) { 
				return null;
			}

			System.Web.Security.MembershipUser output = null;
			try { 
				System.Int32 index;

				index = reader.GetOrdinal( resultConfig.Columns[ "Username" ].ColumnName );
				System.String name = reader.GetString( index ).TrimToNull();

				index = reader.GetOrdinal( resultConfig.Columns[ "ProviderUserKey" ].ColumnName );
				System.Nullable<System.Int32> userId = null;
				if ( !reader.IsDBNull( index ) ) { 
					userId = reader.GetInt32( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "Email" ].ColumnName );
				System.String email = null;
				if ( !reader.IsDBNull( index ) ) { 
					email = reader.GetString( index ).TrimToNull();
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "PasswordQuestion" ].ColumnName );
				System.String passwordQuestion = null;
				if ( !reader.IsDBNull( index ) ) { 
					passwordQuestion = reader.GetString( index ).TrimToNull();
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "Comment" ].ColumnName );
				System.String comment = null;
				if ( !reader.IsDBNull( index ) ) { 
					comment = reader.GetString( index ).TrimToNull();
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "IsApproved" ].ColumnName );
				System.Boolean isApproved = false;
				if ( !reader.IsDBNull( index ) ) { 
					isApproved = reader.GetBoolean( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "IsLockedOut" ].ColumnName );
				System.Boolean isLockedOut = false;
				if ( !reader.IsDBNull( index ) ) { 
					isLockedOut = reader.GetBoolean( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "DateCreated" ].ColumnName );
				System.DateTime creationDate = MinSqlDateTime;
				if ( !reader.IsDBNull( index ) ) { 
					creationDate = reader.GetDateTime( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "LastLoginDate" ].ColumnName );
				System.DateTime lastLoginDate = MinSqlDateTime;
				if ( !reader.IsDBNull( index ) ) { 
					lastLoginDate = reader.GetDateTime( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "LastActivityDate" ].ColumnName );
				System.DateTime lastActivityDate = MinSqlDateTime;
				if ( !reader.IsDBNull( index ) ) { 
					lastActivityDate = reader.GetDateTime( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "LastPasswordChangedDate" ].ColumnName );
				System.DateTime lastPasswordChangedDate = MinSqlDateTime;
				if ( !reader.IsDBNull( index ) ) { 
					lastPasswordChangedDate = reader.GetDateTime( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "LastLockoutDate" ].ColumnName );
				System.DateTime lastLockoutDate = MinSqlDateTime;
				if ( !reader.IsDBNull( index ) ) { 
					lastLockoutDate = reader.GetDateTime( index );
				}

				index = reader.GetOrdinal( resultConfig.Columns[ "LastLogoutDate" ].ColumnName );
				System.Nullable<System.DateTime> lastLogoutDate = null;
				if ( ( 0 < index ) && !reader.IsDBNull( index ) ) { 
					lastLogoutDate = reader.GetDateTime( index );
				}

				output = new Oits.Web.Security.DbMembershipUser( 
					this.Name, 
					name, userId, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate, 
					lastLogoutDate 
				);
			} catch ( System.Exception e ) { 
				throw new System.Configuration.Provider.ProviderException( e.Message, e );
			}
			return output;
		}
		protected override System.String GetUserNameByEmail( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String email ) { 
			email = email.TrimToNull();
			if ( System.String.IsNullOrEmpty( email ) ) { 
				throw new System.ArgumentNullException( "email" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new GetUserNameByEmailParameters { 
				ApplicationName = this.ApplicationName, 
				Email = email 
			};
			var output = this.DbMapGroup.Commands[ "GetUserNameByEmail" ].ExecuteScalar<System.String>( connection, transaction, p, 
				x => ( System.DBNull.Value.Equals( x ) ? null : x.ToString().TrimToNull() ) 
			);
			if ( 0 != p.ReturnValue ) { 
				output = null;
			}
			return output;
		}
		protected override System.String ResetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer ) { 
			answer = answer.TrimToNull().CheckParameter( false, true, true, "answer" ).ToLower();
			username = username.CheckParameter( true, true, true, "username" );
			if ( !this.EnablePasswordReset ) { 
				throw new System.NotSupportedException();
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			System.String passwd = System.Web.Security.Membership.GeneratePassword( System.Math.Max( 1, this.MinRequiredPasswordLength ), System.Math.Max( 0, this.MinRequiredNonAlphanumericCharacters ) );
			var e = new System.Web.Security.ValidatePasswordEventArgs( username, passwd, false );
			this.OnValidatingPassword( e );
			if ( e.Cancel ) { 
				if ( null != e.FailureInformation ) { 
					throw e.FailureInformation;
				} else { 
					throw new System.Configuration.Provider.ProviderException();
				}
			}
			if ( 
				!this.ValidatePasswordAnswer( connection, transaction, username, answer, false ) 
				|| !this.SetPassword( connection, transaction, username, passwd ) 
			) { 
				passwd = null;
			}
			return passwd;
		}
		protected override System.Boolean SafeSetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password ) { 
			password = password.CheckPasswdParameter( this, "password" );
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			return this.SetPassword( connection, transaction, username, password );
		}
		protected override System.Boolean SetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password ) { 
			password = password.TrimToNull();
			if ( System.String.IsNullOrEmpty( password ) ) { 
				throw new System.ArgumentNullException( "password" );
			}
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var option = new UserPasswordOption { 
				Algorithm = System.Web.Security.Membership.HashAlgorithmType, Format = this.PasswordFormat, Salt = this.GenerateSalt() 
			};

			var p = new SetPasswordParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				Algorithm = option.Algorithm, 
				Salt = option.Salt, 
				Format = option.Format, 
				Password = this.GetEncodedString( password, option.Format, option.Algorithm, option.Salt ) 
			};
			this.DbMapGroup.Commands[ "SetPassword" ].ExecuteNonQuery( connection, transaction, p );
			if ( System.Web.Security.MembershipCreateStatus.ProviderError == (System.Web.Security.MembershipCreateStatus)p.ReturnValue ) { 
				throw new System.Configuration.Provider.ProviderException( new System.Web.Security.MembershipCreateUserException( System.Web.Security.MembershipCreateStatus.ProviderError ).Message );
			}
			return ( 0 == p.ReturnValue );
		}
		protected override System.Boolean ValidatePasswordAnswer( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer, System.Boolean failIfNotApproved ) { 
			answer = answer.TrimToNull().CheckParameter( false, true, true, "answer" ).ToLower();
			if ( System.String.IsNullOrEmpty( answer ) ) { 
				return false;
			}
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var option = this.GetUserPasswordQuestionAnswerFormatOption( connection, transaction, username );
			var p = new ValidatePasswordAnswerParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				PasswordAnswer = this.GetEncodedString( answer, option.Format, option.Algorithm, option.Salt ), 
				FailIfNotApproved = failIfNotApproved 
			};
			this.DbMapGroup.Commands[ "ValidatePasswordAnswer" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override System.Boolean UnlockUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new UnlockUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			this.DbMapGroup.Commands[ "UnlockUser" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		protected override void UpdateMembershipUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Security.MembershipUser user ) { 
			if ( null == user ) { 
				throw new System.ArgumentNullException( "user" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new UpdateMembershipUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = user.UserName, 
				IsApproved = user.IsApproved, 
				Email = user.Email, 
				RequireUniqueEmail = this.RequiresUniqueEmail, 
				Comment = user.Comment, 
				LastActivity = user.LastActivityDate, 
				LastLogin = user.LastLoginDate 
			};
			this.DbMapGroup.Commands[ "UpdateMembershipUser" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		protected override System.Boolean ValidateUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.Boolean updateLastLogin, System.Boolean updateLastActive, System.Boolean failIfNotApproved ) { 
			password = password.TrimToNull();
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ValidateUserParameters { 
				ApplicationName = this.ApplicationName, 
				Username = username, 
				UpdateLastLogin = updateLastLogin, 
				UpdateLastActive = updateLastActive, 
				FailIfNotApproved = failIfNotApproved, 
				MaxInvalidPasswordAttempts = this.MaxInvalidPasswordAttempts, 
				PasswordAttemptWindow = this.PasswordAttemptWindow 
			};
			if ( !System.String.IsNullOrEmpty( password ) ) { 
				p.Password = this.GetEncodedPassword( password, this.GetUserPasswordFormatOption( connection, transaction, username ) );
			}
			this.DbMapGroup.Commands[ "ValidateUser" ].ExecuteNonQuery( connection, transaction, p );
			return ( 0 == p.ReturnValue );
		}
		#endregion MembershipProvider extentions

		protected override void Logout( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );
			if ( System.String.IsNullOrEmpty( username ) ) { 
				throw new System.ArgumentNullException( "username" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new BasicUserParameters { 
				Username = username 
			};
			this.DbMapGroup.Commands[ "LogoutUser" ].ExecuteNonQuery( connection, transaction, p );
		}
		#endregion methods

	}

}