﻿namespace Oits.Web.Security { 

	public class DbMembershipUser : System.Web.Security.MembershipUser { 

		#region .ctor
		public DbMembershipUser( 
			System.String providerName, System.String name, System.Object providerUserKey, System.String email, System.String passwordQuestion, System.String comment, System.Boolean isApproved, System.Boolean isLockedOut, System.DateTime creationDate, System.DateTime lastLoginDate, System.DateTime lastActivityDate, System.DateTime lastPasswordChangedDate, System.DateTime lastLockoutDate 
		) : base( 
			providerName, name, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate 
		) { 
		}
		public DbMembershipUser( 
			System.String providerName, System.String name, System.Object providerUserKey, System.String email, System.String passwordQuestion, System.String comment, System.Boolean isApproved, System.Boolean isLockedOut, System.DateTime creationDate, System.DateTime lastLoginDate, System.DateTime lastActivityDate, System.DateTime lastPasswordChangedDate, System.DateTime lastLockoutDate, System.Nullable<System.DateTime> lastLogoutDate 
		) : this( 
			providerName, name, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate 
		) { 
			this.LastLogoutDate = lastLogoutDate;
		}
		#endregion .ctor


		#region properties
		public System.Nullable<System.DateTime> LastLogoutDate { 
			get;
			set;
		}
		public override System.Boolean IsOnline { 
			get { 
				var output = base.IsOnline;
				if ( output ) { 
					var lastLogout = this.LastLogoutDate;
					if ( lastLogout.HasValue ) { 
						var v = lastLogout.Value;
						output = !( ( this.LastLoginDate <= v ) & ( this.LastActivityDate <= v ) );
					}
				}
				return output;
			}
		}
		#endregion properties

	}

}