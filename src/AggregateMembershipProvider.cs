﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	[System.Obsolete( "This class has been deprecated and should not be used.", false )]
	public class AggregateMembershipProvider : WebMatrix.WebData.ExtendedMembershipProvider { 

		#region fields
		private System.String myApplicationName = null;
		private System.Collections.Generic.List<System.String> myProviderNames = new System.Collections.Generic.List<System.String>();
		private AggregateBehaviour myBehaviour = AggregateBehaviour.Any;
		#endregion fields


		#region .ctor
		public AggregateMembershipProvider() : base() { 
		}
		#endregion .ctor


		#region properties
		public override System.String ApplicationName { 
			get { 
				return myApplicationName;
			}
			set { 
				myApplicationName = value.TrimToNull();
			}
		}
		public System.Collections.Generic.ICollection<System.String> ProviderNames { 
			get { 
				if ( ( null == myProviderNames ) || ( 0 == myProviderNames.Count ) ) { 
					throw new System.Configuration.Provider.ProviderException( "No providers have been defined." );
				}
				return myProviderNames.AsReadOnly();
			}
		}
		protected System.Collections.Generic.IEnumerable<System.Web.Security.MembershipProvider> Providers { 
			get { 
				return System.Web.Security.Membership.Providers.OfType<System.Web.Security.MembershipProvider>().Where( 
					x => this.ProviderNames.Contains( x.Name ) 
				);
			}
		}
		protected System.Web.Security.MembershipProvider FirstProvider { 
			get { 
				return this.Providers.First();
			}
		}
		protected System.Collections.Generic.IEnumerable<WebMatrix.WebData.ExtendedMembershipProvider> ExtendedProviders { 
			get { 
				return this.Providers.OfType<WebMatrix.WebData.ExtendedMembershipProvider>();
			}
		}
		public AggregateBehaviour Behaviour { 
			get { 
				if ( !System.Enum.IsDefined( myBehaviour.GetType(), myBehaviour ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
				}
				return myBehaviour;
			}
		}

		public override System.Boolean EnablePasswordReset { 
			get { 
				return this.FirstProvider.EnablePasswordReset;
			}
		}
		public override System.Boolean EnablePasswordRetrieval { 
			get { 
				return this.FirstProvider.EnablePasswordRetrieval;
			}
		}
		public override System.Int32 MaxInvalidPasswordAttempts { 
			get { 
				return this.FirstProvider.MaxInvalidPasswordAttempts;
			}
		}
		public override System.Int32 MinRequiredNonAlphanumericCharacters { 
			get { 
				return this.FirstProvider.MinRequiredNonAlphanumericCharacters;
			}
		}
		public override System.Int32 MinRequiredPasswordLength { 
			get { 
				return this.FirstProvider.MinRequiredPasswordLength;
			}
		}
		public override System.Int32 PasswordAttemptWindow { 
			get { 
				return this.FirstProvider.PasswordAttemptWindow;
			}
		}
		public override System.Web.Security.MembershipPasswordFormat PasswordFormat { 
			get { 
				return this.FirstProvider.PasswordFormat;
			}
		}
		public override System.String PasswordStrengthRegularExpression { 
			get { 
				return this.FirstProvider.PasswordStrengthRegularExpression;
			}
		}
		public override System.Boolean RequiresQuestionAndAnswer { 
			get { 
				return this.FirstProvider.RequiresQuestionAndAnswer;
			}
		}
		public override System.Boolean RequiresUniqueEmail { 
			get { 
				return this.FirstProvider.RequiresUniqueEmail;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) { 
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "applicationName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) {
				configValue = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			}
			myApplicationName = configValue;
			config.Remove( "applicationName" );

			configValue = config[ "providerNames" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				myProviderNames = new System.Collections.Generic.List<System.String>( configValue.Split( new System.Char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries ).Select( 
					x => x.TrimToNull() 
				).Where( 
					x => !System.String.IsNullOrEmpty( x ) 
				) );
			}
			config.Remove( "providerNames" );
			myProviderNames.Remove( this.Name );
			if ( 0 == myProviderNames.Count ) { 
				throw new System.Configuration.Provider.ProviderException( "At least one provider must be specified." );
			}

			configValue = config[ "behaviour" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				if ( !System.Enum.IsDefined( typeof( AggregateBehaviour ), configValue ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
				}
				myBehaviour = (AggregateBehaviour)System.Enum.Parse( typeof( AggregateBehaviour ), configValue );
			}
		}
		#endregion ProviderBase

		#region MembershipProvider
		public override System.Boolean ChangePassword( System.String username, System.String oldPassword, System.String newPassword ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.ChangePassword( username, oldPassword, newPassword ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.ChangePassword( username, oldPassword, newPassword ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override System.Boolean ChangePasswordQuestionAndAnswer( System.String username, System.String password, System.String newPasswordQuestion, System.String newPasswordAnswer ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.ChangePasswordQuestionAndAnswer( username, password, newPasswordQuestion, newPasswordAnswer ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.ChangePasswordQuestionAndAnswer( username, password, newPasswordQuestion, newPasswordAnswer ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override System.Web.Security.MembershipUser CreateUser( System.String username, System.String password, System.String email, System.String passwordQuestion, System.String passwordAnswer, System.Boolean isApproved, System.Object providerUserKey, out System.Web.Security.MembershipCreateStatus status ) { 
			status = System.Web.Security.MembershipCreateStatus.ProviderError;
			System.Web.Security.MembershipUser u = null;
			foreach ( var p in this.Providers ) { 
				try { 
					u = p.CreateUser( username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status );
					if ( System.Web.Security.MembershipCreateStatus.Success == status ) { 
						if ( null == u ) { 
							u = this.GetUser( username, false );
						}
						break;
					}
				} catch ( System.Exception ) { 
					;
				}
			}
			return u;
		}
		protected override System.Byte[] DecryptPassword( System.Byte[] encodedPassword ) { 
			return base.DecryptPassword( encodedPassword );
		}
		public override System.Boolean DeleteUser( System.String username, System.Boolean deleteAllRelatedData ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.DeleteUser( username, deleteAllRelatedData ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.DeleteUser( username, deleteAllRelatedData ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		protected override System.Byte[] EncryptPassword( System.Byte[] password ) { 
			return base.EncryptPassword( password );
		}
		protected override System.Byte[] EncryptPassword( System.Byte[] password, System.Web.Configuration.MembershipPasswordCompatibilityMode legacyPasswordCompatibilityMode ) { 
			return base.EncryptPassword( password, legacyPasswordCompatibilityMode );
		}
		public override System.Web.Security.MembershipUserCollection FindUsersByEmail( System.String emailToMatch, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) { 
			if ( pageSize < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "pageSize", "pageSize may not be negative." );
			} else if ( pageIndex < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "pageIndex", "pageIndex may not be negative." );
			}
			System.Int64 window = ( pageIndex * pageSize ) + pageSize;
			if ( System.Int32.MaxValue < window ) { 
				throw new System.ArgumentOutOfRangeException( "pageSize and pageIndex" );
			}
			totalRecords = 0;
			System.Int32 subtotalRecords = 0;
			var output = new System.Web.Security.MembershipUserCollection();
			System.Web.Security.MembershipUserCollection hold = null;
			foreach ( var p in this.Providers ) { 
				if ( output.Count < pageSize ) { 
					hold = p.FindUsersByEmail( emailToMatch, pageIndex, pageSize, out subtotalRecords );
					foreach ( var u in hold.OfType<System.Web.Security.MembershipUser>().Where( 
						x => ( output.Count < pageSize ) && ( output.Count < System.Int32.MaxValue ) 
					) ) { 
						output.Add( u );
					}
				} else { 
					hold = p.FindUsersByEmail( emailToMatch, 0, 1, out subtotalRecords );
				}
				totalRecords += subtotalRecords;
			}
			return output;
		}
		public override System.Web.Security.MembershipUserCollection FindUsersByName( System.String usernameToMatch, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) {
			if ( pageSize < 0 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "pageSize may not be negative." );
			} else if ( pageIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "pageIndex", "pageIndex may not be negative." );
			}
			System.Int64 window = ( pageIndex * pageSize ) + pageSize;
			if ( System.Int32.MaxValue < window ) {
				throw new System.ArgumentOutOfRangeException( "pageSize and pageIndex" );
			}
			totalRecords = 0;
			System.Int32 subtotalRecords = 0;
			var output = new System.Web.Security.MembershipUserCollection();
			System.Web.Security.MembershipUserCollection hold = null;
			foreach ( var p in this.Providers ) {
				if ( output.Count < pageSize ) {
					hold = p.FindUsersByName( usernameToMatch, pageIndex, pageSize, out subtotalRecords );
					foreach ( var u in hold.OfType<System.Web.Security.MembershipUser>().Where(
						x => ( output.Count < pageSize ) && ( output.Count < System.Int32.MaxValue )
					) ) {
						output.Add( u );
					}
				} else {
					hold = p.FindUsersByName( usernameToMatch, 0, 1, out subtotalRecords );
				}
				totalRecords += subtotalRecords;
			}
			return output;
		}
		public override System.Web.Security.MembershipUserCollection GetAllUsers( System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) { 
			if ( pageSize < 1 ) { 
				throw new System.ArgumentOutOfRangeException( "pageSize", "pageSize must be positive." );
			} else if ( pageIndex < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "pageIndex", "pageIndex may not be negative." );
			}
			System.Int64 window = ( pageIndex * pageSize ) + pageSize;
			if ( System.Int32.MaxValue < window ) { 
				throw new System.ArgumentOutOfRangeException( "pageSize and pageIndex" );
			}
			totalRecords = 0;
			System.Int32 subtotalRecords = 0;
			var output = new System.Web.Security.MembershipUserCollection();
			System.Web.Security.MembershipUserCollection hold = null;
			foreach ( var p in this.Providers ) { 
				if ( output.Count < pageSize ) { 
					hold = p.GetAllUsers( pageIndex, pageSize, out subtotalRecords );
					foreach ( var u in hold.OfType<System.Web.Security.MembershipUser>().Where( 
						x => ( output.Count < pageSize ) && ( output.Count < System.Int32.MaxValue ) 
					) ) { 
						output.Add( u );
					}
				} else { 
					hold = p.GetAllUsers( 0, 1, out subtotalRecords );
				}
				totalRecords += subtotalRecords;
			}
			return output;
		}
		public override System.Int32 GetNumberOfUsersOnline() { 
			System.Int32 output = 0;
			foreach ( var p in this.Providers ) { 
				try { 
					output += p.GetNumberOfUsersOnline();
				} catch ( System.Exception ) { 
					;
				}
			}
			return output;
		}
		public override System.String GetPassword( System.String username, System.String answer ) { 
			return this.Providers.Select( 
				x => { 
						System.String passwd = null;
						try { 
							passwd = x.GetPassword( username, answer );
						} catch ( System.Exception ) { 
							;
						}
						return passwd;
					}
			).Where( 
				x => !System.String.IsNullOrEmpty( x ) 
			).FirstOrDefault();
		}
		public override System.Web.Security.MembershipUser GetUser( System.Object providerUserKey, System.Boolean userIsOnline ) {
			return this.Providers.Select( 
				x => { 
					System.Web.Security.MembershipUser user = null;
					try { 
						user = x.GetUser( providerUserKey, userIsOnline );
					} catch ( System.Exception ) { 
						;
					}
					return user;
				}
			).Where(
				x => ( null != x ) 
			).FirstOrDefault();
		}
		public override System.Web.Security.MembershipUser GetUser( System.String username, System.Boolean userIsOnline ) {
			return this.Providers.Select( 
				x => { 
					System.Web.Security.MembershipUser user = null;
					try { 
						user = x.GetUser( username, userIsOnline );
					} catch ( System.Exception ) { 
						;
					}
					return user;
				}
			).Where( 
				x => ( null != x ) 
			).FirstOrDefault();
		}
		public override System.String GetUserNameByEmail( System.String email ) { 
			return this.Providers.Select( 
				x => { 
					System.String username = null;
					try { 
						username = x.GetUserNameByEmail( email );
					} catch ( System.Exception ) { 
						;
					}
					return username;
				} 
			).Where( 
				x => !System.String.IsNullOrEmpty( x ) 
			).FirstOrDefault();
		}
		public override System.String ResetPassword( System.String username, System.String answer ) { 
			return this.Providers.Select( 
				x => { 
					System.String passwd = null;
					try { 
						passwd = x.ResetPassword( username, answer );
					} catch ( System.Exception ) { 
						;
					}
					return passwd;
				}
			).Where( 
				x => !System.String.IsNullOrEmpty( x ) 
			).FirstOrDefault();
		}
		public override System.Boolean UnlockUser( System.String userName ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.UnlockUser( userName ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.UnlockUser( userName ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override void UpdateUser( System.Web.Security.MembershipUser user ) { 
			if ( null == user ) { 
				throw new System.ArgumentNullException( "user" );
			}
			var providerName = user.ProviderName;
			if ( System.String.IsNullOrEmpty( providerName ) ) { 
				throw new System.ArgumentException( "The Provider Name for the specified user is invalid.", "user" );
			} else if ( this.Name.Equals( providerName ) ) { 
				var e = new System.InvalidOperationException( "This provider is not intended to support users directly." );
				throw new System.Configuration.Provider.ProviderException( e.Message, e );
			}
			var p = this.Providers.FirstOrDefault( 
				x => x.Name.Equals( providerName ) 
			);
			if ( null == p ) { 
				var e = new System.InvalidOperationException( "The Provider Name for the specified user was not found." );
				throw new System.Configuration.Provider.ProviderException( e.Message, e );
			}

			p.UpdateUser( user );
		}
		public override System.Boolean ValidateUser( System.String username, System.String password ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.ValidateUser( username, password ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.ValidateUser( username, password ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		#endregion MembershipProvider

		#region ExtendedMembershipProvider
		public override System.Boolean ConfirmAccount( System.String accountConfirmationToken ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.ExtendedProviders.Any( 
						x => x.ConfirmAccount( accountConfirmationToken ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.ExtendedProviders.All( 
						x => x.ConfirmAccount( accountConfirmationToken ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override System.Boolean ConfirmAccount( System.String userName, System.String accountConfirmationToken ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.ExtendedProviders.Any( 
						x => x.ConfirmAccount( userName, accountConfirmationToken ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.ExtendedProviders.All( 
						x => x.ConfirmAccount( userName, accountConfirmationToken ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override System.String CreateAccount( System.String userName, System.String password ) {
			return this.ExtendedProviders.Select( 
				x => x.CreateAccount( userName, password ) 
			).FirstOrDefault();
		}
		public override System.String CreateAccount( System.String userName, System.String password, System.Boolean requireConfirmationToken ) {
			return this.ExtendedProviders.Select(
				x => x.CreateAccount( userName, password, requireConfirmationToken )
			).FirstOrDefault();
		}
		public override void CreateOrUpdateOAuthAccount( System.String provider, System.String providerUserId, System.String userName ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.ExtendedProviders.FirstOrDefault();
					if ( null != p ) { 
						p.CreateOrUpdateOAuthAccount( provider, providerUserId, userName );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.ExtendedProviders ) { 
						q.CreateOrUpdateOAuthAccount( provider, providerUserId, userName );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password ) { 
			return this.ExtendedProviders.Select( 
				x => x.CreateUserAndAccount( userName, password ) 
			).FirstOrDefault();
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Boolean requireConfirmation ) {
			return this.ExtendedProviders.Select( 
				x => x.CreateUserAndAccount( userName, password, requireConfirmation ) 
			).FirstOrDefault();
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Boolean requireConfirmation, System.Collections.Generic.IDictionary<string, object> values ) {
			return this.ExtendedProviders.Select( 
				x => x.CreateUserAndAccount( userName, password, requireConfirmation, values ) 
			).FirstOrDefault();
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Collections.Generic.IDictionary<string, object> values ) {
			return this.ExtendedProviders.Select( 
				x => x.CreateUserAndAccount( userName, password, values ) 
			).FirstOrDefault();
		}
		public override System.Boolean DeleteAccount( System.String userName ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.ExtendedProviders.Any( 
						x => x.DeleteAccount( userName ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.ExtendedProviders.All( 
						x => x.DeleteAccount( userName ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override void DeleteOAuthAccount( System.String provider, System.String providerUserId ) {
			switch ( this.Behaviour ) {
				case AggregateBehaviour.Any:
					var p = this.ExtendedProviders.FirstOrDefault();
					if ( null != p ) {
						p.DeleteOAuthAccount( provider, providerUserId );
					}
					break;
				case AggregateBehaviour.All:
					foreach ( var q in this.ExtendedProviders ) {
						q.DeleteOAuthAccount( provider, providerUserId );
					}
					break;
				default:
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override void DeleteOAuthToken( System.String token ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.ExtendedProviders.FirstOrDefault();
					if ( null != p ) { 
						p.DeleteOAuthToken( token );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.ExtendedProviders ) { 
						q.DeleteOAuthToken( token );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override System.String GeneratePasswordResetToken( System.String userName ) {
			return this.ExtendedProviders.Select( 
				x => x.GeneratePasswordResetToken( userName ) 
			).FirstOrDefault();
		}
		public override System.String GeneratePasswordResetToken( System.String userName, System.Int32 tokenExpirationInMinutesFromNow ) {
			return this.ExtendedProviders.Select( 
				x => x.GeneratePasswordResetToken( userName, tokenExpirationInMinutesFromNow ) 
			).FirstOrDefault();
		}
		public override System.Collections.Generic.ICollection<WebMatrix.WebData.OAuthAccountData> GetAccountsForUser( System.String userName ) { 
			var output = this.ExtendedProviders.SelectMany( 
				x => x.GetAccountsForUser( userName ) 
			);
			System.Collections.Generic.List<WebMatrix.WebData.OAuthAccountData> list = null;
			if ( ( null == output ) || ( !output.Any( x => true ) ) ) { 
				list = new System.Collections.Generic.List<WebMatrix.WebData.OAuthAccountData>();
			} else { 
				list = new System.Collections.Generic.List<WebMatrix.WebData.OAuthAccountData>( output );
			}
			return list.AsReadOnly();
		}
		public override System.DateTime GetCreateDate( System.String userName ) {
			var u = this.ExtendedProviders.Select( 
				x => x.GetUser( userName, false ) 
			).Where( 
				x => ( null != x ) 
			).FirstOrDefault();
			if ( null == u ) { 
				return System.DateTime.MinValue;
			} else { 
				return u.CreationDate;
			}
		}
		public override System.DateTime GetLastPasswordFailureDate( System.String userName ) {
			var p = this.ExtendedProviders.Where( 
				x => ( null != x.GetUser( userName, false ) ) 
			).FirstOrDefault();
			if ( null == p ) { 
				return System.DateTime.MinValue;
			} else { 
				return p.GetLastPasswordFailureDate( userName );
			}
		}
		public override System.String GetOAuthTokenSecret( System.String token ) { 
			return this.ExtendedProviders.Select( 
				x => x.GetOAuthTokenSecret( token )
			).FirstOrDefault( 
				x => !System.String.IsNullOrEmpty( x ) 
			);
		}
		public override System.DateTime GetPasswordChangedDate( System.String userName ) { 
			return this.ExtendedProviders.Max( 
				x => x.GetPasswordChangedDate( userName ) 
			);
		}
		public override System.Int32 GetPasswordFailuresSinceLastSuccess( System.String userName ) {
			return this.ExtendedProviders.Max(
				x => x.GetPasswordFailuresSinceLastSuccess( userName )
			);
		}
		public override System.Int32 GetUserIdFromOAuth( System.String provider, System.String providerUserId ) { 
			return this.ExtendedProviders.Max(
				x => x.GetUserIdFromOAuth( provider, providerUserId ) 
			);
		}
		public override System.Int32 GetUserIdFromPasswordResetToken( System.String token ) { 
			return this.ExtendedProviders.Max( 
				x => x.GetUserIdFromPasswordResetToken( token ) 
			);
		}
		public override System.String GetUserNameFromId( System.Int32 userId ) { 
			return this.ExtendedProviders.Select( 
				x => x.GetUserNameFromId( userId ) 
			).FirstOrDefault( 
				x => !System.String.IsNullOrEmpty( x ) 
			);
		}
		public override System.Boolean HasLocalAccount( System.Int32 userId ) {
			return this.ExtendedProviders.Any( 
				x => x.HasLocalAccount( userId ) 
			);
		}
		public override System.Boolean IsConfirmed( System.String userName ) { 
			return this.ExtendedProviders.Any( 
				x => x.IsConfirmed( userName ) 
			);
		}
		public override void ReplaceOAuthRequestTokenWithAccessToken( System.String requestToken, System.String accessToken, System.String accessTokenSecret ) {
			switch ( this.Behaviour ) {
				case AggregateBehaviour.Any : 
					var p = this.ExtendedProviders.FirstOrDefault();
					if ( null != p ) { 
						p.ReplaceOAuthRequestTokenWithAccessToken( requestToken, accessToken, accessTokenSecret );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.ExtendedProviders ) { 
						q.ReplaceOAuthRequestTokenWithAccessToken( requestToken, accessToken, accessTokenSecret );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override System.Boolean ResetPasswordWithToken( System.String token, System.String newPassword ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.ExtendedProviders.Any( 
						x => x.ResetPasswordWithToken( token, newPassword ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.ExtendedProviders.All( 
						x => x.ResetPasswordWithToken( token, newPassword ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override void StoreOAuthRequestToken( System.String requestToken, System.String requestTokenSecret ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.ExtendedProviders.FirstOrDefault();
					if ( null != p ) { 
						p.StoreOAuthRequestToken( requestToken, requestTokenSecret );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.ExtendedProviders ) { 
						q.StoreOAuthRequestToken( requestToken, requestTokenSecret );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		#endregion ExtendedMembershipProvider
		#endregion methods

	}

}