﻿using System.Linq;
using System.Configuration;

namespace Oits.Web.Security { 

	public class RightManagerSection : ConfigurationSection { 

		#region fields
		public const System.String DefaultSectionName = "oits.rightsManager";
		private static readonly ConfigurationProperty theDefaultProvider;
		private static readonly ConfigurationProperty theProviders;
		private static readonly ConfigurationProperty theEnabled;
		private static readonly ConfigurationPropertyCollection theProperties;
		#endregion fields


		#region .ctor
		static RightManagerSection() { 
			theDefaultProvider = new ConfigurationProperty( "defaultProvider", typeof( System.String ), "DbRightsProvider", null, new StringValidator( 1 ), ConfigurationPropertyOptions.IsRequired );
			theProviders = new ConfigurationProperty( "providers", typeof( ProviderSettingsCollection ), null, ConfigurationPropertyOptions.None );
			theEnabled = new ConfigurationProperty( "enabled", typeof( System.Boolean ), false, ConfigurationPropertyOptions.None );

			theProperties = new ConfigurationPropertyCollection();
			theProperties.Add( theDefaultProvider );
			theProperties.Add( theProviders );
			theProperties.Add( theEnabled );
		}

		public RightManagerSection() : base() { 
		}
		#endregion .ctor


		#region properties
		[StringValidator(MinLength=1)]
		[ConfigurationProperty( "defaultProvider", DefaultValue = "DbRightsProvider", IsRequired = true )]
		[System.ComponentModel.TypeConverter( typeof( WhiteSpaceTrimStringConverter ) )]
		public System.String DefaultProvider { 
			get { 
				return (System.String)base[ theDefaultProvider ];
			}
			set { 
				base[ theDefaultProvider ] = value.TrimToNull();
			}
		}

		[ConfigurationProperty( "providers" )]
		public ProviderSettingsCollection Providers { 
			get { 
				return (ProviderSettingsCollection)base[ theProviders ];
			}
		}

		[ConfigurationProperty( "enabled", DefaultValue=false )]
		public System.Boolean Enabled { 
			get { 
				return (System.Boolean)base[ theEnabled ];
			}
			set { 
				base[ theEnabled ] = value;
			}
		}
		#endregion properties

	}

}