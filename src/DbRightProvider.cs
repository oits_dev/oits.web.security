﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public class DbRightProvider : DbRightProviderBase { 

		#region internal classes
		protected class ParametersBase : Oits.Configuration.DbMap.ReturnValueBase { 
			public ParametersBase() : base() {
			}

			public System.String ApplicationName {
				get;
				set;
			}
			public System.String RequestingUser {
				get {
					if ( System.Web.HttpContext.Current.User.Identity.IsAuthenticated ) {
						return System.Web.HttpContext.Current.User.Identity.Name;
					} else {
						return System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				set {
					;
				}
			}
			public System.String RoleName {
				get;
				set;
			}
			public System.String Username {
				get;
				set;
			}
			public System.String RightName {
				get;
				set;
			}
			public System.String Resource {
				get;
				set;
			}
			public System.Boolean RequireUniqueResource {
				get;
				set;
			}
			public System.Boolean ThrowIfPopulated {
				get;
				set;
			}
			public System.Boolean DeleteRelatedData { 
				get;
				set;
			}
		}
		#endregion intenal classes


		#region .ctor
		public DbRightProvider() : base() { 
		}
		#endregion .ctor


		#region methods
		public sealed override void Initialize( string name, System.Collections.Specialized.NameValueCollection config ) { 
			if ( null == config ) { 
				throw new System.ArgumentNullException( "config" );
			}
			if ( System.String.IsNullOrEmpty( name ) ) { 
				name = "DbRightProvider";
			}
			if ( System.String.IsNullOrEmpty( config[ "description" ].TrimToNull() ) ) { 
				config.Remove( "description" );
				config.Add( "description", "Oits.Web.Security.DbRightProvider implementation." );
			}

			base.Initialize( name, config );
		}

		protected override void ClearRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || !rightNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "ClearRoleRight" ];
			foreach ( var roleName in roleNames ) { 
				p.RoleName = roleName.TrimToNull().CheckParameter( true, true, true, "roleName" );
				foreach ( var rightName in rightNames ) { 
					p.RightName = rightName.TrimToNull().CheckParameter( true, true, true, "rightName" );
					if ( !this.RightExists( connection, transaction, p.RightName ) ) { 
						throw new System.Configuration.Provider.ProviderException( "The specified rightName is invalid or does not exist for the current application." );
					}
					p.ReturnValue = 0;
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override void ClearUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) {
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) {
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || ( usernames.Count() < 1 ) ) {
				throw new System.ArgumentNullException( "usernames" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "ClearUserRight" ];
			foreach ( var username in usernames ) { 
				p.Username = username.TrimToNull().CheckParameter( true, true, true, "username" );
				foreach ( var rightName in rightNames ) { 
					p.ReturnValue = 0;
					p.RightName = rightName.TrimToNull().CheckParameter( true, true, true, "rightName" );
					if ( !this.RightExists( connection, transaction, p.RightName ) ) { 
						throw new System.Configuration.Provider.ProviderException( "The specified rightName is invalid or does not exist for the current application." );
					}
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override void CreateRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.String resource ) {
			resource = resource.TrimToNull();
			if ( !System.String.IsNullOrEmpty( resource ) && ( this.MaxResourceLength < resource.Length ) ) { 
				var ae = new System.ArgumentException( "The specified resource exceeds the maximum length in characters.", "resource" );
				throw new System.Configuration.Provider.ProviderException( ae.Message, ae );
			}
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName, 
				Resource = resource, 
				RequireUniqueResource = this.RequireUniqueResource 
			};
			this.DbMapGroup.Commands[ "CreateRight" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		protected override System.Boolean DeleteRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.Boolean throwIfPopulated, System.Boolean deleteRelatedData ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName, 
				ThrowIfPopulated = throwIfPopulated, 
				DeleteRelatedData = deleteRelatedData 
			};
			this.DbMapGroup.Commands[ "DeleteRight" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
			return ( 0 == p.ReturnValue );
		}
		protected override void DenyRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || !rightNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "DenyRoleRight" ];
			foreach ( var roleName in roleNames ) { 
				p.RoleName = roleName.TrimToNull().CheckParameter( true, true, true, "roleName" );
				foreach ( var rightName in rightNames ) { 
					p.ReturnValue = 0;
					p.RightName = rightName.TrimToNull().CheckParameter( true, true, true, "rightName" );
					if ( !this.RightExists( connection, transaction, p.RightName ) ) { 
						throw new System.Configuration.Provider.ProviderException( "The specified rightName is invalid or does not exist for the current application." );
					}
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override void DenyUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || !rightNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || !usernames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "DenyUserRight" ];
			foreach ( var username in usernames ) { 
				p.Username = username.TrimToNull().CheckParameter( true, true, true, "username" );
				foreach ( var rightName in rightNames ) { 
					p.ReturnValue = 0;
					p.RightName = rightName.TrimToNull().CheckParameter( true, true, true, "rightName" );
					if ( !this.RightExists( connection, transaction, p.RightName ) ) { 
						throw new System.Configuration.Provider.ProviderException( "The specified rightName is invalid or does not exist for the current application." );
					}
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override System.Collections.Generic.IList<Right> FindRightsByResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String resource ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				Resource = resource.TrimToNull() 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "FindRightsByResource" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> FindRightsByRightName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightNameToMatch ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightNameToMatch.TrimToNull() 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "FindRightsByRightName" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> GetAllRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetRight" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> GetDeniedRightsForRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RoleName = roleName, 
				RequestingUser = System.Web.HttpContext.Current.GetUsername() 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetDeniedRoleRight" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> GetGrantedRightsForRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName ) {
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RoleName = roleName, 
				RequestingUser = System.Web.HttpContext.Current.GetUsername() 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetGrantedRoleRight" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> GetDeniedRightsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				Username = username 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetDeniedUserRight" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override System.Collections.Generic.IList<Right> GetGrantedRightsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) { 
			username = username.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName,
				Username = username 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetGrantedUserRight" ].ExecuteReaderSingleResult<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			if ( output is System.Collections.Generic.IList<Right> ) { 
				return (System.Collections.Generic.IList<Right>)output;
			} else { 
				return new System.Collections.Generic.List<Right>( output );
			}
		}
		protected override Right GetRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetRight" ].ExecuteReaderSingleRow<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			return output;
		}
		protected override Right GetRightByResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String resource ) { 
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				Resource = resource.TrimToNull() 
			};
			var group = this.DbMapGroup;
			var output = group.Commands[ "GetRightByResource" ].ExecuteReaderSingleRow<Right>( connection, transaction, p, group.Results[ "GetRight" ], this.ReadRightFromDb );
			this.CheckReturnValue( p.ReturnValue );
			return output;
		}
		protected override void GrantRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || !rightNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "GrantRoleRight" ];
			foreach ( var roleName in roleNames ) { 
				p.RoleName = roleName.CheckParameter( true, true, true, "roleName" );
				foreach ( var rightName in rightNames ) { 
					p.ReturnValue = 0;
					p.RightName = rightName.CheckParameter( true, true, true, "rightName" );
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override void GrantUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || !rightNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || !usernames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			} else if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName 
			};
			var command = this.DbMapGroup.Commands[ "GrantUserRight" ];
			foreach ( var username in usernames ) { 
				p.Username = username.CheckParameter( true, true, true, "username" );
				foreach ( var rightName in rightNames ) { 
					p.ReturnValue = 0;
					p.RightName = rightName.CheckParameter( true, true, true, "rightName" );
					command.ExecuteNonQuery( connection, transaction, p );
					this.CheckReturnValue( p.ReturnValue );
				}
			}
		}
		protected override System.Boolean IsRoleGrantedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase {
				ApplicationName = this.ApplicationName,
				RightName = rightName,
				RoleName = roleName 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetIsRoleGrantedRight" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override System.Boolean IsRoleDeniedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase {
				ApplicationName = this.ApplicationName,
				RightName = rightName,
				RoleName = roleName 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetIsRoleDeniedRight" ].ExecuteScalar<System.Boolean>( connection, transaction, p,
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x )
			);
		}
		protected override System.Boolean IsUserGrantedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			username = username.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName, 
				Username = username 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetIsUserGrantedRight" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override System.Boolean IsUserDeniedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			username = username.CheckParameter( true, true, true, "roleName" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName, 
				Username = username 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetIsUserDeniedRight" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override System.Boolean RightExists( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName 
			};
			var group = this.DbMapGroup;
			return group.Commands[ "GetRightExists" ].ExecuteScalar<System.Boolean>( connection, transaction, p, 
				x => System.DBNull.Value.Equals( x ) ? false : System.Convert.ToBoolean( x ) 
			);
		}
		protected override void SetRightResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.String resource ) { 
			resource = this.CheckResourceParameter( resource, "resource" );
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			if ( null == connection ) { 
				throw new System.ArgumentNullException( "connection" );
			}

			var p = new ParametersBase { 
				ApplicationName = this.ApplicationName, 
				RightName = rightName, 
				Resource = resource, 
				RequireUniqueResource = this.RequireUniqueResource 
			};
			var group = this.DbMapGroup;
			group.Commands[ "SetRightResource" ].ExecuteNonQuery( connection, transaction, p );
			this.CheckReturnValue( p.ReturnValue );
		}
		#endregion methods

	}

}