﻿using System.Linq;

namespace Oits.Web.Security { 

	public abstract class RightProviderBase : System.Configuration.Provider.ProviderBase { 

		#region fields
		private System.Boolean myRequireUniqueResource = false;
		#endregion fields


		#region .ctor
		protected RightProviderBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public abstract System.String ApplicationName { 
			get;
			set;
		}
		public virtual System.Boolean RequireUniqueResource { 
			get { 
				return myRequireUniqueResource;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( string name, System.Collections.Specialized.NameValueCollection config ) {
			base.Initialize( name, config );

			System.String configValue;
			System.Boolean configBool;

			configValue = config[ "requireUniqueResource" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				if ( System.Boolean.TryParse( configValue, out configBool ) ) { 
					myRequireUniqueResource = configBool;
				}
			}
			config.Remove( "requireUniqueResource" );
		}
		#endregion ProviderBase

		public abstract void ClearUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public abstract void ClearRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public virtual void CreateRight( System.String rightName ) { 
			this.CreateRight( rightName.CheckParameter( true, true, true, "rightName" ), null );
		}
		public abstract void CreateRight( System.String rightName, System.String resource );
		public virtual System.Boolean DeleteRight( System.String rightName ) { 
			return this.DeleteRight( rightName.CheckParameter( true, true, true, "rightName" ), true );
		}
		public virtual System.Boolean DeleteRight( System.String rightName, System.Boolean throwIfPopulated ) { 
			return this.DeleteRight( rightName.CheckParameter( true, true, true, "rightName" ), throwIfPopulated, true );
		}
		public abstract System.Boolean DeleteRight( System.String rightName, System.Boolean throwIfPopulated, System.Boolean deleteRelatedData );
		public abstract void DenyUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public abstract void DenyRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public abstract System.Collections.Generic.IList<Right> FindRightsByResource( System.String resourceToMatch );
		public abstract System.Collections.Generic.IList<Right> FindRightsByRightName( System.String rightNameToMatch );
		public abstract System.Collections.Generic.IList<Right> GetAllRights();
		public abstract System.Collections.Generic.IList<Right> GetGrantedRightsForUser( System.String username );
		public abstract System.Collections.Generic.IList<Right> GetDeniedRightsForUser( System.String username );
		public abstract System.Collections.Generic.IList<Right> GetGrantedRightsForRole( System.String roleName );
		public abstract System.Collections.Generic.IList<Right> GetDeniedRightsForRole( System.String roleName );
		public abstract Right GetRight( System.String rightName );
		public abstract Right GetRightByResource( System.String resource );
		public abstract void GrantUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public abstract void GrantRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		public abstract System.Boolean IsUserGrantedRight( System.String username, System.String rightName );
		public abstract System.Boolean IsUserDeniedRight( System.String username, System.String rightName );
		public abstract System.Boolean IsRoleGrantedRight( System.String roleName, System.String rightName );
		public abstract System.Boolean IsRoleDeniedRight( System.String roleName, System.String rightName );
		public abstract System.Boolean RightExists( System.String rightName );
		public abstract void SetRightResource( System.String rightName, System.String resource );
		#endregion methods

	}

}