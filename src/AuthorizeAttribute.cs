﻿using System.Linq;

namespace Oits.Web.Security { 

	[System.AttributeUsage( System.AttributeTargets.Method | System.AttributeTargets.Class, Inherited = true, AllowMultiple = true )]
	public class AuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute { 

		#region fields
		private System.String myRights;
		private System.Collections.Generic.IList<System.String> mySplitRights;
		#endregion fields


		#region .ctor
		public AuthorizeAttribute() : base() { 
			mySplitRights = new System.Collections.Generic.List<System.String>();
		}
		#endregion .ctor


		#region properties
		public System.String Rights { 
			get { 
				return myRights ?? System.String.Empty;
			}
			set { 
				myRights = value.TrimToNull();
				mySplitRights = new System.Collections.Generic.List<System.String>( 
					this.Rights.Split( new System.Char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries ) 
				);
			}
		}
		#endregion properties


		#region methods
		protected override System.Boolean AuthorizeCore( System.Web.HttpContextBase httpContext ) { 
			if ( null == httpContext ) { 
				throw new System.ArgumentNullException( "httpContext" );
			}
			var user = httpContext.User;
			if ( !user.Identity.IsAuthenticated ) { 
				return false;
			}
			if ( !base.AuthorizeCore( httpContext ) ) { 
				return false;
			}

			if ( 0 < mySplitRights.Count ) { 
				var username = user.Identity.Name;
				if ( mySplitRights.Any<System.String>( 
					x => Oits.Web.Security.Rights.IsUserDeniedRight( username, x ) 
				) ) { 
					return false;
				}
				var roles = System.Web.Security.Roles.GetRolesForUser( username );
				if ( null != roles.SelectMany<System.String, Oits.Web.Security.Right>( 
						x => Oits.Web.Security.Rights.GetDeniedRightsForRole( x ) 
					).Select<Oits.Web.Security.Right, System.String>( 
						x => x.RightName 
					).Where<System.String>( 
						x => mySplitRights.Contains<System.String>( x, System.StringComparer.OrdinalIgnoreCase ) 
					).FirstOrDefault<System.String>() 
				) { 
					return false;
				}

				if ( mySplitRights.Any<System.String>( 
					x => Oits.Web.Security.Rights.IsUserGrantedRight( username, x ) 
				) ) { 
					return true;
				}
				if ( null != roles.SelectMany<System.String, Oits.Web.Security.Right>( 
						x => Oits.Web.Security.Rights.GetGrantedRightsForRole( x ) 
					).Select<Oits.Web.Security.Right, System.String>( 
						x => x.RightName 
					).Where<System.String>( 
						x => mySplitRights.Contains<System.String>( x, System.StringComparer.OrdinalIgnoreCase ) 
					).FirstOrDefault<System.String>() 
				) { 
					return true;
				}
				return false;
			}
			return true;
		}
		#endregion methods

	}

}