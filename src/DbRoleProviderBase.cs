﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public abstract class DbRoleProviderBase : RoleProviderBase { 

		#region internal classes
		protected enum RoleOperationStatus : int { 
			Success = 0, 
			InvalidRoleName = 1, 
			InvalidUsername = 2, 
			DuplicateRoleName = 3, 
			DuplicateUsername = 4, 
			RoleIsPopulated = 5, 
			InvalidOperation = 11 
		}
		#endregion internal classes


		#region fields
		private System.String myDbMapGroupName = null;
		#endregion fields


		#region .ctor
		protected DbRoleProviderBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public virtual Oits.Configuration.DbMap.DbMapGroupElement DbMapGroup { 
			get {
				return Oits.Configuration.DbMap.DbMapSection.GetSection().Groups[ this.DbMapGroupName ];
			}
		}
		public virtual System.String DbMapGroupName { 
			get { 
				return myDbMapGroupName;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) { 
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "dbMapGroupName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) { 
				throw new System.Configuration.ConfigurationErrorsException( "The dbMapGroupName attribute was not found, or was null, or was empty." );
			} else { 
				myDbMapGroupName = configValue;
			}
			config.Remove( "dbMapGroupName" );
		}
		#endregion ProviderBase

		#region RoleProvider
		public sealed override void AddUsersToRoles( System.String[] usernames, System.String[] roleNames ) { 
			if ( ( null == roleNames ) || ( roleNames.Length < 1 ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			} else if ( ( null == usernames ) || ( usernames.Length < 1 ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			}

			var users = usernames.Select( 
				x => x.CheckParameter( true, true, true, "username" ) 
			).Where( 
				x => ( null != System.Web.Security.Membership.GetUser( x, false ) ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var roles = roleNames.Select( 
							x => x.CheckParameter( true, true, true, "roleName" ) 
						);
						if ( roles.Any( 
							x => !this.RoleExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified roleName does not exist in the current application." );
						}
						this.AddUsersToRoles( cnxn, txn, users, roles );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
		}
		public sealed override void CreateRole( System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						if ( this.RoleExists( cnxn, txn, roleName ) ) { 
							throw new System.InvalidOperationException( "The specified roleName is already defined for this application." );
						}
						this.CreateRole( cnxn, txn, roleName );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
		}
		public sealed override System.Boolean DeleteRole( System.String roleName, System.Boolean throwOnPopulatedRole ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						if ( throwOnPopulatedRole && ( 0 < this.GetUsersInRole( cnxn, txn, roleName ).Length ) ) { 
							throw new System.Configuration.Provider.ProviderException();
						}
						output = this.DeleteRole( cnxn, txn, roleName, throwOnPopulatedRole );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
			return output;
		}
		public sealed override System.String[] FindUsersInRole( System.String roleName, System.String usernameToMatch ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.FindUsersInRole( cnxn, null, roleName, usernameToMatch );
			}
		}
		public sealed override System.String[] GetAllRoles() { 
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetAllRoles( cnxn, null );
			}
		}
		public sealed override System.String[] GetRolesForUser( System.String username ) { 
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetRolesForUser( cnxn, null, username );
			}
		}
		public sealed override System.String[] GetUsersInRole( System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetUsersInRole( cnxn, null, roleName );
			}
		}
		public sealed override System.Boolean IsUserInRole( System.String username, System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.IsUserInRole( cnxn, null, username, roleName );
			}
		}
		public sealed override void RemoveUsersFromRoles( System.String[] usernames, System.String[] roleNames ) { 
			if ( ( null == roleNames ) || ( roleNames.Length < 1 ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			} else if ( ( null == usernames ) || ( usernames.Length < 1 ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			}

			var users = usernames.Select( 
				x => x.CheckParameter( true, true, true, "username" ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var roles = roleNames.Select( 
							x => x.CheckParameter( true, true, true, "roleName" ) 
						);
						if ( roles.Any( 
							x => !this.RoleExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified roleName does not exist in the current application." );
						}
						this.RemoveUsersFromRoles( cnxn, txn, users, roles );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
		}
		public sealed override System.Boolean RoleExists( System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.RoleExists( cnxn, null, roleName );
			}
		}
		#endregion RoleProvider

		protected virtual void CheckReturnParameter( System.Int32 value ) { 
			var v = (RoleOperationStatus)value;
			switch ( v ) { 
				case RoleOperationStatus.Success : 
					break;
				case RoleOperationStatus.InvalidRoleName : 
					throw new System.Configuration.Provider.ProviderException( "The specified roleName is invalid or does not exist for the current application." );
				case RoleOperationStatus.InvalidUsername : 
					throw new System.Configuration.Provider.ProviderException( "The specified username is invalid or does not exist for the current application." );
				case RoleOperationStatus.DuplicateRoleName:
					throw new System.Configuration.Provider.ProviderException( "The specified roleName already exists." );
				case RoleOperationStatus.DuplicateUsername:
					throw new System.Configuration.Provider.ProviderException( "The specified username already exists." );
				case RoleOperationStatus.InvalidOperation: 
					throw new System.Configuration.Provider.ProviderException( "The attempted action is invalid." );
				default : 
					throw new System.Configuration.Provider.ProviderException( "An invalid value was returned by the database server." );
			}
		}

		protected abstract void AddUsersToRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> users, System.Collections.Generic.IEnumerable<System.String> roles );
		protected abstract void CreateRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName );
		protected abstract System.Boolean DeleteRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.Boolean throwOnPopulatedRole );
		protected abstract System.Boolean DeleteRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.Boolean throwOnPopulatedRole, System.Boolean deleteRelatedData );
		protected abstract System.String[] FindUsersInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String usernameToMatch );
		protected abstract System.String[] GetAllRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction );
		protected abstract System.String[] GetRolesForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract System.String[] GetUsersInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName );
		protected abstract System.Boolean IsUserInRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String roleName );
		protected abstract void RemoveUsersFromRoles( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> users, System.Collections.Generic.IEnumerable<System.String> roles );
		protected abstract System.Boolean RoleExists( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName );
		#endregion methods

	}

}