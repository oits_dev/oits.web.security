﻿using System.Linq;

namespace Oits.Web.Security { 

	public abstract class DbRightProviderBase : RightProviderBase { 

		#region internal classes
		protected enum RightOperationStatus : int { 
			Success = 0, 
			InvalidRight = 1, 
			InvalidResource = 2, 
			InvalidRole = 3, 
			InvalidUser = 4, 
			DuplicateRight = 5, 
			DuplicateResource = 6, 
			DuplicateRole = 7, 
			DuplicateUser = 8, 
			RightIsPopulated = 9, 
			InvalidOperation = 11 
		}
		#endregion internal classes


		#region fields
		private System.String myDbMapGroupName;
		private System.Int32 myMaxResourceLength = 1024;
		private System.String myApplicationName;
		#endregion fields


		#region .ctor
		protected DbRightProviderBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public virtual Oits.Configuration.DbMap.DbMapGroupElement DbMapGroup {
			get {
				return Oits.Configuration.DbMap.DbMapSection.GetSection().Groups[ this.DbMapGroupName ];
			}
		}
		public virtual System.String DbMapGroupName { 
			get { 
				return myDbMapGroupName;
			}
		}
		public virtual System.Int32 MaxResourceLength { 
			get { 
				return myMaxResourceLength;
			}
		}
		public override string ApplicationName { 
			get { 
				return myApplicationName;
			}
			set { 
				value = value.TrimToNull();
				if ( System.String.IsNullOrEmpty( value ) ) { 
					throw new System.ArgumentException( "ApplicationName property may not be null or empty.", "ApplicationName" );
				}
				myApplicationName = value;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) { 
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "applicationName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) { 
				configValue = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			}
			myApplicationName = configValue;
			config.Remove( "applicationName" );

			configValue = config[ "dbMapGroupName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) { 
				throw new System.Configuration.ConfigurationErrorsException( "The dbMapGroupName attribute was not found, was null, or was empty." );
			} else { 
				myDbMapGroupName = configValue;
			}
			config.Remove( "dbMapGroupName" );

			configValue = config[ "maxResourceLength" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				try { 
					myMaxResourceLength = System.Int32.Parse( configValue );
				} catch ( System.Exception e ) { 
					throw new System.Configuration.ConfigurationErrorsException( e.Message, e );
				}
				if ( myMaxResourceLength < 1 ) { 
					throw new System.Configuration.ConfigurationErrorsException( "maxResourceLength attribute must be positive." );
				}
			}
			config.Remove( "maxResourceLength" );
		}
		#endregion ProviderBase

		#region RightProviderBase
		public sealed override void ClearRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			}

			var roles = roleNames.Select( 
				x => x.CheckParameter( true, true, true, "roleName" ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.ClearRolesRights( cnxn, txn, roles, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public sealed override void ClearUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || ( usernames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			}

			var users = usernames.Select( 
				x => x.CheckParameter( true, true, true, "username" ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.ClearUsersRights( cnxn, txn, users, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public sealed override void CreateRight( System.String rightName, System.String resource ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			resource = this.CheckResourceParameter( resource, "resource" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				this.CreateRight( cnxn, null, rightName, resource );
			}
		}
		public override System.Boolean DeleteRight( System.String rightName, System.Boolean throwIfPopulated, System.Boolean deleteRelatedData ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						output = this.DeleteRight( cnxn, null, rightName, throwIfPopulated, deleteRelatedData );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override void DenyRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			}

			var roles = roleNames.Select( 
				x => x.CheckParameter( true, true, true, "roleName" ) 
			).Where( 
				x => System.Web.Security.Roles.RoleExists( x ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.DenyRolesRights( cnxn, txn, roles, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public sealed override void DenyUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || !usernames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			}

			var users = usernames.Select( 
				x => x.CheckParameter( true, true, true, "username" ) 
			).Where( 
				x => ( null != System.Web.Security.Membership.GetUser( x, false ) ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.DenyUsersRights( cnxn, txn, users, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public sealed override System.Collections.Generic.IList<Right> FindRightsByResource( System.String resourceToMatch ) { 
			if ( null != resourceToMatch ) { 
				resourceToMatch = resourceToMatch.Trim();
			}

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.FindRightsByResource( cnxn, null, resourceToMatch );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> FindRightsByRightName( System.String rightNameToMatch ) { 
			rightNameToMatch = rightNameToMatch.TrimToNull();
			if ( System.String.IsNullOrEmpty( rightNameToMatch ) ) { 
				throw new System.ArgumentNullException( "rightNameToMatch" );
			}

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.FindRightsByRightName( cnxn, null, rightNameToMatch );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> GetAllRights() { 
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetAllRights( cnxn, null );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> GetDeniedRightsForRole( System.String roleName ) { 
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetDeniedRightsForRole( cnxn, null, roleName );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> GetDeniedRightsForUser( System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetDeniedRightsForUser( cnxn, null, username );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> GetGrantedRightsForRole( System.String roleName ) {
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetGrantedRightsForRole( cnxn, null, roleName );
			}
		}
		public sealed override System.Collections.Generic.IList<Right> GetGrantedRightsForUser( System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetGrantedRightsForUser( cnxn, null, username );
			}
		}
		public sealed override Right GetRight( System.String rightName ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetRight( cnxn, null, rightName );
			}
		}
		public sealed override Right GetRightByResource( System.String resource ) { 
			resource = this.CheckResourceParameter( resource, "resource" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				return this.GetRightByResource( cnxn, null, resource );
			}
		}
		public sealed override void GrantRolesRights( System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == roleNames ) || !roleNames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "roleNames" );
			}

			var roles = roleNames.Select( 
				x => x.CheckParameter( true, true, true, "roleName" ) 
			).Where( 
				x => System.Web.Security.Roles.RoleExists( x ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try {
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.GrantRolesRights( cnxn, txn, roles, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
		}
		public sealed override void GrantUsersRights( System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames ) { 
			if ( ( null == rightNames ) || ( rightNames.Count() < 1 ) ) { 
				throw new System.ArgumentNullException( "rightNames" );
			} else if ( ( null == usernames ) || !usernames.Any( x => true ) ) { 
				throw new System.ArgumentNullException( "usernames" );
			}

			var users = usernames.Select( 
				x => x.CheckParameter( true, true, true, "username" ) 
			).Where( 
				x => ( null != System.Web.Security.Membership.GetUser( x, false ) ) 
			);
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) { 
					try { 
						var rights = rightNames.Select( 
							x => x.CheckParameter( true, true, true, "rightName" ) 
						);
						if ( rights.Any( 
							x => !this.RightExists( cnxn, txn, x ) 
						) ) { 
							throw new System.Configuration.Provider.ProviderException( "The specified rightName does not exist in the current application." );
						}
						this.GrantUsersRights( cnxn, txn, users, rights );
						txn.Commit();
					} catch ( System.Exception ) { 
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
		}
		public sealed override System.Boolean IsRoleDeniedRight( System.String roleName, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.IsRoleDeniedRight( cnxn, null, roleName, rightName );
			}
		}
		public sealed override System.Boolean IsRoleGrantedRight( System.String roleName, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			roleName = roleName.CheckParameter( true, true, true, "roleName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.IsRoleGrantedRight( cnxn, null, roleName, rightName );
			}
		}
		public sealed override System.Boolean IsUserDeniedRight( System.String username, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.IsUserDeniedRight( cnxn, null, username, rightName );
			}
		}
		public sealed override System.Boolean IsUserGrantedRight( System.String username, System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.IsUserGrantedRight( cnxn, null, username, rightName );
			}
		}
		public sealed override System.Boolean RightExists( System.String rightName ) {
			rightName = rightName.CheckParameter( true, true, true, "rightName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.RightExists( cnxn, null, rightName );
			}
		}
		public sealed override void SetRightResource( System.String rightName, System.String resource ) { 
			rightName = rightName.CheckParameter( true, true, true, "rightName" );
			resource = this.CheckResourceParameter( resource, "resource" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) { 
				this.SetRightResource( cnxn, null, rightName, resource );
			}
		}
		#endregion RightProviderBase

		protected virtual void CheckReturnValue( System.Int32 value ) { 
			var v = (DbRightProvider.RightOperationStatus)value;
			switch ( v ) { 
				case RightOperationStatus.Success : 
					break;
				case RightOperationStatus.InvalidResource : 
					throw new System.Configuration.Provider.ProviderException( "The specified resource is invalid." );
				case RightOperationStatus.InvalidRight : 
					throw new System.Configuration.Provider.ProviderException( "The specified rightName is invalid or does not exist for the current application." );
				case RightOperationStatus.InvalidRole : 
					throw new System.Configuration.Provider.ProviderException( "The specified roleName is invalid or does not exist for the current application." );
				case RightOperationStatus.InvalidUser : 
					throw new System.Configuration.Provider.ProviderException( "The specified username is invalid or does not exist for the current application." );
				case RightOperationStatus.DuplicateResource : 
					throw new System.Configuration.Provider.ProviderException( "The specified resource already exists." );
				case RightOperationStatus.DuplicateRight : 
					throw new System.Configuration.Provider.ProviderException( "The specified rightName already exists." );
				case RightOperationStatus.DuplicateRole : 
					throw new System.Configuration.Provider.ProviderException( "The specified roleName already exists." );
				case RightOperationStatus.DuplicateUser : 
					throw new System.Configuration.Provider.ProviderException( "The specified username already exists." );
				case RightOperationStatus.RightIsPopulated : 
					throw new System.Configuration.Provider.ProviderException( "The specified rightName is populated." );
				case RightOperationStatus.InvalidOperation : 
					throw new System.Configuration.Provider.ProviderException( "The attempted action is invalid." );
				default : 
					throw new System.Configuration.Provider.ProviderException( "An invalid value was returned by the database server." );
			}
		}
		protected virtual System.String CheckResourceParameter( System.String resource, System.String parameterName ) { 
			parameterName = parameterName.TrimToNull();
			if ( System.String.IsNullOrEmpty( parameterName ) ) { 
				throw new System.ArgumentNullException( "parameterName" );
			}
			return resource.TrimToNull().CheckParameter( false, this.RequireUniqueResource, this.RequireUniqueResource, this.RequireUniqueResource ? 1 : 0, this.MaxResourceLength, parameterName );
		}

		protected virtual Right ReadRightFromDb( System.Data.Common.DbDataReader reader, Oits.Configuration.DbMap.DbResultElement config ) { 
			if ( null == config ) { 
				throw new System.ArgumentNullException( "config" );
			} else if ( null == reader ) { 
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( reader.IsClosed ) || ( !reader.HasRows ) ) { 
				return null;
			}

			System.String right = reader.GetString( reader.GetOrdinal( config.Columns[ "RightName" ].ColumnName ) ).TrimToNull();
			System.String res = null;
			System.Int32 o = reader.GetOrdinal( config.Columns[ "Resource" ].ColumnName );
			if ( !reader.IsDBNull( o ) ) { 
				res = reader.GetString( o ).TrimToNull();
			}

			return new Right( right, res );
		}

		protected abstract void ClearRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract void ClearUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract void CreateRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.String resource );
		protected abstract System.Boolean DeleteRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.Boolean throwIfPopulated, System.Boolean deleteRelatedData );
		protected abstract void DenyUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract void DenyRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract System.Collections.Generic.IList<Right> FindRightsByResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String resource );
		protected abstract System.Collections.Generic.IList<Right> FindRightsByRightName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightNameToMatch );
		protected abstract System.Collections.Generic.IList<Right> GetAllRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction );
		protected abstract System.Collections.Generic.IList<Right> GetGrantedRightsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract System.Collections.Generic.IList<Right> GetDeniedRightsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract System.Collections.Generic.IList<Right> GetGrantedRightsForRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName );
		protected abstract System.Collections.Generic.IList<Right> GetDeniedRightsForRole( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName );
		protected abstract Right GetRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName );
		protected abstract Right GetRightByResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String resource );
		protected abstract void GrantUsersRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> usernames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract void GrantRolesRights( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<System.String> roleNames, System.Collections.Generic.IEnumerable<System.String> rightNames );
		protected abstract System.Boolean IsUserGrantedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String rightName );
		protected abstract System.Boolean IsUserDeniedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String rightName );
		protected abstract System.Boolean IsRoleGrantedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String rightName );
		protected abstract System.Boolean IsRoleDeniedRight( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String roleName, System.String rightName );
		protected abstract System.Boolean RightExists( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName );
		protected abstract void SetRightResource( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String rightName, System.String resource );
		#endregion methods

	}

}