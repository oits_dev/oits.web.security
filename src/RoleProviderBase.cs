﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public abstract class RoleProviderBase : System.Web.Security.RoleProvider {

		#region fields
		private System.String myApplicationName;
		#endregion fields


		#region .ctor
		protected RoleProviderBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public override System.String ApplicationName {
			get {
				return myApplicationName;
			}
			set {
				value = value.TrimToNull();
				if ( System.String.IsNullOrEmpty( value ) ) {
					throw new System.ArgumentException( "ApplicationName property may not be null or empty.", "ApplicationName" );
				}
				myApplicationName = value;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) {
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "applicationName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) {
				configValue = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			}
			myApplicationName = configValue;
			config.Remove( "applicationName" );
		}
		#endregion ProviderBase
		#endregion methods

	}

}