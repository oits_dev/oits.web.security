﻿using System.Linq;

namespace Oits.Web.Security { 

	public static class ExtentionMethods { 

		public static System.String TrimToNull( this System.String @string ) { 
			if ( System.String.IsNullOrEmpty( @string ) ) { 
				return null;
			}

			@string = @string.Trim();
			if ( System.String.IsNullOrEmpty( @string ) ) { 
				return null;
			}

			return @string;
		}

		public static System.String CheckParameter( this System.String @string, System.Boolean checkComma, System.Boolean checkEmpty, System.Boolean checkNull, System.Int32 minLength, System.Int32 maxLength, System.String parameterName ) { 
			if ( System.String.IsNullOrEmpty( parameterName.TrimToNull() ) ) { 
				throw new System.ArgumentNullException( "parameterName" );
			} else if ( maxLength <= 0 ) { 
				throw new System.ArgumentException( "Value must be positive.", "maxLength" );
			} else if ( maxLength < minLength ) { 
				throw new System.ArgumentException( "Value must be less than or equal to maxLength parameter.", "minLength" );
			} else if ( minLength < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "Value may not be negative.", "minLength" );
			}

			if ( checkNull && ( null == @string ) ) { 
				throw new System.ArgumentNullException( parameterName );
			}
			if ( null != @string ) { 
				@string = @string.Trim();
				if ( checkEmpty && System.String.IsNullOrEmpty( @string ) ) { 
					throw new System.ArgumentNullException( parameterName );
				}
				var len = @string.Length;
				if ( maxLength < len ) { 
					throw new System.ArgumentOutOfRangeException( "Value is too long.", parameterName );
				}
				if ( len < minLength ) { 
					throw new System.ArgumentOutOfRangeException( "Value is too short.", parameterName );
				}
				if ( checkComma && @string.Contains( ',' ) ) { 
					throw new System.ArgumentOutOfRangeException( "Value may not contain commas.", parameterName );
				}
			}
			return @string;
		}
		public static System.String CheckParameter( this System.String @string, System.Boolean checkComma, System.Boolean checkEmpty, System.Boolean checkNull, System.String parameterName ) { 
			return CheckParameter( @string, checkComma, checkEmpty, checkNull, 1, System.Int32.MaxValue, parameterName );
		}
		public static System.String CheckParameter( this System.String @string, System.String parameterName ) {	
			return CheckParameter( @string, true, true, true, 1, System.Int32.MaxValue, parameterName );
		}

		public static System.String CheckPasswdParameter( this System.String @string, System.Web.Security.MembershipProvider provider, System.String parameterName ) { 
			if ( null == provider ) { 
				throw new System.ArgumentOutOfRangeException( "provider" );
			}
			return CheckPasswdParameter( @string, provider.MinRequiredPasswordLength, provider.MinRequiredNonAlphanumericCharacters, provider.PasswordStrengthRegularExpression, parameterName );
		}
		public static System.String CheckPasswdParameter( this System.String @string, System.Int32 minLength, System.Int32 minAlphaNumericChars, System.String regularExpression, System.String parameterName ) { 
			if ( System.String.IsNullOrEmpty( parameterName.TrimToNull() ) ) { 
				throw new System.ArgumentNullException( "parameterName" );
			} else if ( minAlphaNumericChars < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "Value may not be negative.", "MinRequiredNonAlphanumericCharacters" );
			} else if ( minLength < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "Value may not be negative.", "MinRequiredPasswordLength" );
			}

			@string = @string.CheckParameter( false, true, true, minLength, System.Int32.MaxValue, parameterName );
			if ( @string.Count<System.Char>( x => !System.Char.IsLetterOrDigit( x ) ) < System.Math.Min( minAlphaNumericChars, minLength ) ) { 
				throw new System.ArgumentOutOfRangeException( "Value contains too few non alpha-numeric characters.", parameterName );
			}
			regularExpression = regularExpression.TrimToNull();
			if ( !System.String.IsNullOrEmpty( regularExpression ) ) { 
				var match = System.Text.RegularExpressions.Regex.Match( @string, regularExpression, System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.Singleline );
				if ( 
					!match.Success 
					|| ( 0 != match.Index ) 
					|| ( @string.Length != match.Length ) 
				) { 
					throw new System.ArgumentOutOfRangeException( parameterName );
				}
			}
			return @string;
		}

		public static System.String GetUsername( this System.Web.HttpContext context ) { 
			return GetUsername( context, true );
		}
		public static System.String GetUsername( this System.Web.HttpContext context, System.Boolean useAnonymous ) { 
			if ( null == context ) { 
				throw new System.ArgumentNullException( "context" );
			}

			System.String output = null;
			if ( context.User.Identity.IsAuthenticated ) { 
				output = context.User.Identity.Name;
			} else { 
				if ( useAnonymous ) { 
					output = context.Request.AnonymousID.TrimToNull();
				}
				if ( System.String.IsNullOrEmpty( output ) || !useAnonymous ) { 
					output = System.Threading.Thread.CurrentPrincipal.Identity.Name;
				}
			}
			return output;
		}

	}

}