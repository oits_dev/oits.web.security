﻿using System.Linq;
namespace Oits.Web.Security { 

	public abstract class PasswordValidatorControllerBase : System.Web.Mvc.Controller { 

		#region fields
		private const System.String theDefaultFieldName = "Password";
		private const System.String theDefaultRequiredErrorMessage = "'{0}' is required.";
		private const System.String theDefaultMinLengthErrorMessage = "'{0}' must be at least '{1:g}' characters long.";
		private const System.String theDefaultMinRequiredNonalphanumericCharactersErrorMessage = "'{0}' must containt at least '{1:g}' non-alphanumeric characters.";
		private const System.String theDefaultStrengthRegularExpressionErrorMessage = "'{0}' does neet meet the Password Strength Regular Expression.";

		private System.String myFieldName;
		private System.String myRequiredErrorMessage;
		private System.String myMinLengthErrorMessage;
		private System.String myMinRequiredNonalphanumericCharactersErrorMessage;
		private System.String myStrengthRegularExpressionErrorMessage;
		private System.String myProviderName;
		#endregion fields


		#region .ctor
		protected PasswordValidatorControllerBase() : this( null ) { 
		}
		protected PasswordValidatorControllerBase( System.String fieldName ) : this( fieldName.TrimToNull() ?? theDefaultFieldName, null ) { 
		}
		protected PasswordValidatorControllerBase( System.String fieldName, System.String providerName ) : this( fieldName.TrimToNull() ?? theDefaultFieldName, theDefaultMinLengthErrorMessage, theDefaultMinRequiredNonalphanumericCharactersErrorMessage, theDefaultStrengthRegularExpressionErrorMessage, providerName.TrimToNull() ) { 
		}
		protected PasswordValidatorControllerBase( System.String fieldName, System.String minLengthErrorMessage, System.String minRequiredNonalphanumericCharactersErrorMessage, System.String strengthRegularExpressionErrorMessage, System.String providerName ) : base() { 
			myFieldName = theDefaultFieldName.TrimToNull() ?? theDefaultFieldName;
			myMinLengthErrorMessage = theDefaultMinLengthErrorMessage.TrimToNull() ?? theDefaultMinLengthErrorMessage;
			myMinRequiredNonalphanumericCharactersErrorMessage = theDefaultMinRequiredNonalphanumericCharactersErrorMessage.TrimToNull() ?? theDefaultMinRequiredNonalphanumericCharactersErrorMessage;
			myStrengthRegularExpressionErrorMessage = theDefaultStrengthRegularExpressionErrorMessage.TrimToNull() ?? theDefaultStrengthRegularExpressionErrorMessage;
		}
		#endregion .ctor


		#region properties
		public virtual System.String ProviderName { 
			get { 
				return myProviderName;
			}
			set { 
				myProviderName = value.TrimToNull();
			}
		}
		public virtual System.Web.Security.MembershipProvider Provider { 
			get { 
				if ( System.String.IsNullOrEmpty( this.ProviderName ) ) { 
					return System.Web.Security.Membership.Provider;
				} else { 
					return System.Web.Security.Membership.Providers[ this.ProviderName ];
				}
			}
		}

		public virtual System.String FieldName { 
			get { 
				return myFieldName;
			}
			set { 
				myFieldName = value.TrimToNull() ?? theDefaultFieldName;
			}
		}
		public virtual System.String RequiredErrorMessage {
			get {
				return myRequiredErrorMessage;
			}
			set {
				myRequiredErrorMessage = value.TrimToNull() ?? theDefaultRequiredErrorMessage;
			}
		}
		public virtual System.String MinLengthErrorMessage { 
			get { 
				return myMinLengthErrorMessage;
			}
			set { 
				myMinLengthErrorMessage = value.TrimToNull() ?? theDefaultMinLengthErrorMessage;
			}
		}
		public virtual System.String MinRequiredNonalphanumericCharactersErrorMessage { 
			get { 
				return myMinRequiredNonalphanumericCharactersErrorMessage;
			}
			set { 
				myMinRequiredNonalphanumericCharactersErrorMessage = value.TrimToNull() ?? theDefaultMinRequiredNonalphanumericCharactersErrorMessage;
			}
		}
		public virtual System.String StrengthRegularExpressionErrorMessage { 
			get { 
				return myStrengthRegularExpressionErrorMessage;
			}
			set { 
				myStrengthRegularExpressionErrorMessage = value.TrimToNull() ?? theDefaultStrengthRegularExpressionErrorMessage;
			}
		}
		#endregion properties


		#region methods
		[System.Web.Mvc.OutputCache( Location = System.Web.UI.OutputCacheLocation.None, NoStore = true )]
		[System.Web.Mvc.HttpGet]
		[System.Web.Mvc.AllowAnonymous]
		public virtual System.Web.Mvc.JsonResult ValidatePassword( System.String password ) { 
			System.ComponentModel.DataAnnotations.ValidationResult output;

			output = this.IsValidPassword( password );
			if ( System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( output ) ) { 
				return Json( true, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			} else { 
				return Json( output.ErrorMessage, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			}
		}
		public virtual System.ComponentModel.DataAnnotations.ValidationResult IsValidPassword( System.String password ) { 
			System.ComponentModel.DataAnnotations.ValidationResult status;

			status = this.IsValidPasswordRequired( password );
			if ( !System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( status ) ) { 
				return status;
			}

			status = this.IsValidPasswordMinLength( password );
			if ( !System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( status ) ) { 
				return status;
			}

			status = this.IsValidPasswordMinRequiredNonalphanumericCharacters( password );
			if ( !System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( status ) ) { 
				return status;
			}

			status = this.IsValidPasswordStrengthRegularExpression( password );
			if ( !System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( status ) ) { 
				return status;
			}

			return status;
		}

		[System.Web.Mvc.OutputCache( Location = System.Web.UI.OutputCacheLocation.None, NoStore = true )]
		[System.Web.Mvc.HttpGet]
		[System.Web.Mvc.AllowAnonymous]
		public virtual System.Web.Mvc.JsonResult ValidatePasswordRequired( System.String password ) { 
			var output = this.IsValidPasswordRequired( password );
			if ( System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( output ) ) { 
				return Json( true, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			} else {
				return Json( output.ErrorMessage, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			}
		}
		public virtual System.ComponentModel.DataAnnotations.ValidationResult IsValidPasswordRequired( System.String password ) { 
			if ( null == password ) { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( 
					System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.RequiredErrorMessage, this.FieldName ) 
				);
			} else { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}
		}

		[System.Web.Mvc.OutputCache( Location = System.Web.UI.OutputCacheLocation.None, NoStore = true )]
		[System.Web.Mvc.HttpGet]
		[System.Web.Mvc.AllowAnonymous]
		public virtual System.Web.Mvc.JsonResult ValidatePasswordMinLength( System.String password ) { 
			var output = this.IsValidPasswordMinLength( password );
			if ( System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( output ) ) { 
				return Json( true, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			} else {
				return Json( output.ErrorMessage, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			}
		}
		public virtual System.ComponentModel.DataAnnotations.ValidationResult IsValidPasswordMinLength( System.String password ) { 
			var min = System.Math.Max( this.Provider.MinRequiredPasswordLength, 1 );
			if ( 0 == min ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null == password ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( min <= password.Length ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( 
					System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.MinLengthErrorMessage, this.FieldName, min ) 
				);
			}
		}

		[System.Web.Mvc.OutputCache( Location = System.Web.UI.OutputCacheLocation.None, NoStore = true )]
		[System.Web.Mvc.HttpGet]
		[System.Web.Mvc.AllowAnonymous]
		public virtual System.Web.Mvc.JsonResult ValidatePasswordMinRequiredNonalphanumericCharacters( System.String password ) { 
			var output = this.IsValidPasswordMinRequiredNonalphanumericCharacters( password );
			if ( System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( output ) ) { 
				return Json( true, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			} else {
				return Json( output.ErrorMessage, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			}
		}
		public virtual System.ComponentModel.DataAnnotations.ValidationResult IsValidPasswordMinRequiredNonalphanumericCharacters( System.String password ) { 
			var naLen = ( password ?? System.String.Empty ).Count( 
				x => !System.Char.IsLetterOrDigit( x ) 
			);
			var nonalphamin = this.Provider.MinRequiredNonAlphanumericCharacters;
			if ( nonalphamin == 0 ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null == password ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( nonalphamin <= naLen ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( 
					System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.StrengthRegularExpressionErrorMessage, this.FieldName, nonalphamin ) 
				);
			}
		}

		[System.Web.Mvc.OutputCache( Location = System.Web.UI.OutputCacheLocation.None, NoStore = true )]
		[System.Web.Mvc.HttpGet]
		[System.Web.Mvc.AllowAnonymous]
		public virtual System.Web.Mvc.JsonResult ValidatePasswordStrengthRegularExpression( System.String password ) { 
			var output = this.IsValidPasswordStrengthRegularExpression( password );
			if ( System.ComponentModel.DataAnnotations.ValidationResult.Success.Equals( output ) ) { 
				return Json( true, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			} else { 
				return Json( output.ErrorMessage, System.Web.Mvc.JsonRequestBehavior.AllowGet );
			}
		}
		public virtual System.ComponentModel.DataAnnotations.ValidationResult IsValidPasswordStrengthRegularExpression( System.String password ) { 
			var pattern = this.Provider.PasswordStrengthRegularExpression.TrimToNull();
			if ( System.String.IsNullOrEmpty( pattern ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( null == password ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( System.Text.RegularExpressions.Regex.IsMatch( pattern, password ) ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return new System.ComponentModel.DataAnnotations.ValidationResult( 
					System.String.Format( System.Globalization.CultureInfo.CurrentCulture, this.StrengthRegularExpressionErrorMessage, this.FieldName, pattern ) 
				);
			}
		}
		#endregion methods

	}

}