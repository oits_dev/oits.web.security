﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public class ParameterValuesBase : Oits.Configuration.DbMap.ReturnValueBase { 

		#region .ctor
		public ParameterValuesBase() : base() { 
		}
		#endregion .ctor


		#region properties
		public System.String ApplicationName { 
			get;
			set;
		}
		public virtual System.String RequestingUser { 
			get { 
				if ( System.Web.HttpContext.Current.User.Identity.IsAuthenticated ) { 
					return ( WebMatrix.WebData.WebSecurity.CurrentUserName ?? System.Web.HttpContext.Current.User.Identity.Name );
				} else { 
					return System.Threading.Thread.CurrentPrincipal.Identity.Name;
				}
			}
		}
		#endregion properties

	}

}