﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security {

	public abstract class DbMembershipProviderBase : WebMatrix.WebData.ExtendedMembershipProvider {

		#region internal classes
		protected class UserPasswordOption {
			private System.Web.Security.MembershipPasswordFormat myFormat;
			private System.String myAlgorithm;
			private System.String mySalt;

			public UserPasswordOption() : base() {
				myFormat = System.Web.Security.Membership.Provider.PasswordFormat;
				myAlgorithm = System.Web.Security.Membership.HashAlgorithmType;
				mySalt = null;
			}
			public UserPasswordOption( System.Web.Security.MembershipPasswordFormat format, System.String algorithm ) : this() {
				this.Format = format;
				this.Algorithm = algorithm;
			}
			public UserPasswordOption( System.Web.Security.MembershipPasswordFormat format, System.String algorithm, System.String salt ) : this( format, algorithm ) {
				mySalt = salt.TrimToNull();
			}

			public System.Web.Security.MembershipPasswordFormat Format {
				get {
					return myFormat;
				}
				set {
					myFormat = value;
				}
			}
			public System.String Algorithm {
				get {
					return myAlgorithm;
				}
				set {
					myAlgorithm = value.TrimToNull() ?? System.Web.Security.Membership.HashAlgorithmType;
				}
			}
			public System.String Salt {
				get {
					return mySalt;
				}
				set {
					mySalt = value.TrimToNull();
				}
			}
		}
		#endregion internal classes


		#region fields
		public static readonly System.DateTime MinSqlDateTime;

		private System.String myDbMapGroupName = null;
		private System.String myApplicationName = null;
		private System.Boolean myEnablePasswordReset = false;
		private System.Boolean myEnablePasswordRetrieval = false;
		private System.Int32 myMaxInvalidPasswordAttempts = 6;
		private System.Int32 myMinRequiredNonAlphanumericCharacters = 2;
		private System.Int32 myMinRequiredPasswordLength = 6;
		private System.Int32 myPasswordAttemptWindow = 60;
		private System.Web.Security.MembershipPasswordFormat myPasswordFormat = System.Web.Security.MembershipPasswordFormat.Clear;
		private System.String myPasswordStrengthRegularExpression = null;
		private System.Boolean myRequiresQuestionAndAnswer = false;
		private System.Boolean myRequiresUniqueEmail = false;
		private System.Int32 myTokenLength = 0x10;
		private System.Int32 myTokenLifetime = 1440;
		private System.Int32 mySaltSize = 0x10;
		private System.Boolean myRequireConfirmation = false;
		private System.Byte[] myIV = null;
		#endregion fields


		#region .ctor
		static DbMembershipProviderBase() {
			MinSqlDateTime = new System.DateTime( 1753, 1, 1, 0, 0, 1 );
		}

		protected DbMembershipProviderBase() : base() {
		}
		#endregion .ctor


		#region properaties
		#region MembershipProvider
		public override System.String ApplicationName {
			get {
				return myApplicationName;
			}
			set {
				value = value.TrimToNull();
				if ( System.String.IsNullOrEmpty( value ) ) {
					throw new System.ArgumentException( "ApplicationName property may not be null or empty.", "ApplicationName" );
				}
				myApplicationName = value;
			}
		}
		public override System.Boolean EnablePasswordReset {
			get {
				return myEnablePasswordReset;
			}
		}
		public override System.Boolean EnablePasswordRetrieval {
			get {
				return myEnablePasswordRetrieval;
			}
		}
		public override System.Int32 MaxInvalidPasswordAttempts {
			get {
				return myMaxInvalidPasswordAttempts;
			}
		}
		public override System.Int32 MinRequiredNonAlphanumericCharacters {
			get {
				return myMinRequiredNonAlphanumericCharacters;
			}
		}
		public override System.Int32 MinRequiredPasswordLength {
			get {
				return myMinRequiredPasswordLength;
			}
		}
		public override System.Int32 PasswordAttemptWindow {
			get {
				return myPasswordAttemptWindow;
			}
		}
		public override System.Web.Security.MembershipPasswordFormat PasswordFormat {
			get {
				return myPasswordFormat;
			}
		}
		public override System.String PasswordStrengthRegularExpression {
			get {
				return myPasswordStrengthRegularExpression;
			}
		}
		public override System.Boolean RequiresQuestionAndAnswer {
			get {
				return myRequiresQuestionAndAnswer;
			}
		}
		public override System.Boolean RequiresUniqueEmail {
			get {
				return myRequiresUniqueEmail;
			}
		}
		#endregion MembershipProvider

		public virtual Oits.Configuration.DbMap.DbMapGroupElement DbMapGroup {
			get {
				return Oits.Configuration.DbMap.DbMapSection.GetSection().Groups[ this.DbMapGroupName ];
			}
		}
		public virtual System.String DbMapGroupName {
			get {
				return myDbMapGroupName;
			}
		}
		public virtual System.Int32 TokenLength {
			get {
				return myTokenLength;
			}
		}
		protected virtual System.Int32 SaltSize {
			get {
				return mySaltSize;
			}
		}
		public virtual System.Boolean RequireConfirmation {
			get {
				return myRequireConfirmation;
			}
		}
		public virtual System.Int32 TokenLifetime {
			get {
				return myTokenLifetime;
			}
		}

		protected virtual System.Byte[] IV {
			get {
				return myIV;
			}
			set {
				myIV = value;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( System.String name, System.Collections.Specialized.NameValueCollection config ) {
			base.Initialize( name, config );

			System.String configValue;
			System.Int32 configInt;
			System.Boolean configBool;

			configValue = config[ "applicationName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) {
				configValue = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			}
			myApplicationName = configValue;
			config.Remove( "applicationName" );

			configValue = config[ "dbMapGroupName" ].TrimToNull();
			if ( System.String.IsNullOrEmpty( configValue ) ) {
				throw new System.Configuration.ConfigurationErrorsException( "The dbMapGroupName attribute was not found, or was null, or was empty." );
			} else {
				myDbMapGroupName = configValue;
			}
			config.Remove( "dbMapGroupName" );

			configValue = config[ "tokenLength" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( !System.Int32.TryParse( configValue, out configInt ) || ( ( configInt < 0x10 ) || ( System.SByte.MaxValue <= configInt ) ) ) {
					throw new System.Configuration.ConfigurationErrorsException( "The TokenLength must be at least 16 characters and less than 128 characters." );
				}
				myTokenLength = configInt;
			}
			config.Remove( "tokenLength" );

			configValue = config[ "saltSize" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( !System.Int32.TryParse( configValue, out configInt ) || ( ( configInt < 0x10 ) || ( System.SByte.MaxValue <= configInt ) ) ) {
					throw new System.Configuration.ConfigurationErrorsException( "The SaltSize must be at least 16 bytes and less than 128 bytes." );
				}
				mySaltSize = configInt;
			}
			config.Remove( "saltSize" );

			configValue = config[ "enablePasswordReset" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Boolean.TryParse( configValue, out configBool ) ) {
					myEnablePasswordReset = configBool;
				}
			}
			config.Remove( "enablePasswordReset" );

			configValue = config[ "maxInvalidPasswordAttempts" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Int32.TryParse( configValue, out configInt ) ) {
					myMaxInvalidPasswordAttempts = configInt;
				}
			}
			config.Remove( "maxInvalidPasswordAttempts" );

			configValue = config[ "minRequiredPasswordLength" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Int32.TryParse( configValue, out configInt ) ) {
					myMinRequiredPasswordLength = configInt;
				}
			}
			config.Remove( "minRequiredPasswordLength" );

			configValue = config[ "minRequiredNonalphanumericCharacters" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Int32.TryParse( configValue, out configInt ) ) {
					myMinRequiredNonAlphanumericCharacters = configInt;
					if ( myMinRequiredPasswordLength < myMinRequiredNonAlphanumericCharacters ) {
						throw new System.Configuration.ConfigurationErrorsException( "MinRequiredPasswordLength value cannot be less than the MinRequiredNonAlphanumericCharacters." );
					}
				}
			}
			config.Remove( "minRequiredNonalphanumericCharacters" );

			configValue = config[ "passwordAttemptWindow" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( true == System.Int32.TryParse( configValue, out configInt ) ) {
					myPasswordAttemptWindow = configInt;
				}
			}
			config.Remove( "passwordAttemptWindow" );

			configValue = config[ "passwordStrengthRegularExpression" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				myPasswordStrengthRegularExpression = configValue;
			}
			config.Remove( "passwordStrengthRegularExpression" );

			configValue = config[ "requiresQuestionAndAnswer" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Boolean.TryParse( configValue, out configBool ) ) {
					myRequiresQuestionAndAnswer = configBool;
				}
			}
			config.Remove( "requiresQuestionAndAnswer" );

			configValue = config[ "requiresUniqueEmail" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Boolean.TryParse( configValue, out configBool ) ) {
					myRequiresUniqueEmail = configBool;
				}
			}
			config.Remove( "requiresUniqueEmail" );

			configValue = config[ "requireConfirmation" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Boolean.TryParse( configValue, out configBool ) ) {
					myRequireConfirmation = configBool;
				}
			}
			config.Remove( "requireConfirmation" );

			configValue = config[ "enablePasswordRetrieval" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Boolean.TryParse( configValue, out configBool ) ) {
					myEnablePasswordRetrieval = configBool;
				}
			}
			config.Remove( "enablePasswordRetrieval" );

			configValue = config[ "passwordFormat" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				System.Web.Security.MembershipPasswordFormat configPasswordFormat = myPasswordFormat;
				if ( !System.Enum.TryParse<System.Web.Security.MembershipPasswordFormat>( configValue, true, out configPasswordFormat ) ) {
					if ( System.Int32.TryParse( configValue, out configInt ) ) {
						configPasswordFormat = (System.Web.Security.MembershipPasswordFormat)configInt;
					}
				}
				switch ( configPasswordFormat ) {
					case System.Web.Security.MembershipPasswordFormat.Clear:
					case System.Web.Security.MembershipPasswordFormat.Encrypted:
					case System.Web.Security.MembershipPasswordFormat.Hashed:
						break;
					default:
						throw new System.Configuration.ConfigurationErrorsException( "An unknown or unsupported passwordFormat attribute value was specified." );
				}
				myPasswordFormat = configPasswordFormat;
			}
			config.Remove( "passwordFormat" );
			if ( this.EnablePasswordRetrieval && ( System.Web.Security.MembershipPasswordFormat.Hashed == this.PasswordFormat ) ) {
				throw new System.Configuration.ConfigurationErrorsException( "This provider cannot retrieve hashed passwords." );
			}

			configValue = config[ "tokenLifetime" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				if ( System.Int32.TryParse( configValue, out configInt ) ) {
					if ( configInt < 1 ) {
						throw new System.Configuration.ConfigurationErrorsException( "The TokenLifetime must be positive." );
					}
					myTokenLifetime = configInt;
				}
			}
			config.Remove( "tokenLifetime" );

			configValue = config[ "iv" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) {
				myIV = System.Convert.FromBase64String( configValue );
			}
			config.Remove( "iv" );
			if (
				( this.PasswordFormat == System.Web.Security.MembershipPasswordFormat.Encrypted )
				&& ( ( null == this.IV ) || ( 0 == this.IV.Length ) )
			) {
				throw new System.Configuration.ConfigurationErrorsException( "This provider cannot encrypt or decrypt passwords without an initialization vector." );
			}
		}
		#endregion ProviderBase

		#region MembershipProvider
		public sealed override System.Boolean ChangePassword( System.String username, System.String oldPassword, System.String newPassword ) {
			newPassword = newPassword.CheckPasswdParameter( this, "newPassword" );
			oldPassword = oldPassword.TrimToNull();
			username = username.CheckParameter( true, true, true, "username" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.ChangePassword( cnxn, txn, username, oldPassword, newPassword );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.Boolean ChangePasswordQuestionAndAnswer( System.String username, System.String password, System.String newPasswordQuestion, System.String newPasswordAnswer ) {
			newPasswordAnswer = newPasswordAnswer.TrimToNull();
			newPasswordQuestion = newPasswordQuestion.TrimToNull();
			if ( this.RequiresQuestionAndAnswer ) {
				newPasswordAnswer = newPasswordAnswer.CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, "newPasswordAnswer" );
				newPasswordQuestion = newPasswordQuestion.CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, "newPasswordQuestion" );
			}
			password = password.TrimToNull() ?? System.String.Empty;
			username = username.CheckParameter( true, true, true, "username" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.ChangePasswordQuestionAndAnswer( cnxn, txn, username, password, newPasswordQuestion, newPasswordAnswer );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.Web.Security.MembershipUser CreateUser( System.String username, System.String password, System.String email, System.String passwordQuestion, System.String passwordAnswer, System.Boolean isApproved, System.Object providerUserKey, out System.Web.Security.MembershipCreateStatus status ) {
			status = System.Web.Security.MembershipCreateStatus.Success;
			this.CheckCreateUserParameters( false, ref username, ref password, ref email, ref passwordQuestion, ref passwordAnswer, ref providerUserKey, out status );
			if ( System.Web.Security.MembershipCreateStatus.Success != status ) {
				return null;
			}

			System.Web.Security.MembershipUser output = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						if ( null != this.GetUser( cnxn, txn, username, false ) ) {
							status = System.Web.Security.MembershipCreateStatus.DuplicateUserName;
							txn.Rollback();
						} else {
							output = this.CreateUser( cnxn, txn, username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status );
							if (
								( ( System.Web.Security.MembershipCreateStatus.Success != status ) && ( null != output ) )
								|| ( ( System.Web.Security.MembershipCreateStatus.Success == status ) && ( null == output ) )
							) {
								throw new System.Configuration.Provider.ProviderException( "There was a very serious problem with the database server." );
							}
							txn.Commit();
						}
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
				cnxn.Close();
			}
			return output;
		}
		protected virtual void CheckCreateUserParameters( System.Boolean throwOnError, ref System.String username, ref System.String password, ref System.String email, ref System.String passwordQuestion, ref System.String passwordAnswer, ref System.Object providerUserKey, out System.Web.Security.MembershipCreateStatus status ) {
			status = System.Web.Security.MembershipCreateStatus.Success;
			try {
				username = username.CheckParameter( true, true, true, "username" );
			} catch ( System.Exception ) {
				status = System.Web.Security.MembershipCreateStatus.InvalidUserName;
				if ( throwOnError ) {
					throw new System.Web.Security.MembershipCreateUserException( status );
				}
			}
			try {
				password = password.CheckPasswdParameter( this, "password" );
			} catch ( System.Exception ) {
				status = System.Web.Security.MembershipCreateStatus.InvalidPassword;
				if ( throwOnError ) {
					throw new System.Web.Security.MembershipCreateUserException( status );
				}
			}
			if ( this.RequiresQuestionAndAnswer ) {
				try {
					passwordQuestion = passwordQuestion.CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, "passwordQuestion" );
				} catch ( System.Exception ) {
					status = System.Web.Security.MembershipCreateStatus.InvalidQuestion;
					if ( throwOnError ) {
						throw new System.Web.Security.MembershipCreateUserException( status );
					}
				}
				try {
					passwordAnswer = passwordAnswer.CheckParameter( false, this.RequiresQuestionAndAnswer, this.RequiresQuestionAndAnswer, "passwordAnswer" );
				} catch ( System.Exception ) {
					status = System.Web.Security.MembershipCreateStatus.InvalidAnswer;
					if ( throwOnError ) {
						throw new System.Web.Security.MembershipCreateUserException( status );
					}
				}
			} else {
				passwordAnswer = passwordAnswer.TrimToNull();
				passwordQuestion = passwordQuestion.TrimToNull();
			}
			try {
				email = email.CheckParameter( false, this.RequiresUniqueEmail, this.RequiresUniqueEmail, "email" );
				email = new System.Net.Mail.MailAddress( email ).Address;
			} catch ( System.Exception ) {
				status = System.Web.Security.MembershipCreateStatus.InvalidEmail;
				if ( throwOnError ) {
					throw new System.Web.Security.MembershipCreateUserException( status );
				}
			}
			if ( null != providerUserKey ) {
				status = System.Web.Security.MembershipCreateStatus.InvalidProviderUserKey;
				if ( throwOnError ) {
					throw new System.Web.Security.MembershipCreateUserException( status );
				}
			}
		}
		public sealed override System.Boolean DeleteUser( System.String username, System.Boolean deleteAllRelatedData ) {
			username = username.CheckParameter( true, true, true, "username" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.DeleteUser( cnxn, txn, username, deleteAllRelatedData );
						if ( deleteAllRelatedData ) {
							try {
								if ( System.Web.Security.Roles.Enabled ) {
									System.Web.Security.Roles.RemoveUserFromRoles( username, System.Web.Security.Roles.GetRolesForUser( username ) );
								}
							} catch ( System.Exception ) {
								;
							}
							try {
								if ( System.Web.Profile.ProfileManager.Enabled ) {
									System.Web.Profile.ProfileManager.DeleteProfile( username );
								}
							} catch ( System.Exception ) {
								;
							}
							try {
								if ( Oits.Web.Security.Rights.Enabled ) {
									Oits.Web.Security.Rights.ClearUserRights( username, Oits.Web.Security.Rights.GetGrantedRightsForUser( username ).Select(
										x => x.RightName
									) );
									Oits.Web.Security.Rights.ClearUserRights( username, Oits.Web.Security.Rights.GetDeniedRightsForUser( username ).Select(
										x => x.RightName
									) );
								}
							} catch ( System.Exception ) {
								;
							}
						}
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.Web.Security.MembershipUserCollection FindUsersByEmail( System.String emailToMatch, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) {
			if ( pageSize < 1 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be positive." );
			} else if ( pageIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be non-negative." );
			} else {
				System.Int64 test = pageIndex * pageSize;
				if ( System.Int32.MaxValue <= test ) {
					throw new System.ArgumentException( "The product of pageIndex and pageSize must be less than the maximum Int32 value.", "pageIndex and pageSize" );
				}
			}

			System.Collections.Generic.IList<System.Web.Security.MembershipUser> list = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				list = this.FindUsersByEmail( cnxn, null, emailToMatch );
			}
			totalRecords = list.Count;
			System.Web.Security.MembershipUserCollection output = new System.Web.Security.MembershipUserCollection();
			System.Int32 start = pageSize * pageIndex;
			System.Int32 stop = System.Math.Min( totalRecords, start + pageSize );
			for ( System.Int32 i = start; i < stop; i++ ) {
				output.Add( list[ i ] );
			}
			return output;
		}
		public sealed override System.Web.Security.MembershipUserCollection FindUsersByName( System.String usernameToMatch, System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) {
			if ( pageSize < 1 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be positive." );
			} else if ( pageIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be non-negative." );
			} else {
				System.Int64 test = pageIndex * pageSize;
				if ( System.Int32.MaxValue <= test ) {
					throw new System.ArgumentException( "The product of pageIndex and pageSize must be less than the maximum Int32 value.", "pageIndex and pageSize" );
				}
			}

			System.Collections.Generic.IList<System.Web.Security.MembershipUser> list = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				list = this.FindUsersByName( cnxn, null, usernameToMatch );
			}
			totalRecords = list.Count;
			System.Web.Security.MembershipUserCollection output = new System.Web.Security.MembershipUserCollection();
			System.Int32 start = pageSize * pageIndex;
			System.Int32 stop = System.Math.Min( totalRecords, start + pageSize );
			for ( System.Int32 i = start; i < stop; i++ ) {
				output.Add( list[ i ] );
			}
			return output;
		}
		public sealed override System.Web.Security.MembershipUserCollection GetAllUsers( System.Int32 pageIndex, System.Int32 pageSize, out System.Int32 totalRecords ) {
			if ( pageSize < 1 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be positive." );
			} else if ( pageIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "pageSize", "Value must be non-negative." );
			} else {
				System.Int64 test = pageIndex * pageSize;
				if ( System.Int32.MaxValue <= test ) {
					throw new System.ArgumentException( "The product of pageIndex and pageSize must be less than the maximum Int32 value.", "pageIndex and pageSize" );
				}
			}

			System.Collections.Generic.IList<System.Web.Security.MembershipUser> list = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				list = this.GetAllUsers( cnxn, null );
			}
			totalRecords = list.Count;
			System.Web.Security.MembershipUserCollection output = new System.Web.Security.MembershipUserCollection();
			System.Int32 start = pageSize * pageIndex;
			System.Int32 stop = System.Math.Min( totalRecords, start + pageSize );
			for ( System.Int32 i = start; i < stop; i++ ) {
				output.Add( list[ i ] );
			}
			return output;
		}
		public sealed override System.Int32 GetNumberOfUsersOnline() {
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetNumberOfUsersOnline( cnxn, null );
			}
		}
		public sealed override System.String GetPassword( System.String username, System.String answer ) {
			answer = answer.TrimToNull();
			username = username.CheckParameter( true, true, true, "username" );

			if ( !this.EnablePasswordRetrieval ) {
				throw new System.NotSupportedException();
			}

			System.String output = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.GetPassword( cnxn, txn, username, answer );
						txn.Commit();
					} catch {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.Web.Security.MembershipUser GetUser( System.Object providerUserKey, System.Boolean userIsOnline ) {
			if ( null == providerUserKey ) {
				throw new System.ArgumentNullException( "providerUserKey" );
			}

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetUser( cnxn, null, providerUserKey, userIsOnline );
			}
		}
		public sealed override System.Web.Security.MembershipUser GetUser( System.String username, System.Boolean userIsOnline ) {
			username = username.CheckParameter( true, true, true, "username" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetUser( cnxn, null, username, userIsOnline );
			}
		}
		public sealed override System.String GetUserNameByEmail( System.String email ) {
			try {
				email = email.TrimToNull();
				if ( System.String.IsNullOrEmpty( email ) ) {
					throw new System.ArgumentNullException( "email" );
				}
				try {
					email = new System.Net.Mail.MailAddress( email ).Address.TrimToNull();
				} catch {
					;
				}
				if ( System.String.IsNullOrEmpty( email ) ) {
					throw new System.ArgumentNullException( "email" );
				}

				using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
					return this.GetUserNameByEmail( cnxn, null, email );
				}
			} catch ( System.Exception e ) {
				throw new System.Configuration.Provider.ProviderException( e.Message, e );
			}
		}
		public sealed override System.String ResetPassword( System.String username, System.String answer ) {
			answer = answer.CheckParameter( false, true, true, "answer" );
			username = username.CheckParameter( true, true, true, "username" );
			if ( !this.EnablePasswordReset ) {
				throw new System.NotSupportedException();
			}

			System.String output = null;
			var map = this.DbMapGroup;
			using ( var connection = map.CreateConnection() ) {
				connection.Open();
				using ( var txn = connection.BeginTransaction() ) {
					try {
						output = this.ResetPassword( connection, txn, username, answer );
						if ( System.String.IsNullOrEmpty( output.TrimToNull() ) ) {
							txn.Rollback();
						} else {
							txn.Commit();
						}
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.Boolean UnlockUser( System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.UnlockUser( cnxn, txn, username );
						txn.Commit();
					} catch {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override void UpdateUser( System.Web.Security.MembershipUser user ) {
			if ( null == user ) {
				throw new System.ArgumentNullException( "user" );
			} else if ( System.String.IsNullOrEmpty( user.UserName.TrimToNull() ) ) {
				throw new System.ArgumentException( "The specified user has an invalid username.", "user" );
			}

			var map = this.DbMapGroup;
			using ( var cnxn = map.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.UpdateMembershipUser( cnxn, txn, user );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public sealed override System.Boolean ValidateUser( System.String username, System.String password ) {
			password = password.TrimToNull();
			username = username.CheckParameter( true, true, true, "username" );
			return this.ValidateUser( username, password, true, true, this.RequireConfirmation );
		}
		#endregion MembershipProvider

		#region ExtendedMembershipProvider
		public override System.Boolean ConfirmAccount( System.String accountConfirmationToken ) {
			accountConfirmationToken = accountConfirmationToken.TrimToNull();
			if ( System.String.IsNullOrEmpty( accountConfirmationToken ) ) {
				throw new System.ArgumentNullException( "accountConfirmationToken" );
			}

			return this.ConfirmAccount( null, accountConfirmationToken );
		}
		public override System.Boolean ConfirmAccount( System.String userName, System.String accountConfirmationToken ) {
			accountConfirmationToken = accountConfirmationToken.TrimToNull();
			if ( System.String.IsNullOrEmpty( accountConfirmationToken ) ) {
				throw new System.ArgumentNullException( "accountConfirmationToken" );
			}
			userName = userName.TrimToNull().CheckParameter( true, false, false, "userName" );

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.ConfirmAccount( cnxn, txn, userName, accountConfirmationToken );
						txn.Commit();
					} catch {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public sealed override System.String CreateAccount( System.String userName, System.String password ) {
			return this.CreateAccount( userName, password, this.RequireConfirmation );
		}
		public override System.String CreateAccount( System.String userName, System.String password, System.Boolean requireConfirmationToken ) {
			password = password.CheckPasswdParameter( this, "password" );
			userName = userName.CheckParameter( true, true, true, "userName" );

			return this.CreateUserAndAccount( userName, password, requireConfirmationToken, null );
		}
		public override void CreateOrUpdateOAuthAccount( System.String provider, System.String providerUserId, System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.CreateOrUpdateOAuthAccount( cnxn, null, provider, providerUserId, userName );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public override void DeleteOAuthAccount( System.String provider, System.String providerUserId ) {
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.DeleteOAuthAccount( cnxn, txn, provider, providerUserId );
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public override void DeleteOAuthToken( System.String token ) {
			token = token.TrimToNull().CheckParameter( false, true, true, "token" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.DeleteOAuthToken( cnxn, txn, token );
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password ) {
			password = password.CheckPasswdParameter( this, "password" );
			userName = userName.CheckParameter( true, true, true, "userName" );

			return this.CreateUserAndAccount( userName, password, this.RequireConfirmation );
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Boolean requireConfirmation ) {
			password = password.CheckPasswdParameter( this, "password" );
			userName = userName.CheckParameter( true, true, true, "userName" );

			return this.CreateUserAndAccount( userName, password, requireConfirmation, null );
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Collections.Generic.IDictionary<System.String, System.Object> values ) {
			password = password.CheckPasswdParameter( this, "password" );
			userName = userName.CheckParameter( true, true, true, "userName" );

			return this.CreateUserAndAccount( userName, password, this.RequireConfirmation, values );
		}
		public override System.String CreateUserAndAccount( System.String userName, System.String password, System.Boolean requireConfirmation, System.Collections.Generic.IDictionary<System.String, System.Object> values ) {
			password = password.CheckPasswdParameter( this, "password" );
			userName = userName.CheckParameter( true, true, true, "userName" );

			System.String email = null;
			System.String passwordQuestion = null;
			System.String passwordAnswer = null;

			if ( null != values ) {
				if ( values.ContainsKey( "email" ) ) {
					email = System.Convert.ToString( values[ "email" ] ).TrimToNull();
				}
				if ( values.ContainsKey( "passwordQuestion" ) ) {
					passwordQuestion = System.Convert.ToString( values[ "passwordQuestion" ] ).TrimToNull();
				}
				if ( values.ContainsKey( "passwordAnswer" ) ) {
					passwordAnswer = System.Convert.ToString( values[ "passwordAnswer" ] ).TrimToNull();
				}
			}
			if ( !System.String.IsNullOrEmpty( passwordAnswer ) ) {
				passwordAnswer = passwordAnswer.ToLower();
			}

			System.Object providerUserKey = null;
			System.Web.Security.MembershipCreateStatus status = System.Web.Security.MembershipCreateStatus.UserRejected;
			this.CheckCreateUserParameters( true, ref userName, ref password, ref email, ref passwordQuestion, ref passwordAnswer, ref providerUserKey, out status );
			System.String token = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						var user = this.CreateUser( cnxn, txn, userName, password, email, passwordQuestion, passwordAnswer, !requireConfirmation, null, out status );
						if ( System.Web.Security.MembershipCreateStatus.Success != status ) {
							throw new System.Web.Security.MembershipCreateUserException( status );
						} else if ( null == user ) {
							throw new System.Configuration.Provider.ProviderException( new System.Web.Security.MembershipCreateUserException( System.Web.Security.MembershipCreateStatus.ProviderError ).Message );
						}
						if ( requireConfirmation ) {
							token = this.GenerateToken();
							this.SetConfirmationToken( cnxn, txn, userName, token );
						}
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}

			if ( ( null != values ) && ( System.Web.Profile.ProfileManager.Enabled ) ) {
				try {
					var profile = System.Web.Profile.ProfileBase.Create( userName, true );
					foreach ( var kvp in values ) {
						try {
							profile[ kvp.Key ] = kvp.Value;
						} catch ( System.Exception ) {
							;
						}
					}
					profile.Save();
				} catch ( System.Exception ) {
					;
				}
			}

			return token;
		}
		public override System.Boolean DeleteAccount( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			return this.DeleteUser( userName, false );
		}
		public sealed override System.String GeneratePasswordResetToken( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );
			return this.GeneratePasswordResetToken( userName, this.TokenLifetime );
		}
		public override System.String GeneratePasswordResetToken( System.String userName, System.Int32 tokenExpirationInMinutesFromNow ) {
			if ( tokenExpirationInMinutesFromNow <= 0 ) {
				throw new System.ArgumentException( "Value must be positive.", "tokenExpirationInMinutesFromNow" );
			}
			userName = userName.CheckParameter( true, true, true, "userName" );

			var token = this.GenerateToken();
			System.String output = null;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						if ( this.SetPasswordResetToken( cnxn, txn, userName, token ) ) {
							output = token;
						}
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public override System.Collections.Generic.ICollection<WebMatrix.WebData.OAuthAccountData> GetAccountsForUser( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetAccountsForUser( cnxn, null, userName );
			}
		}
		public override System.DateTime GetCreateDate( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetCreateDate( cnxn, null, userName );
			}
		}
		public override System.DateTime GetLastPasswordFailureDate( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetLastPasswordFailureDate( cnxn, null, userName );
			}
		}
		public override System.String GetOAuthTokenSecret( System.String token ) {
			token = token.TrimToNull().CheckParameter( false, true, true, "token" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetOAuthTokenSecret( cnxn, null, token );
			}
		}
		public override System.DateTime GetPasswordChangedDate( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetPasswordChangedDate( cnxn, null, userName );
			}
		}
		public override System.Int32 GetPasswordFailuresSinceLastSuccess( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetPasswordFailuresSinceLastSuccess( cnxn, null, userName );
			}
		}
		public override System.Int32 GetUserIdFromPasswordResetToken( System.String token ) {
			token = token.TrimToNull();
			if ( System.String.IsNullOrEmpty( token ) ) {
				throw new System.ArgumentNullException( "token" );
			}

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetUserIdFromPasswordResetToken( cnxn, null, token );
			}
		}
		public override System.Int32 GetUserIdFromOAuth( System.String provider, System.String providerUserId ) {
			providerUserId = providerUserId.TrimToNull().CheckParameter( false, true, true, "providerUserId" );
			provider = provider.TrimToNull().CheckParameter( false, true, true, "provider" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.GetUserIdFromOAuth( cnxn, null, provider, providerUserId );
			}
		}
		public override System.String GetUserNameFromId( System.Int32 userId ) {
			var user = this.GetUser( (System.Object)userId, false );
			if ( null == user ) {
				return null;
			} else {
				return user.UserName;
			}
		}
		public override System.Boolean HasLocalAccount( System.Int32 userId ) {
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.HasLocalAccount( cnxn, null, userId );
			}
		}
		public override System.Boolean IsConfirmed( System.String userName ) {
			userName = userName.CheckParameter( true, true, true, "userName" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				return this.IsConfirmed( cnxn, null, userName );
			}
		}
		public override void ReplaceOAuthRequestTokenWithAccessToken( System.String requestToken, System.String accessToken, System.String accessTokenSecret ) {
			accessTokenSecret = accessTokenSecret.TrimToNull().CheckParameter( false, true, true, "accessTokenSecret" );
			accessToken = accessToken.TrimToNull().CheckParameter( false, true, true, "accessToken" );
			requestToken = requestToken.TrimToNull().CheckParameter( false, true, true, "requestToken" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.ReplaceOAuthRequestTokenWithAccessToken( cnxn, txn, requestToken, accessToken, accessTokenSecret );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		public override System.Boolean ResetPasswordWithToken( System.String token, System.String newPassword ) {
			newPassword = newPassword.CheckPasswdParameter( this, "newPassword" );
			token = token.TrimToNull();
			if ( System.String.IsNullOrEmpty( token ) ) {
				throw new System.ArgumentNullException( "token" );
			}

			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.ResetPasswordWithToken( cnxn, txn, token, newPassword );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		public override void StoreOAuthRequestToken( System.String requestToken, System.String requestTokenSecret ) {
			requestTokenSecret = requestTokenSecret.TrimToNull().CheckParameter( false, true, true, "requestTokenSecret" );
			requestToken = requestTokenSecret.TrimToNull().CheckParameter( false, true, true, "requestToken" );

			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						this.StoreOAuthRequestToken( cnxn, txn, requestToken, requestTokenSecret );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
		}
		#endregion ExtendedMembershipProvider

		#region utilities
		protected virtual System.String GenerateSalt() {
			using ( var gen = new System.Security.Cryptography.RNGCryptoServiceProvider() ) {
				return this.GenerateSalt( gen );
			}
		}
		protected System.String GenerateSalt( System.Security.Cryptography.RandomNumberGenerator generator ) {
			var salt = new System.Byte[ this.SaltSize ];
			generator.GetBytes( salt );
			return System.Convert.ToBase64String( salt );
		}

		protected virtual System.String GenerateToken() {
			using ( var gen = new System.Security.Cryptography.RNGCryptoServiceProvider() ) {
				return this.GenerateToken( gen );
			}
		}
		protected System.String GenerateToken( System.Security.Cryptography.RandomNumberGenerator generator ) {
			var token = new System.Byte[ this.TokenLength ];
			generator.GetBytes( token );
			return System.Web.HttpServerUtility.UrlTokenEncode( token );
		}

		protected virtual System.String GetEncodedPassword( System.String password, UserPasswordOption option ) {
			if ( null == option ) {
				throw new System.ArgumentNullException( "option" );
			}
			password = password.TrimToNull() ?? System.String.Empty;

			return this.GetEncodedString( password, option.Format, option.Algorithm, option.Salt );
		}
		protected virtual System.String GetEncodedString( System.String value, System.Web.Security.MembershipPasswordFormat format, System.String algorithm, System.String salt ) {

			System.String output = null;
			switch ( format ) {
				case System.Web.Security.MembershipPasswordFormat.Clear:
					output = value;
					break;
				case System.Web.Security.MembershipPasswordFormat.Hashed:
					output = this.GetHasedString( value ?? System.String.Empty, format, algorithm, salt );
					break;
				case System.Web.Security.MembershipPasswordFormat.Encrypted:
					output = this.GetEncryptedString( value ?? System.String.Empty, format, algorithm, salt );
					break;
				default:
					throw new System.NotSupportedException();
			}
			return output;
		}

		protected virtual System.String GetHasedPassword( System.String password, UserPasswordOption option ) {
			if ( null == option ) {
				throw new System.ArgumentNullException( "option" );
			} else if ( System.Web.Security.MembershipPasswordFormat.Hashed != option.Format ) {
				throw new System.InvalidOperationException();
			}
			password = password.CheckPasswdParameter( this, "password" );

			return this.GetHasedString( password, option.Format, option.Algorithm, option.Salt );
		}
		protected virtual System.String GetHasedString( System.String value, System.Web.Security.MembershipPasswordFormat format, System.String algorithm, System.String salt ) {
			if ( System.Web.Security.MembershipPasswordFormat.Hashed != format ) {
				throw new System.InvalidOperationException();
			}

			var key = System.Convert.FromBase64String( salt );
			var passwd = System.Text.Encoding.Unicode.GetBytes( value );
			using ( var hasher = this.InitializeHasher( algorithm, salt ) ) {
				System.Byte[] redText = null;
				if ( hasher is System.Security.Cryptography.KeyedHashAlgorithm ) {
					redText = passwd;
				} else {
					var buffer = new System.Byte[ key.Length + passwd.Length ];
					System.Buffer.BlockCopy( passwd, 0, buffer, 0, passwd.Length );
					System.Buffer.BlockCopy( key, 0, buffer, passwd.Length, key.Length );
					redText = buffer;
				}
				var blackText = System.Convert.ToBase64String( hasher.ComputeHash( redText ) );
				return blackText;
			}
		}
		protected virtual System.Security.Cryptography.HashAlgorithm InitializeHasher( System.String algorithm, System.String salt ) {
			var key = System.Convert.FromBase64String( salt );
			var hasher = System.Security.Cryptography.HashAlgorithm.Create( algorithm );
			if ( hasher is System.Security.Cryptography.KeyedHashAlgorithm ) {
				var kHasher = (System.Security.Cryptography.KeyedHashAlgorithm)hasher;
				if ( kHasher.Key.Length == key.Length ) {
					kHasher.Key = key;
				} else if ( kHasher.Key.Length < key.Length ) {
					var buffer = new System.Byte[ kHasher.Key.Length ];
					System.Buffer.BlockCopy( key, 0, buffer, 0, buffer.Length );
					kHasher.Key = buffer;
				} else {
					System.Int32 n = 0;
					var buffer = new System.Byte[ kHasher.Key.Length ];
					for ( System.Int32 i = 0; i < buffer.Length; i += n ) {
						n = System.Math.Min( key.Length, buffer.Length - i );
						System.Buffer.BlockCopy( key, 0, buffer, i, n );
					}
					kHasher.Key = buffer;
				}
			}

			return hasher;
		}

		protected virtual System.String GetEncryptedPassword( System.String password, UserPasswordOption option ) {
			if ( null == option ) {
				throw new System.ArgumentNullException( "option" );
			} else if ( System.Web.Security.MembershipPasswordFormat.Encrypted != option.Format ) {
				throw new System.InvalidOperationException();
			}
			password = password.CheckPasswdParameter( this, "password" );

			return this.GetEncryptedString( password, option.Format, option.Algorithm, option.Salt );
		}
		protected virtual System.String GetEncryptedString( System.String value, System.Web.Security.MembershipPasswordFormat format, System.String algorithm, System.String salt ) {
			if ( System.Web.Security.MembershipPasswordFormat.Encrypted != format ) {
				throw new System.InvalidOperationException();
			}

			using ( var cipher = this.InitializeCipher( algorithm, salt ) ) {
				using ( var writer = new System.IO.MemoryStream() ) {
					using ( var stream = new System.Security.Cryptography.CryptoStream( writer, cipher.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write ) ) {
						var redText = System.Text.Encoding.Unicode.GetBytes( value );
						stream.Write( redText, 0, redText.Length );
						if ( !stream.HasFlushedFinalBlock ) {
							stream.FlushFinalBlock();
						}
						stream.Flush();
						writer.Seek( 0, System.IO.SeekOrigin.Begin );
						var blackText = System.Convert.ToBase64String( writer.ToArray() );
						return blackText;
					}
				}
			}
		}
		protected virtual System.String GetDecryptedString( System.String value, System.Web.Security.MembershipPasswordFormat format, System.String algorithm, System.String salt ) {
			if ( System.Web.Security.MembershipPasswordFormat.Encrypted != format ) {
				throw new System.InvalidOperationException();
			}

			using ( var cipher = this.InitializeCipher( algorithm, salt ) ) {
				using ( var writer = new System.IO.MemoryStream() ) {
					using ( var stream = new System.Security.Cryptography.CryptoStream( writer, cipher.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write ) ) {
						var blackText = System.Convert.FromBase64String( value );
						stream.Write( blackText, 0, blackText.Length );
					}
					writer.Seek( 0, System.IO.SeekOrigin.Begin );
					return System.Text.Encoding.Unicode.GetString( writer.ToArray() );
				}
			}
		}
		protected virtual System.Security.Cryptography.SymmetricAlgorithm InitializeCipher( System.String algorithm, System.String salt ) {
			var iv = this.IV;
			if ( ( null == iv ) || ( 0 == iv.Length ) ) {
				throw new System.Configuration.ConfigurationErrorsException( "This provider cannot encrypt or decrypt passwords without an initialization vector." );
			}
			var key = System.Convert.FromBase64String( salt );

			var cipher = System.Security.Cryptography.SymmetricAlgorithm.Create( algorithm );
			if ( cipher.Key.Length == key.Length ) {
				cipher.Key = key;
			} else if ( cipher.Key.Length < key.Length ) {
				var buffer = new System.Byte[ cipher.Key.Length ];
				System.Buffer.BlockCopy( key, 0, buffer, 0, buffer.Length );
				cipher.Key = buffer;
			} else {
				System.Int32 n = 0;
				var buffer = new System.Byte[ cipher.Key.Length ];
				for ( System.Int32 i = 0; i < buffer.Length; i += n ) {
					n = System.Math.Min( key.Length, buffer.Length - i );
					System.Buffer.BlockCopy( key, 0, buffer, i, n );
				}
				cipher.Key = buffer;
			}

			if ( cipher.IV.Length == iv.Length ) {
				cipher.IV = iv;
			} else if ( cipher.IV.Length < iv.Length ) {
				var buffer = new System.Byte[ cipher.IV.Length ];
				System.Buffer.BlockCopy( iv, 0, buffer, 0, buffer.Length );
				cipher.IV = buffer;
			} else {
				System.Int32 n = 0;
				var buffer = new System.Byte[ cipher.IV.Length ];
				for ( System.Int32 i = 0; i < buffer.Length; i += n ) {
					n = System.Math.Min( iv.Length, buffer.Length - i );
					System.Buffer.BlockCopy( iv, 0, buffer, i, n );
				}
				cipher.IV = buffer;
			}

			return cipher;
		}

		protected virtual void CheckReturnValue( System.Int32 returnValue ) {
			var v = (System.Web.Security.MembershipCreateStatus)returnValue;
			switch ( v ) {
				case System.Web.Security.MembershipCreateStatus.Success:
					break;
				case System.Web.Security.MembershipCreateStatus.DuplicateEmail:
					throw new System.Configuration.Provider.ProviderException( "The specified email already exists." );
				case System.Web.Security.MembershipCreateStatus.DuplicateProviderUserKey:
					throw new System.Configuration.Provider.ProviderException( "The specified providerUserKey already exists." );
				case System.Web.Security.MembershipCreateStatus.DuplicateUserName:
					throw new System.Configuration.Provider.ProviderException( "The specified username already exists." );
				case System.Web.Security.MembershipCreateStatus.InvalidAnswer:
					throw new System.Configuration.Provider.ProviderException( "The specified answer is invalid." );
				case System.Web.Security.MembershipCreateStatus.InvalidEmail:
					throw new System.Configuration.Provider.ProviderException( "The specified email is invalid." );
				case System.Web.Security.MembershipCreateStatus.InvalidPassword:
					throw new System.Configuration.Provider.ProviderException( "The specified password is invalid." );
				case System.Web.Security.MembershipCreateStatus.InvalidProviderUserKey:
					throw new System.Configuration.Provider.ProviderException( "The specified providerUserKey is invalid." );
				case System.Web.Security.MembershipCreateStatus.InvalidQuestion:
					throw new System.Configuration.Provider.ProviderException( "The specified question is invalid." );
				case System.Web.Security.MembershipCreateStatus.InvalidUserName:
					throw new System.Configuration.Provider.ProviderException( "The specified username is invalid." );
				case System.Web.Security.MembershipCreateStatus.UserRejected:
					throw new System.Configuration.Provider.ProviderException( "Invalid username or password is invalid." );
				case System.Web.Security.MembershipCreateStatus.ProviderError:
				default:
					throw new System.Configuration.Provider.ProviderException( "An unknown and serious error has occurred within the database." );
			}
		}
		#endregion utilities

		#region ExtendedMembershipProvider extentions
		protected abstract System.Boolean ConfirmAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String accountConfirmationToken );
		protected abstract void SetConfirmationToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String token );
		protected abstract void CreateOrUpdateOAuthAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId, System.String userName );
		protected abstract void DeleteOAuthAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId );
		protected abstract void DeleteOAuthToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token );
		protected abstract System.Collections.Generic.ICollection<WebMatrix.WebData.OAuthAccountData> GetAccountsForUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract WebMatrix.WebData.OAuthAccountData ReadOathAccountData( System.Data.Common.DbDataReader reader, Oits.Configuration.DbMap.DbResultElement resultConfig );
		protected abstract System.Boolean SetPasswordResetToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String token );
		protected virtual System.DateTime GetCreateDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var user = this.GetUser( connection, transaction, username, false );
			if ( null == user ) {
				throw new System.Configuration.Provider.ProviderException();
			}
			return user.CreationDate;
		}
		protected abstract System.DateTime GetLastPasswordFailureDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract System.String GetOAuthTokenSecret( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token );
		protected virtual System.DateTime GetPasswordChangedDate( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username ) {
			username = username.CheckParameter( true, true, true, "username" );
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var user = this.GetUser( connection, transaction, username, false );
			if ( null == user ) {
				throw new System.Configuration.Provider.ProviderException();
			}
			return user.LastPasswordChangedDate;
		}
		protected abstract System.Int32 GetPasswordFailuresSinceLastSuccess( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract System.Int32 GetUserIdFromOAuth( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String provider, System.String providerUserId );
		protected abstract System.String GetUserNameFromId( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Int32 userId );
		protected abstract System.Int32 GetUserIdFromPasswordResetToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token );
		protected abstract System.Boolean HasLocalAccount( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Int32 userId );
		protected abstract System.Boolean IsConfirmed( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract void ReplaceOAuthRequestTokenWithAccessToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String requestToken, System.String accessToken, System.String accessTokenSecret );
		protected abstract System.Boolean ResetPasswordWithToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String token, System.String newPassword );
		protected abstract void StoreOAuthRequestToken( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String requestToken, System.String requestTokenSecret );
		#endregion ExtendedMembershipProvider extentions

		#region MembershipProvider extentions
		protected abstract System.Boolean ChangePassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String oldPassword, System.String newPassword );
		protected abstract System.Boolean ChangePasswordQuestionAndAnswer( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.String newPasswordQuestion, System.String newPasswordAnswer );
		protected abstract System.Web.Security.MembershipUser CreateUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.String email, System.String passwordQuestion, System.String passwordAnswer, System.Boolean isApproved, System.Object providerUserKey, out System.Web.Security.MembershipCreateStatus status );
		protected abstract System.Boolean DeleteUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.Boolean deleteAllRelatedData );
		protected abstract System.Collections.Generic.IList<System.Web.Security.MembershipUser> FindUsersByEmail( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String emailToMatch );
		protected abstract System.Collections.Generic.IList<System.Web.Security.MembershipUser> FindUsersByName( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String usernameToMatch );
		protected abstract System.Collections.Generic.IList<System.Web.Security.MembershipUser> GetAllUsers( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction );
		protected virtual System.Int32 GetNumberOfUsersOnline( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var cutoff = System.DateTime.Now - new System.TimeSpan( 0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0 );
			return this.GetAllUsers( connection, transaction ).Where<System.Web.Security.MembershipUser>(
				x => ( cutoff <= x.LastLoginDate ) || ( cutoff <= x.LastActivityDate )
			).Count();
		}
		protected abstract System.String GetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer );
		protected abstract System.Web.Security.MembershipUser GetUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object providerUserKey, System.Boolean updateLastActive );
		protected abstract System.Web.Security.MembershipUser GetUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.Boolean updateLastActive );
		protected abstract System.Web.Security.MembershipUser ReadMembershipUser( System.Data.Common.DbDataReader reader, Oits.Configuration.DbMap.DbResultElement resultConfig );
		protected virtual System.String GetUserNameByEmail( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String email ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			System.String output = null;

			email = email.TrimToNull();
			var user = this.GetAllUsers( connection, transaction ).Where<System.Web.Security.MembershipUser>(
				x => System.String.Equals( email, x.Email, System.StringComparison.OrdinalIgnoreCase )
			).FirstOrDefault<System.Web.Security.MembershipUser>();
			if ( null != user ) {
				output = user.UserName;
			}

			return output;
		}
		protected abstract System.String ResetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer );
		protected abstract System.Boolean SafeSetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password );
		protected abstract System.Boolean SetPassword( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password );
		protected abstract System.Boolean ValidatePasswordAnswer( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String answer, System.Boolean failifNotApproved );
		protected abstract System.Boolean UnlockUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		protected abstract void UpdateMembershipUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Web.Security.MembershipUser user );
		protected virtual System.Boolean ValidateUser( System.String username, System.String password, System.Boolean updateLastLogin, System.Boolean updateLastActive, System.Boolean failIfNotApproved ) {
			password = password.TrimToNull() ?? System.String.Empty;
			username = username.CheckParameter( true, true, true, "username" );
			System.Boolean output = false;
			using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
				cnxn.Open();
				using ( var txn = cnxn.BeginTransaction() ) {
					try {
						output = this.ValidateUser( cnxn, txn, username, password, updateLastLogin, updateLastActive, failIfNotApproved );
						txn.Commit();
					} catch ( System.Exception ) {
						txn.Rollback();
						throw;
					}
				}
			}
			return output;
		}
		protected abstract System.Boolean ValidateUser( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username, System.String password, System.Boolean updateLastLogin, System.Boolean updateLastActive, System.Boolean failIfNotApproved );
		#endregion MembershipProvider extentions

		public virtual void Logout() {
			if ( !WebMatrix.WebData.WebSecurity.IsAuthenticated ) {
				return;
			}

			try {
				var username = System.Web.Security.Membership.GetUser( true ).UserName.CheckParameter( true, true, true, "username" );
				WebMatrix.WebData.WebSecurity.Logout();

				using ( var cnxn = this.DbMapGroup.CreateConnection() ) {
					cnxn.Open();
					using ( var txn = cnxn.BeginTransaction() ) {
						try {
							this.Logout( cnxn, txn, username );
							txn.Commit();
						} catch ( System.Exception ) {
							txn.Rollback();
							throw;
						}
					}
				}
			} catch ( System.Exception ) {
				throw;
			}
		}
		protected abstract void Logout( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.String username );
		#endregion methods

	}

}