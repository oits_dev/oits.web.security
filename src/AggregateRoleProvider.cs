﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Web.Security { 

	public class AggregateRoleProvider : RoleProviderBase { 

		#region fields
		private System.Collections.Generic.List<System.String> myProviderNames = new System.Collections.Generic.List<System.String>();
		private AggregateBehaviour myBehaviour = AggregateBehaviour.Any;
		#endregion fields


		#region .ctor
		public AggregateRoleProvider() : base() { 
		}
		#endregion .ctor


		#region properties
		public System.Collections.Generic.ICollection<System.String> ProviderNames { 
			get { 
				if ( ( null == myProviderNames ) || ( 0 == myProviderNames.Count ) ) { 
					throw new System.Configuration.Provider.ProviderException( "No providers have been defined." );
				}
				return myProviderNames.AsReadOnly();
			}
		}
		protected System.Collections.Generic.IEnumerable<System.Web.Security.RoleProvider> Providers { 
			get { 
				return System.Web.Security.Roles.Providers.OfType<System.Web.Security.RoleProvider>().Where( 
					x => this.ProviderNames.Contains( x.Name ) 
				);
			}
		}
		protected System.Web.Security.RoleProvider FirstProvider { 
			get { 
				return this.Providers.First();
			}
		}
		public AggregateBehaviour Behaviour {
			get {
				if ( !System.Enum.IsDefined( myBehaviour.GetType(), myBehaviour ) ) {
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
				}
				return myBehaviour;
			}
		}
		#endregion properties


		#region methods
		#region ProviderBase
		public override void Initialize( string name, System.Collections.Specialized.NameValueCollection config ) { 
			base.Initialize( name, config );

			System.String configValue;

			configValue = config[ "providerNames" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				myProviderNames = new System.Collections.Generic.List<System.String>( configValue.Split( new System.Char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries ).Select( 
					x => x.TrimToNull() 
				).Where( 
					x => !System.String.IsNullOrEmpty( x ) 
				) );
			}
			config.Remove( "providerNames" );
			myProviderNames.Remove( this.Name );
			if ( 0 == myProviderNames.Count ) { 
				throw new System.Configuration.Provider.ProviderException( "At least one provider must be specified." );
			}

			configValue = config[ "behaviour" ].TrimToNull();
			if ( !System.String.IsNullOrEmpty( configValue ) ) { 
				if ( !System.Enum.IsDefined( typeof( AggregateBehaviour ), configValue ) ) { 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
				}
				myBehaviour = (AggregateBehaviour)System.Enum.Parse( typeof( AggregateBehaviour ), configValue );
			}
		}
		#endregion ProviderBase

		#region RoleProvider
		public override void AddUsersToRoles( System.String[] usernames, System.String[] roleNames ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.Providers.FirstOrDefault();
					if ( null != p ) { 
						p.AddUsersToRoles( usernames, roleNames );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.Providers ) { 
						q.AddUsersToRoles( usernames, roleNames );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override void CreateRole( System.String roleName ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.Providers.FirstOrDefault();
					if ( null != p ) { 
						p.CreateRole( roleName );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.Providers ) { 
						q.CreateRole( roleName );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override System.Boolean DeleteRole( System.String roleName, System.Boolean throwOnPopulatedRole ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.DeleteRole( roleName, throwOnPopulatedRole ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.DeleteRole( roleName, throwOnPopulatedRole ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override System.String[] FindUsersInRole( System.String roleName, System.String usernameToMatch ) { 
			return this.Providers.SelectMany( 
				x => x.FindUsersInRole( roleName, usernameToMatch ) 
			).Distinct().ToArray();
		}
		public override System.String[] GetAllRoles() { 
			return this.Providers.SelectMany( 
				x => x.GetAllRoles() 
			).Distinct().ToArray();
		}
		public override System.String[] GetRolesForUser( System.String username ) { 
			return this.Providers.SelectMany( 
				x => x.GetRolesForUser( username ) 
			).Distinct().ToArray();
		}
		public override System.String[] GetUsersInRole( System.String roleName ) { 
			return this.Providers.SelectMany( 
				x => x.GetUsersInRole( roleName ) 
			).Distinct().ToArray();
		}
		public override System.Boolean IsUserInRole( System.String username, System.String roleName ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.IsUserInRole( username, roleName ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.IsUserInRole( username, roleName ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		public override void RemoveUsersFromRoles( System.String[] usernames, System.String[] roleNames ) { 
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					var p = this.Providers.FirstOrDefault();
					if ( null != p ) { 
						p.RemoveUsersFromRoles( usernames, roleNames );
					}
					break;
				case AggregateBehaviour.All : 
					foreach ( var q in this.Providers ) { 
						q.RemoveUsersFromRoles( usernames, roleNames );
					}
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
		}
		public override System.Boolean RoleExists( System.String roleName ) { 
			var output = false;
			switch ( this.Behaviour ) { 
				case AggregateBehaviour.Any : 
					output = this.Providers.Any( 
						x => x.RoleExists( roleName ) 
					);
					break;
				case AggregateBehaviour.All : 
					output = this.Providers.All( 
						x => x.RoleExists( roleName ) 
					);
					break;
				default : 
					throw new System.Configuration.Provider.ProviderException( "The specified Behaviour is invalid." );
			}
			return output;
		}
		#endregion RoleProvider
		#endregion methods

	}

}