﻿using System.Linq;
namespace Oits.Web.Security { 

	[System.AttributeUsage( ( System.AttributeTargets.Field | System.AttributeTargets.Property ), AllowMultiple = false, Inherited = true )]
	public sealed class PasswordMinLengthAttribute : PasswordValidatorAttributeBase { 

		#region fields
		private const System.String theDefaultErrorMessage = "'{0}' must be at least '{1:g}' characters long.";

		private const System.String theMaxLengthErrorMessage = "The field {0} must be a string with a maximum length of {2}.";
		private const System.String theMinMaxLegnthErrorMessage = "The field {0} must be a string with a minimum length of {1} and a maximum length of {2}.";
		private const System.String theMinLengthErrorMessage = "The field {0} must be a string with a minimum length of {1}.";
		#endregion fields


		#region .ctor
		public PasswordMinLengthAttribute() : this( theDefaultErrorMessage ) { 
		}
		public PasswordMinLengthAttribute( System.String errorMessage ) : this( null, errorMessage.TrimToNull() ) { 
		}
		public PasswordMinLengthAttribute( System.String providerName, System.String errorMessage ) : base( providerName.TrimToNull(), errorMessage.TrimToNull() ?? theDefaultErrorMessage ) { 
		}
		#endregion .ctor


		#region methods
		public sealed override System.String FormatErrorMessage( System.String name ) { 
			return base.FormatErrorMessage( this.ErrorMessageString, name, this.Provider.MinRequiredPasswordLength );
		}
		protected sealed override System.ComponentModel.DataAnnotations.ValidationResult IsValid( System.String value, System.ComponentModel.DataAnnotations.ValidationContext validationContext ) { 
			if ( null == value ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			}

			var minCount = this.Provider.MinRequiredPasswordLength;
			if ( 0 == minCount ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else if ( minCount <= value.Length ) { 
				return System.ComponentModel.DataAnnotations.ValidationResult.Success;
			} else { 
				return this.CreateValidationFailure( validationContext );
			}
		}

		public sealed override System.Collections.Generic.IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules( System.Web.Mvc.ModelMetadata metadata, System.Web.Mvc.ControllerContext context ) { 
			System.Collections.Generic.ICollection<System.Web.Mvc.ModelClientValidationRule> output = new System.Collections.Generic.List<System.Web.Mvc.ModelClientValidationRule>( 1 );

			var min = this.Provider.MinRequiredPasswordLength;
			if ( 0 < min ) { 
				var minRule = this.CreateRule( "password", this.ErrorMessageString, metadata, min );
				minRule.ValidationParameters.Add( "min", min );
				output.Add( minRule );
			}

			return output;
		}
		#endregion methods

	}

}